angular.module('report').controller('ReportController', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal",
    function($http, $location, $scope, $compile, $rootScope, $modal) {
    //var self = this;
    console.log('reporty');

    $scope.url = function(url){
        $location.path(url);
    };

    $scope.relativeDetail = function(id) {
        window.location.assign("#!/registration_relative_detail/2");
        console.log(id);
    };

    $scope.showModal = function (id) {
        $('#' + id).modal('show');
    };
}]);
