angular.module('report').controller('ReportMark', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal","$window","$sce",
    function($http, $location, $scope, $compile, $rootScope, $modal, $window,$sce) {

    $scope.JSONtemplate = [
        {
            mainModel: "tdrsUser"
        },
        {form: [
            {
                url: "/data/save",
                model: "register12",
                method: "post",
                fieldsColumn: 2,
                fields: [
                    {
                        field: "select",
                        type: "text",
                        model: "cbLegalStatusType",
                        label: "Аймаг хот",
                        value: "",
                        select: "/refAimagCities",
                        selectValue: {
                            id: "id",
                            name: "name"
                        },
                        disable: false,
                        validation: []
                    },
                    {
                        field: "select",
                        type: "text",
                        model: "cbLegalStatusType",
                        label: "Сум дүүрэг",
                        value: "",
                        select: "/refSoumDistricts",
                        selectValue: {
                            id: "id",
                            name: "name"
                        },
                        disable: false,
                        validation: []
                    },
                    {
                        field: "input",
                        type: "Date",
                        model: "text",
                            label: "Эхлэх огноо",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    },
                    {
                        field: "input",
                        type: "Date",
                        model: "text",
                        label: "Дуусах огноо",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    }
                ],
                buttons: []
            },
            {
                url: "",
                model: "a1",
                method: "",
                fieldsColumn: 2,
                fields: [],
                buttons: [
                    {
                        type: "button",
                        value: "Хайх",
                        action: 'search'
                    },
                    {
                        type: "button-green",
                        value: "Excel",
                        action: 'excel'
                    },
                    {
                        type: "button",
                        value: "PDF",
                        action: 'pdf'
                    }
                ]
            }
        ]},

        // {table: [
        //     {
        //         url: "/sysUsers/?page=0&size=10",
        //         header: ["Д/д", "Улсын №", "Өмнөх №", "Арлын дугаар", "Эзэмшигч", "Марк, загвар","Огноо","Тэнцсэн эсэх","ТХТехХГ №","Утааны ангилал","Тусгай тэмдэглэл"],
        //         editable: true,
        //         dataList: 'dataList',
        //         bodyModels: ["id","branchId","fullname","loginname","levelId","status","levelId","status","levelId","status","levelId"],
        //         itemSize: 20
        //     }
        // ]},
        // {accordion: [
        //     {
        //         title: 'Хаягийн бүртгэл',
        //         data: [
        //             {form: [
        //                 {
        //                     url: "",
        //                     model: "z2",
        //                     method: "dd",
        //                     fieldsColumn: 2,
        //                     fields: [],
        //                     buttons: [
        //                         {
        //                             type: "button",
        //                             value: "нэмэх",
        //                             action: 'addressAdd()'
        //                         }
        //                     ]
        //                 }
        //             ]},
        //             {table: [
        //                 {
        //                     url: "/data/table",
        //                     header: ["Хаягийн төрөл", "Хаяг", "Шуудангийн хайрцаг", "Үндсэн хаяг", "Бүртгүүлсэн огноо", "Бүртгэл дуусгавар болсон огноо"],
        //                     editable: true,
        //                     dataList: 'dataList',
        //                     bodyModels: [1,2,3,4,5,6],
        //                     itemSize: 20
        //                 }
        //             ]}
        //         ]
        //     },
        //     // {
        //     //     title: 'Холбоо барих этгээдийн бүртгэл',
        //     //     data: [
        //     //         {form: [
        //     //             {
        //     //                 url: "",
        //     //                 model: "d2",
        //     //                 method: "aa",
        //     //                 fieldsColumn: 2,
        //     //                 fields: [],
        //     //                 buttons: [
        //     //                     {
        //     //                         type: "button",
        //     //                         value: "нэмэх",
        //     //                         action: ''
        //     //                     }
        //     //                 ]
        //     //             }
        //     //         ]},
        //     //         {table: [
        //     //             {
        //     //                 url: "/data/table",
        //     //                 header: ["Холбоо барих этгээдийн нэр", "Татвар төлөгчийн юу болох", "Холбоо барих хэрэгсэл", "Утасны дугаар,факс, цахим шуудан", "Бүртгүүлсэн огноо", "Бүртгэл дуусгавар болсон огноо", "Үндсэн эсэх"],
        //     //                 editable: true,
        //     //                 dataList: 'dataList',
        //     //                 bodyModels: [1,2,3,4,5,6,7],
        //     //                 itemSize: 20
        //     //             }
        //     //         ]}
        //     //     ]
        //     // }
        // ]}
    ];
    //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
    templateLoad('ReportMark', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);

    // $scope.url = $sce.trustAsResourceUrl('http://localhost:8082/reports/TechnicalDiagnosticResults/html');
    // $scope.url = url.replace("watch?v=", "v/");

    $scope.search = function() {
        // $window.open('http://202.131.242.228:8082/reports/MarkByCity/html', '_blank');
        $window.open('http://localhost:8082/reports/MarkByCity/html', '_blank');
    };
    $scope.excel = function() {
        // $window.open('http://202.131.242.228:8082/reports/MarkByCity/xlsx');
        $window.open('http://localhost:8082/reports/MarkByCity/xlsx');
    };
    $scope.pdf = function() {
        // $window.open('http://202.131.242.228:8082/reports/MarkByCity/pdf');
        $window.open('http://localhost:8082/reports/MarkByCity/pdf');
    };
}]);
