'use strict';
// Setting up route
angular.module('report').config(['$stateProvider',
    function($stateProvider) {
        $stateProvider.
        state('report', {
            url: '/report',
            abstract: true,
            templateUrl: 'modules/template/layout.html'
        }).
        state('report.dashboard', {
            url: '',
            templateUrl: 'modules/report/views/report.user.html',
            controller: 'ReportUser'
        }).
        state('report.umchlul', {
            url: '/umchlul',
            templateUrl: 'modules/report/views/report.umchlul.html',
            controller: 'ReportUmchlul'
        }).
        state('report.diagnosticAmt', {
            url: '/diagnosticAmt',
            templateUrl: 'modules/report/views/report.onoshlogoodun.html',
            controller: 'ReportOnosh'
        }).
        state('report.mark', {
            url: '/mark',
            templateUrl: 'modules/report/views/report.mark.html',
            controller: 'ReportMark'
        }).
        state('report.diagnosticResearch', {
            url: '/diagnosticResearch',
            templateUrl: 'modules/report/views/report.diagnosticRes.html',
            controller: 'ReportDiagnosticRes'
        }).
        state('report.compareByYear', {
            url: '/compareByYear',
            templateUrl: 'modules/report/views/report.compareByYear.html',
            controller: 'ReportCompareByYear'
        }).
        state('report.equipmentModel', {
            url: '/equipment/model',
            templateUrl: 'modules/registration/views/registration.equipment.model.html',
            controller: 'RegistrationEquipmentModel'
        }).
        state('report.equipmentSparePart', {
            url: '/equipment/sparepart',
            templateUrl: 'modules/registration/views/registration.equipment.spare.html',
            controller: 'RegistrationEquipmentSpare'
        }).
        state('report.equipmentFix', {
            url: '/equipment/fix',
            templateUrl: 'modules/registration/views/registration.equipment.fix.html',
            controller: 'RegistrationEquipmentFix'
        }).
        state('report.equipmentSent', {
            url: '/equipment/sent',
            templateUrl: 'modules/registration/views/registration.equipment.sent.html',
            controller: 'RegistrationEquipmentSent'
        }).
        state('report.equipmentReceive', {
            url: '/equipment/receive',
            templateUrl: 'modules/registration/views/registration.equipment.receive.html',
            controller: 'RegistrationEquipmentReceive'
        }).
        state('report.relative', {
            url: '/relative',
            templateUrl: 'modules/registration/views/registration.relative.html',
            controller: 'RegistrationRelative'
        }).
        state('report.relativeDetail', {
            url: '/relative/detail',
            templateUrl: 'modules/registration/views/registration.relative.detail.html',
            controller: 'RegistrationController'
        }).
        state('report.list', {
            url: '/list',
            templateUrl: 'modules/registration/views/registration.update.list.html',
            controller: 'RegistrationUpdateList'
        }).
        state('report.update', {
            url: '/update/taxpayer/:taxpayerId',
            templateUrl: 'modules/registration/views/registration.update.taxpayer.html',
            controller: 'RegistrationUpdateTaxpayer'
        }).
        state('report.update.taxpayer', {
            url: '/registration/taxpayer/edit',
            templateUrl: 'modules/registration/views/registration.update.taxpayer.html',
            controller: 'RegistrationController'
        }).
        state('report.waitlist', {
            url: '/waitlist',
            templateUrl: 'modules/registration/views/registration.waitlist.html',
            controller: 'RegistrationWaitlist'
        }).
        state('report.retype', {
            url: '/retype',
            templateUrl: 'modules/registration/views/registration.repeat.type.html',
            controller: 'RegistrationTypeRepeat'
        }).
        state('report.unregistered', {
            url: '/unregistered',
            templateUrl: 'modules/registration/views/registration.unregistered.html',
            controller: 'Unregistered'
        }).
        state('report.country', {
            url: '/country',
            templateUrl: 'modules/registration/views/registration.country.html',
            controller: 'RegistrationCountry'
        }).
        state('report.city', {
            url: '/city',
            templateUrl: 'modules/registration/views/registration.city.html',
            controller: 'RegistrationCity'
        }).
        state('report.soum', {
            url: '/soum',
            templateUrl: 'modules/registration/views/registration.soum.html',
            controller: 'RegistrationSoum'
        }).
        state('report.branch', {
            url: '/branch',
            templateUrl: 'modules/registration/views/registration.branch.html',
            controller: 'RegistrationBranch'
        }).
        state('report.foreign', {
            url: '/foreign',
            templateUrl: 'modules/registration/views/registration.foreign.html',
            controller: 'RegistrationForeign'
        })
         .state('report.repeat', {
            url: '/repeat',
            templateUrl: 'modules/registration/views/registration.repeat.html',
             controller: 'RegistrationRepeat'

        })
        ;
    }
]);

