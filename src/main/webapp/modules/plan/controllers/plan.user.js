angular.module('plan').controller('PlanUser', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal","notificationService",
    function($http, $location, $scope, $compile, $rootScope, $modal,notificationService) {

    $scope.JSONtemplate = [
        {
            mainModel: "tdrsPlan"
        },
        {form: [
            {
                url: "/data/save",
                model: "register12",
                method: "post",
                fieldsColumn: 2,
                fields: [
                    {
                        field: "input",
                        type: "text",
                        model: "planYear",
                        label: "Жил",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    },
                    {
                        field: "select",
                        type: "",
                        model: "branch.id",
                        label: "Салбар",
                        value: "",
                        select: "/sysBranches",
                        selectValue: {
                            id: "id",
                            name: "name"
                        },
                        disable: false,
                        validation: []
                    },
                    {
                        field: "input",
                        type: "text",
                        model: "planMonth",
                        label: "Сар",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    }

                ],
                buttons: [
                    {
                        type: "button",
                        value: "Хайх",
                        action: 'search',
                        actionModel: 'planList',
                        resultTables: [
                            {
                                service: "/data/sysPlans",
                                model: "dataList"
                            }
                        ]
                    },
                    {
                        type: "button",
                        value: "Нэмэх",
                        class: "btn btn-success",
                        action: '',
                        modal: [
                            {
                                form: [
                                    {
                                        url: "/",
                                        model: "locationModel",
                                        method: "post",
                                        defaultValues: {
                                            data: {},
                                            url: ""
                                        },
                                        fieldsColumn: 2,
                                        fields: [
                                            {
                                                field: "select",
                                                type: "",
                                                model: "planYear",
                                                label: "Төлөвлөсөн жил",
                                                value: "",
                                                select: "/sysBranches",
                                                selectValue: {
                                                    id: "id",
                                                    name: "name"
                                                },
                                                disable: false,
                                                validation: []
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "carCount",
                                                label: "Суудал",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "planMonth",
                                                label: "Төлөвлөсөн сар",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "lorryCount",
                                                label: "Ачаа",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "select",
                                                type: "",
                                                model: "branchId",
                                                label: "Салбар",
                                                value: "",
                                                select: "/sysBranches",
                                                selectValue: {
                                                    id: "id",
                                                    name: "name"
                                                },
                                                disable: false,
                                                validation: ["required"]
                                            },


                                            {
                                                field: "input",
                                                type: "text",
                                                model: "busCount",
                                                label: "Автобус",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "select",
                                                type: "",
                                                model: "planUser",
                                                label: "Төлөвлөлт хийсэн",
                                                value: "",
                                                select: "/sysUsers",
                                                selectValue: {
                                                    id: "id",
                                                    name: "fullname"
                                                },
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "specialCount",
                                                label: "Тусгай",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "select",
                                                type: "",
                                                model: "acceptUser",
                                                label: "Баталгаажуулсан",
                                                value: "",
                                                select: "/sysUsers",
                                                selectValue: {
                                                    id: "id",
                                                    name: "fullname"
                                                },
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "machineCount",
                                                label: "Механизм",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "trailerCount",
                                                label: "Чиргүүл",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "motorcycleCount",
                                                label: "Мотоцикл",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                        ],
                                        buttons: [
                                            {
                                                type: "submit",
                                                value: "Нэмэх",
                                                action: 'add',
                                                url: "/data/sysPlans",
                                                actionModel: 'locationModel',
                                                resultTables: [
                                                    {
                                                        service: "/data/sysPlans",
                                                        model: "dataList",
                                                        // temp: true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]},
        {table: [
            {
                url: "/data/sysPlans",
                header: ["Д/д", "Салбар", "Жил", "Сар", "Суудал", "Ачаа","Автобус","Тусгай","Механизм","Чиргүүл","Мотоцикл","Төлөвлөлт хийсэн","Баталгаажуулсан"],
                editable: true,
                modal: [
                    {
                        form: [
                            {
                                url: "/",
                                model: "locationModel",
                                method: "post",
                                defaultValues: {
                                    data: {},
                                    url: "",
                                    fromTable: true
                                },
                                fieldsColumn: 2,
                                fields: [
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "planYear",
                                        label: "Төлөвлөсөн жил",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: []
                                    },
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "carCount",
                                        label: "Суудал",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "planMonth",
                                        label: "Төлөвлөсөн сар",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "lorryCount",
                                        label: "Ачаа",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "select",
                                        type: "",
                                        model: "branch",
                                        label: "Салбар",
                                        value: "",
                                        select: "/sysBranches",
                                        selectValue: {
                                            replaceValues: [
                                                {
                                                    sourceModel: "branch.id",
                                                    replaceModel: "name"
                                                }
                                            ],
                                            id: "id",
                                            name: "name"
                                        },
                                        disable: false,
                                        validation: ["required"]
                                    },


                                    {
                                        field: "input",
                                        type: "text",
                                        model: "busCount",
                                        label: "Автобус",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "select",
                                        type: "",
                                        model: "planUser",
                                        label: "Төлөвлөлт хийсэн",
                                        value: "",
                                        select: "/sysUsers",
                                        selectValue: {
                                            replaceValues: [
                                                {
                                                    sourceModel: "user.id",
                                                    replaceModel: "fullname"
                                                }
                                            ],
                                            id: "id",
                                            name: "fullname"
                                        },
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "specialCount",
                                        label: "Тусгай",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "select",
                                        type: "",
                                        model: "acceptUser",
                                        label: "Баталгаажуулсан",
                                        value: "",
                                        select: "/sysUsers",
                                        selectValue: {
                                            replaceValues: [
                                                {
                                                    sourceModel: "user.id",
                                                    replaceModel: "fullname"
                                                }
                                            ],
                                            id: "id",
                                            name: "fullname"
                                        },
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "machineCount",
                                        label: "Механизм",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "trailerCount",
                                        label: "Чиргүүл",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "motorcycleCount",
                                        label: "Мотоцикл",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                ],
                                buttons: [
                                    {
                                        type: "submit",
                                        value: "Хадгалах",
                                        action: 'update',
                                        url: "/data/sysPlans/",
                                        actionModel: 'locationModel',
                                        resultTables: [
                                            {
                                                service: "/data/sysPlans",
                                                model: "dataList",
                                                // temp: true
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ],
                dataList: 'dataList',
                bodyModels: ["id","branch.name","planYear","planMonth","carCount","lorryCount","busCount","specialCount","machineCount","trailerCount","motorcycleCount","planUser.fullname","acceptUser.fullname"],
                itemSize: 20
            }
        ]},

    ];

    //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
    templateLoad('PlanUser', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);
        $rootScope.$on("notificationServiceSuccess", function(event, data) {
            console.log("success");
            notificationService.notify({
                title: "Амжилттай",
                text: data,
                type: 'success',
                hide: false,
                closer: true,
                styling: 'bootstrap3'
            });
        });
        $rootScope.$on("notificationServiceError", function(event, data) {
            console.log("FAILS");
            notificationService.notify({
                title: "Амжилтгүй",
                text: data,
                type: 'error',
                hide: false,
                closer: true,
                styling: 'bootstrap3'
            });
        });
    $scope.add = function() {
        console.log("add");
    };
}]);
