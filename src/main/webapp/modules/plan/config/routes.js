'use strict';
// Setting up route
angular.module('plan').config(['$stateProvider',
    function($stateProvider) {
        $stateProvider.
        state('plan', {
            url: '/plan',
            abstract: true,
            templateUrl: 'modules/template/layout.html'
        }).
        state('plan.dashboard', {
            url: '',
            templateUrl: 'modules/plan/views/plan.user.html',
            controller: 'PlanUser'
        }).
        state('report.umchlul1', {
            url: '/umchlul1',
            templateUrl: 'modules/report/views/report.umchlul.html',
            controller: 'ReportUmchlul'
        })
        ;
    }
]);

