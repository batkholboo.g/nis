'use strict';

// Setting up route
angular.module('user').config(['$stateProvider',
    function($stateProvider) {
        // Users state routing
        $stateProvider.
        state('signup', {
            url: '/signup',
            templateUrl: 'modules/user/views/signup.client.view.html'
        }).
        state('login', {
            url: '/login',
            templateUrl: 'modules/user/views/login.html'
        }).
        state('forgot', {
            url: '/password/forgot',
            templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
        }).
        state('reset-invalid', {
            url: '/password/reset/invalid',
            templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
        }).
        state('reset-success', {
            url: '/password/reset/success',
            templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
        }).
        state('reset', {
            url: '/password/reset/:token',
            templateUrl: 'modules/users/views/password/reset-password.client.view.html'
        });
    }
]);