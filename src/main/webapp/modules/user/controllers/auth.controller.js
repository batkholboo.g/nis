'use strict';

angular.module('user').controller('AuthenticationController', ['$scope', '$http', '$window', '$localStorage','$cookies',
    function($scope, $http, $window, $localStorage,$cookies) {

        $scope.login = function() {
            // $localStorage.currentUser = { username: 'Tushig', token: '123123123' };
            // $window.location.href = '/';
            $scope.loading = true;
            $http.post('/auth/login', { username: $scope.username, password: $scope.password})
                .success(function (response) {
                    if (response) {
                        $localStorage.currentUser = { username: $scope.username, token: response.token };
                        $http.defaults.headers.common.Authorization = 'Bearer ' + response;
                        // console.log(currentUser);
                        // console.log(currentUser);
                        $cookies.menuId = 1;
                        $window.location.href = '/';
                    } else {
                        $scope.error = 'Ажилтаны код эсвэл нууц үг буруу байна.';
                        $scope.loading = false;
                    }
                }).error(function(response) {
                    $scope.error = 'Ажилтаны код эсвэл нууц үг буруу байна.';
                    $scope.loading = false;
                });
        };


    }
]);