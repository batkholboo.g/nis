'use strict';
// Setting up route
angular.module('registration').config(['$stateProvider',
    function($stateProvider) {
        $stateProvider.
        state('registration', {
            url: '/registration',
            abstract: true,
            templateUrl: 'modules/template/layout.html'
        }).
        state('registration.dashboard', {
            url: '',
            templateUrl: 'modules/registration/views/registration.user.html',
            controller: 'RegistrationUser'
        }).
        state('registration.individual', {
            url: '/individual',
            templateUrl: 'modules/registration/views/registration.individual.html',
            controller: 'RegistrationIndividual'
        }).
        state('registration.permission', {
            url: '/permission',
            templateUrl: 'modules/registration/views/registration.permission.html',
            controller: 'RegistrationPermission'
        }).
        state('registration.level', {
            url: '/level',
            templateUrl: 'modules/registration/views/registration.level.html',
            controller: 'RegistrationLevel'
        }).
        state('registration.equipment', {
            url: '/equipment',
            templateUrl: 'modules/registration/views/registration.equipment.html',
            controller: 'RegistrationEquipment'
        }).
        state('registration.equipmentCategory', {
            url: '/equipment/category',
            templateUrl: 'modules/registration/views/registration.equipment.category.html',
            controller: 'RegistrationEquipmentCategory'
        }).
        state('registration.equipmentModel', {
            url: '/equipment/model',
            templateUrl: 'modules/registration/views/registration.equipment.model.html',
            controller: 'RegistrationEquipmentModel'
        }).
        state('registration.equipmentSparePart', {
            url: '/equipment/sparepart',
            templateUrl: 'modules/registration/views/registration.equipment.spare.html',
            controller: 'RegistrationEquipmentSpare'
        }).
        state('registration.equipmentFix', {
            url: '/equipment/fix',
            templateUrl: 'modules/registration/views/registration.equipment.fix.html',
            controller: 'RegistrationEquipmentFix'
        }).
        state('registration.equipmentSent', {
            url: '/equipment/sent',
            templateUrl: 'modules/registration/views/registration.equipment.sent.html',
            controller: 'RegistrationEquipmentSent'
        }).
        state('registration.equipmentReceive', {
            url: '/equipment/receive',
            templateUrl: 'modules/registration/views/registration.equipment.receive.html',
            controller: 'RegistrationEquipmentReceive'
        }).
        state('registration.relative', {
            url: '/relative',
            templateUrl: 'modules/registration/views/registration.relative.html',
            controller: 'RegistrationRelative'
        }).
        state('registration.relativeDetail', {
            url: '/relative/detail',
            templateUrl: 'modules/registration/views/registration.relative.detail.html',
            controller: 'RegistrationController'
        }).
        state('registration.list', {
            url: '/list',
            templateUrl: 'modules/registration/views/registration.update.list.html',
            controller: 'RegistrationUpdateList'
        }).
        state('registration.update', {
            url: '/update/taxpayer/:taxpayerId',
            templateUrl: 'modules/registration/views/registration.update.taxpayer.html',
            controller: 'RegistrationUpdateTaxpayer'
        }).
        state('registration.update.taxpayer', {
            url: '/registration/taxpayer/edit',
            templateUrl: 'modules/registration/views/registration.update.taxpayer.html',
            controller: 'RegistrationController'
        }).
        state('registration.waitlist', {
            url: '/waitlist',
            templateUrl: 'modules/registration/views/registration.waitlist.html',
            controller: 'RegistrationWaitlist'
        }).
        state('registration.retype', {
            url: '/retype',
            templateUrl: 'modules/registration/views/registration.repeat.type.html',
            controller: 'RegistrationTypeRepeat'
        }).
        state('registration.unregistered', {
            url: '/unregistered',
            templateUrl: 'modules/registration/views/registration.unregistered.html',
            controller: 'Unregistered'
        }).
        state('registration.country', {
            url: '/country',
            templateUrl: 'modules/registration/views/registration.country.html',
            controller: 'RegistrationCountry'
        }).
        state('registration.city', {
            url: '/city',
            templateUrl: 'modules/registration/views/registration.city.html',
            controller: 'RegistrationCity'
        }).
        state('registration.soum', {
            url: '/soum',
            templateUrl: 'modules/registration/views/registration.soum.html',
            controller: 'RegistrationSoum'
        }).
        state('registration.branch', {
            url: '/branch',
            templateUrl: 'modules/registration/views/registration.branch.html',
            controller: 'RegistrationBranch'
        }).
        state('registration.foreign', {
            url: '/foreign',
            templateUrl: 'modules/registration/views/registration.foreign.html',
            controller: 'RegistrationForeign'
        })
         .state('registration.repeat', {
            url: '/repeat',
            templateUrl: 'modules/registration/views/registration.repeat.html',
             controller: 'RegistrationRepeat'

        })
        ;
    }
]);

