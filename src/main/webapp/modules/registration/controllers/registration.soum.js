angular.module('registration').controller('RegistrationSoum', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal","notificationService",
        function($http, $location, $scope, $compile, $rootScope, $modal,notificationService) {

            $scope.JSONtemplate = [
                {
                    mainModel: "tdrsSoum"
                },
                {form: [
                    {
                        url: "/data/waitlist",
                        model: "RegWaitlist",
                        method: "get",
                        fieldsColumn: 2,
                        fields: [
                            {
                                field: "input",
                                type: "text",
                                model: "code",
                                label: "Код",
                                value: "4124125",
                                select: "",
                                disable: false,
                                validation: []
                            },

                            {
                                field: "input",
                                type: "text",
                                model: "status",
                                label: "Төлөв",
                                value: "",
                                select: "/list",
                                disable: false,
                                validation: []
                            },
                            {
                                field: "input",
                                type: "text",
                                model: "soumName",
                                label: "Нэр",
                                value: "",
                                select: "",
                                disable: false,
                                validation: []
                            },
                            {
                                field: "select",
                                type: "text",
                                model: "aimagCity",
                                label: "Аймаг хот",
                                value: "",
                                select: "/refAimagCities",
                                selectValue: {
                                    replaceValues: [
                                        {
                                            sourceModel: "aimagCity.id",
                                            replaceModel: "name"
                                        }
                                    ],
                                    id: "id",
                                    name: "name"
                                },
                                disable: false,
                                validation: []
                            }
                        ],
                        buttons: [
                            {
                                type: "submit",
                                value: "хайх",
                                action: 'search',
                                actionModel: 'RegistrationSoumList',
                                resultTables: [
                                    {
                                        service: "/data/refSoumDistricts",
                                        model: "dataList1"
                                    }
                                ]
                            },
                            {
                                type: "button",
                                value: "нэмэх",
                                class: "btn btn-success",
                                action: '',
                                modal: [
                                    {
                                        form: [
                                            {
                                                url: "/",
                                                model: "locationModel",
                                                method: "post",
                                                defaultValues: {
                                                    data: {},
                                                    url: ""
                                                },
                                                fieldsColumn: 2,
                                                fields: [
                                                    {
                                                        field: "input",
                                                        type: "checkbox",
                                                        model: "status",
                                                        label: "Төлөв",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: []
                                                    },
                                                    {
                                                        field: "select",
                                                        type: "",
                                                        model: "branchId",
                                                        label: "Салбар",
                                                        value: "",
                                                        select: "/sysBranches",
                                                        selectValue: {
                                                            id: "id",
                                                            name: "name"
                                                        },
                                                        disable: false,
                                                        validation: ["required"]
                                                    },
                                                    {
                                                        field: "select",
                                                        type: "",
                                                        model: "levelId",
                                                        label: "Эрхийн түвшин",
                                                        value: "",
                                                        select: "/sysLevels",
                                                        selectValue: {
                                                            id: "id",
                                                            name: "name"
                                                        },
                                                        disable: false,
                                                        validation: ["required"]
                                                    },
                                                    {
                                                        field: "input",
                                                        type: "text",
                                                        model: "fullname",
                                                        label: "Нэр",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: ["required"]
                                                    },
                                                    {
                                                        field: "input",
                                                        type: "text",
                                                        model: "loginname",
                                                        label: "Нэвтрэх нэр",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: ["required"]
                                                    },
                                                    {
                                                        field: "input",
                                                        type: "text",
                                                        model: "password",
                                                        label: "Нууц үг",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: ["required"]
                                                    },
                                                ],
                                                buttons: [
                                                    {
                                                        type: "submit",
                                                        value: "Нэмэх",
                                                        action: 'add',
                                                        url: "/data/sysUsers",
                                                        actionModel: 'locationModel',
                                                        resultTables: [
                                                            {
                                                                service: "/data/sysUsers",
                                                                model: "dataList",
                                                                // temp: true
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]},
                {table: [
                    {
                        url: "/data/refSoumDistricts/?page=0&size=10",
                        header: ["Д/д", "Аймаг хот", "Код", "Нэр","Татварын төрөл","Tөлөв"],
                        editable: true,
                        dataList: 'dataList1', //TODO: list model
                        bodyModels: ["id", "aimagCity.name", "code", "soumName","taxtype","status"],
                        pageIndex: 0,
                        itemSize: 10
                    }
                ]}
            ];

            // $scope.subComponentLoad = function(data) {
            //
            // };

            // $rootScope.templateLoad('data', $scope.JSONtemplate);

            $scope.aregister12 = {};

            $rootScope.$on('selectList', function (event, data) {
                console.log(data);
                $scope.aregister12.ttd = data['2'];
                $scope.aregister12.rd = data['2'];
                $scope.aregister12.aann = data['3'];
            });

            //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
            templateLoad('RegistrationSoum', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);

            $scope.az2 = {};
            $scope.az2.demomodel = "123124124";
            $scope.aann = "АОЙРЛБОЙ ХХК";

            $scope.add = function() {
                console.log("add");
            };

            $scope.remove = function() {
                console.log("remove");
            };


            $rootScope.modalUrl = "/aaa/bbb";

            // $scope.url = function(url){
            //     $location.path(url);
            //     if (url == '/debtcall_home') {
            //         $cookies.debtCallPage = 0;
            //     }
            // };
            //
            // $scope.openNewTabUrl = function(url) {
            //     window.open(url);
            // };

            // $scope.relativeDetail = function(id) {
            //     window.location.assign("#!/registration_relative_detail/2");
            //     console.log(id);
            // }
        }]);
