angular.module('registration').controller('RegistrationBranch', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal",
        function($http, $location, $scope, $compile, $rootScope, $modal) {

            $scope.JSONtemplate = [
                {
                    mainModel: "tdrsUser"
                },
                {form: [
                    {
                        url: "/data/waitlist",
                        model: "RegWaitlist",
                        method: "get",
                        fieldsColumn: 2,
                        fields: [
                            {
                                field: "input",
                                type: "text",
                                model: "rd",
                                label: "Салбар",
                                value: "4124125",
                                select: "",
                                disable: false,
                                validation: []
                            },

                            {
                                field: "select",
                                type: "text",
                                model: "cbLegalStatusType",
                                label: "Аймаг хот",
                                value: "",
                                select: "/refAimagCities",
                                selectValue: {
                                    id: "id",
                                    name: "name"
                                },
                                disable: false,
                                validation: []
                            },
                            {
                                field: "input",
                                type: "text",
                                model: "rd1",
                                label: "Нэр",
                                value: "4124125",
                                select: "",
                                disable: false,
                                validation: []
                            }
                        ],
                        buttons: [
                            {
                                type: "submit",
                                value: "хайх",
                                action: 'search',
                                actionModel: 'BranchSearchList',
                                resultTables: [
                                    {
                                        service: "/data/sysBranches",
                                        model: "dataList"
                                    }
                                ]
                            },
                            {
                                type: "button",
                                value: "нэмэх",
                                class: "btn btn-success",
                                action: '',
                                modal: [
                                    {
                                        form: [
                                            {
                                                url: "/",
                                                model: "locationModel",
                                                method: "post",
                                                defaultValues: {
                                                    data: {},
                                                    url: ""
                                                },
                                                fieldsColumn: 2,
                                                fields: [
                                                    {
                                                        field: "input",
                                                        type: "checkbox",
                                                        model: "status",
                                                        label: "Төлөв",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: []
                                                    },
                                                    {
                                                        field: "select",
                                                        type: "",
                                                        model: "branchId",
                                                        label: "Салбар",
                                                        value: "",
                                                        select: "/sysBranches",
                                                        selectValue: {
                                                            id: "id",
                                                            name: "name"
                                                        },
                                                        disable: false,
                                                        validation: ["required"]
                                                    },
                                                    {
                                                        field: "select",
                                                        type: "",
                                                        model: "groupId",
                                                        label: "Эрхийн түвшин",
                                                        value: "",
                                                        select: "/sysGroups",
                                                        selectValue: {
                                                            id: "id",
                                                            name: "name"
                                                        },
                                                        disable: false,
                                                        validation: ["required"]
                                                    },
                                                    {
                                                        field: "input",
                                                        type: "text",
                                                        model: "fullname",
                                                        label: "Нэр",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: ["required"]
                                                    },
                                                    {
                                                        field: "input",
                                                        type: "text",
                                                        model: "loginname",
                                                        label: "Нэвтрэх нэр",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: ["required"]
                                                    },
                                                    //
                                                    // {
                                                    //     field: "input",
                                                    //     type: "Date",
                                                    //     model: "registerDate",
                                                    //     label: "Бүртгүүлсэн огноо",
                                                    //     value: "",
                                                    //     select: "",
                                                    //     disable: false,
                                                    //     validation: ["required"]
                                                    // },
                                                    {
                                                        field: "input",
                                                        type: "text",
                                                        model: "password",
                                                        label: "Нууц үг",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: ["required"]
                                                    },
                                                ],
                                                buttons: [
                                                    {
                                                        type: "submit",
                                                        value: "Нэмэх",
                                                        action: 'add',
                                                        url: "/data/sysUsers",
                                                        actionModel: 'locationModel',
                                                        resultTables: [
                                                            {
                                                                service: "/data/sysUsers",
                                                                model: "dataList",
                                                                // temp: true
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]},
                {table: [
                    {
                        url: "/data/sysBranches/?page=0&size=10",
                        header: ["Д/д", "Толгой салбар", "Аймаг хот", "Нэр","Емэйл","Салбар түвшин","Синхрон","Төлөв"],
                        editable: true,
                        dataList: 'dataList1', //TODO: list model
                        bodyModels: ["id", "parentId", "aimagId", "name","email","branchLevel","isSync","status"],
                        pageIndex: 0,
                        itemSize: 10
                    }
                ]}
            ];

            // $scope.subComponentLoad = function(data) {
            //
            // };

            // $rootScope.templateLoad('data', $scope.JSONtemplate);

            $scope.aregister12 = {};

            $rootScope.$on('selectList', function (event, data) {
                console.log(data);
                $scope.aregister12.ttd = data['2'];
                $scope.aregister12.rd = data['2'];
                $scope.aregister12.aann = data['3'];
            });

            //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
            templateLoad('RegistrationBranch', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);

            $scope.az2 = {};
            $scope.az2.demomodel = "123124124";
            $scope.aann = "АОЙРЛБОЙ ХХК";

            $scope.add = function() {
                console.log("add");
            };

            $scope.remove = function() {
                console.log("remove");
            };


            $rootScope.$on("notificationServiceSuccess", function(event, data) {
                console.log("success");
                notificationService.notify({
                    title: "Амжилттай",
                    text: data,
                    type: 'success',
                    hide: false,
                    closer: true,
                    styling: 'bootstrap3'
                });
            });
            $rootScope.$on("notificationServiceError", function(event, data) {
                console.log("FAILS");
                notificationService.notify({
                    title: "Амжилтгүй",
                    text: data,
                    type: 'error',
                    hide: false,
                    closer: true,
                    styling: 'bootstrap3'
                });
            });

            // $scope.url = function(url){
            //     $location.path(url);
            //     if (url == '/debtcall_home') {
            //         $cookies.debtCallPage = 0;
            //     }
            // };
            //
            // $scope.openNewTabUrl = function(url) {
            //     window.open(url);
            // };

            // $scope.relativeDetail = function(id) {
            //     window.location.assign("#!/registration_relative_detail/2");
            //     console.log(id);
            // }
        }]);
