angular.module('registration').controller('RegistrationEquipmentModel', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal",
    function($http, $location, $scope, $compile, $rootScope, $modal) {

    $scope.JSONtemplate = [
        {
            mainModel: "tdrsUser"
        },
        {form: [
            {
                url: "/data/save",
                model: "register12",
                method: "post",
                fieldsColumn: 2,
                fields: [
                    {
                        field: "input",
                        type: "text",
                        model: "ttd",
                        label: "Дугаар",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    },
                    {
                        field: "input",
                        type: "text",
                        model: "ttt",
                        label: "Нэр",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    }
                    // ,{
                    //     field: "input",
                    //     type: "text",
                    //     model: "rd",
                    //     label: "Нэр",
                    //     value: "",
                    //     select: "",
                    //     disable: false,
                    //     validation: []
                    // }
                    // {
                    //     field: "input",
                    //     type: "date",
                    //     model: "text",
                    //     label: "Бүртгүүлсэн огноо",
                    //     value: "",
                    //     select: "",
                    //     disable: true,
                    //     validation: []
                    // },
                   // , {
                   //      field: "select",
                   //      type: "",
                   //      model: "aann",
                   //      label: "Салбар",
                   //      value: "",
                   //      select: "/ttturul",
                   //      disable: false,
                   //      validation: []
                   //  },
                    ,
                    {
                        field: "input",
                        type: "",
                        model: "text",
                        label: "Төлөв",
                        value: "",
                        select: "/tttbaidal",
                        disable: false,
                        validation: []
                    }
                ],
                buttons: []
            },
            {
                url: "",
                model: "a1",
                method: "",
                fieldsColumn: 2,
                fields: [],
                buttons: [
                    {
                        type: "button",
                        value: "Хайх",
                        action: 'ubeghaih()'
                    }
                ]
            }
        ]},
        {table: [
            {
                url: "/data/equModels/?page=0&size=10",
                header: ["Д/д", "Нэр","Хэмжилтийн хязгаар","Хэмжилтийн хязгаар","Хэмжилтийн хязгаар","Хэмжилтийн хязгаар","Хэмжилтийн хязгаар","Хэмжилтийн хязгаар","Хэмжилтийн хязгаар","Хэмжилтийн хязгаар","Хэмжилтийн хязгаар","Худалдаж авсан үнэ"],
                editable: true,
                dataList: 'dataList',
                bodyModels: ["id","modeName","countryId","infimum1St","supremum1St","infimum2Nd","supremum2Nd","infimum3Rd","supremum3Rd","infimum4Rd","supremum4Rd","cost"],
                itemSize: 20
            }
        ]},
        // {accordion: [
        //     {
        //         title: 'Хаягийн бүртгэл',
        //         data: [
        //             {form: [
        //                 {
        //                     url: "",
        //                     model: "z2",
        //                     method: "dd",
        //                     fieldsColumn: 2,
        //                     fields: [],
        //                     buttons: [
        //                         {
        //                             type: "button",
        //                             value: "нэмэх",
        //                             action: 'addressAdd()'
        //                         }
        //                     ]
        //                 }
        //             ]},
        //             {table: [
        //                 {
        //                     url: "/data/table",
        //                     header: ["Хаягийн төрөл", "Хаяг", "Шуудангийн хайрцаг", "Үндсэн хаяг", "Бүртгүүлсэн огноо", "Бүртгэл дуусгавар болсон огноо"],
        //                     editable: true,
        //                     dataList: 'dataList',
        //                     bodyModels: [1,2,3,4,5,6],
        //                     itemSize: 20
        //                 }
        //             ]}
        //         ]
        //     },
        //     // {
        //     //     title: 'Холбоо барих этгээдийн бүртгэл',
        //     //     data: [
        //     //         {form: [
        //     //             {
        //     //                 url: "",
        //     //                 model: "d2",
        //     //                 method: "aa",
        //     //                 fieldsColumn: 2,
        //     //                 fields: [],
        //     //                 buttons: [
        //     //                     {
        //     //                         type: "button",
        //     //                         value: "нэмэх",
        //     //                         action: ''
        //     //                     }
        //     //                 ]
        //     //             }
        //     //         ]},
        //     //         {table: [
        //     //             {
        //     //                 url: "/data/table",
        //     //                 header: ["Холбоо барих этгээдийн нэр", "Татвар төлөгчийн юу болох", "Холбоо барих хэрэгсэл", "Утасны дугаар,факс, цахим шуудан", "Бүртгүүлсэн огноо", "Бүртгэл дуусгавар болсон огноо", "Үндсэн эсэх"],
        //     //                 editable: true,
        //     //                 dataList: 'dataList',
        //     //                 bodyModels: [1,2,3,4,5,6,7],
        //     //                 itemSize: 20
        //     //             }
        //     //         ]}
        //     //     ]
        //     // }
        // ]}
    ];

    //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
    templateLoad('RegistrationEquipmentModel', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);

    $scope.add = function() {
        console.log("add");
    };
}]);
