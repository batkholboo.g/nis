angular.module('registration').controller('RegistrationPermission', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal","notificationService",
    function($http, $location, $scope, $compile, $rootScope, $modal,notificationService) {

    $scope.showModal = function (id) {
        $('#' + id).modal('show');
    };
    $scope.JSONtemplate = [
        {
            mainModel: "tdrsPermission"
        },
        {form: [
            {
                url: "/data/save",
                model: "register12",
                method: "post",
                fieldsColumn: 2,
                fields: [
                    {
                        field: "select",
                        type: "",
                        model: "id",
                        label: "Эрхийн түвшин",
                        value: "",
                        select: "/sysGroups",
                        selectValue: {
                            id: "id",
                            name: "name"
                        },
                        disable: false,
                        validation: []
                    }
                ],
                buttons: [
                    {
                        type: "button",
                        value: "Хайх",
                        action: 'search',
                        actionModel: 'RegistrationUpdateList',
                        resultTables: [
                            {
                                service: "/data/sysGroups",
                                model: "dataList"
                            }
                        ]
                    },
                    {
                        type: "button",
                        value: "нэмэх",
                        class: "btn btn-success",
                        action: '',
                        modal: [
                            {
                                form: [
                                    {
                                        url: "/",
                                        model: "locationModel",
                                        method: "post",
                                        defaultValues: {
                                            data: {},
                                            url: ""
                                        },
                                        fieldsColumn: 2,
                                        fields: [
                                            {
                                                field: "select",
                                                type: "",
                                                model: "id",
                                                label: "Эрхийн түвшин",
                                                value: "",
                                                select: "/sysGroups",
                                                selectValue: {
                                                    id: "id",
                                                    name: "name"
                                                },
                                                disable: false,
                                                validation: []
                                            },
                                            {
                                                field: "select",
                                                type: "",
                                                model: "sysRoles.id",
                                                label: "Эрхүүд",
                                                value: "",
                                                select: "/sysRoles",
                                                selectValue: {
                                                    id: "id",
                                                    name: "name"
                                                },
                                                disable: false,
                                                validation: []
                                            },
                                        ],
                                        buttons: [
                                            {
                                                type: "submit",
                                                value: "Нэмэх",
                                                action: 'add',
                                                url: "/data/sysGroups",
                                                actionModel: 'locationModel',
                                                resultTables: [
                                                    {
                                                        service: "/data/sysGroups",
                                                        model: "dataList",
                                                        // temp: true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }]
            }
        ]},
        {table: [
            {
                url: "/data/sysGroups",
                header: ["Д/д", "Нэр","Төлөв"],
                editable: true,
                modal: [
                    {
                        form: [
                            {
                                url: "/",
                                model: "locationModel",
                                method: "post",
                                defaultValues: {
                                    data: {},
                                    url: "",
                                    fromTable: true
                                },
                                fieldsColumn: 2,
                                fields: [
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "code",
                                        label: "Код",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "name",
                                        label: "Нэр",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "input",
                                        type: "checkbox",
                                        model: "isRead",
                                        label: "Унших",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: []
                                    },

                                    {
                                        field: "input",
                                        type: "checkbox",
                                        model: "isWrite",
                                        label: "Бичих",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: []
                                    },
                                    {
                                        field: "input",
                                        type: "checkbox",
                                        model: "isUpdate",
                                        label: "Засах",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: []
                                    },
                                    {
                                        field: "input",
                                        type: "checkbox",
                                        model: "isDelete",
                                        label: "Устгах",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: []
                                    }
                                ],
                                buttons: [
                                    {
                                        type: "submit",
                                        value: "Хадгалах",
                                        action: 'update',
                                        url: "/data/sysPermissions/",
                                        actionModel: 'locationModel',
                                        resultTables: [
                                            {
                                                service: "/data/sysPermissions",
                                                model: "dataList",
                                                // temp: true
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ],
                dataList: 'dataList',
                bodyModels: ["id","name","status"],
                itemSize: 20
            }
        ]},

    ];

    //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
    templateLoad('RegistrationPermission', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);

    $rootScope.$on("notificationServiceSuccess", function(event, data) {
        console.log("success");
        notificationService.notify({
            title: "Амжилттай",
            text: data,
            type: 'success',
            hide: false,
            closer: true,
            styling: 'bootstrap3'
        });
    });
    $rootScope.$on("notificationServiceError", function(event, data) {
        console.log("FAILS");
        notificationService.notify({
            title: "Амжилтгүй",
            text: data,
            type: 'error',
            hide: false,
            closer: true,
            styling: 'bootstrap3'
        });
    });

    $scope.add = function() {
        $scope.showModal('newGroupModal');
    };
}]);
