angular.module('registration').controller('RegistrationEquipment', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal",
    function($http, $location, $scope, $compile, $rootScope, $modal) {

    $scope.JSONtemplate = [
        {
            mainModel: "tdrsUser"
        },
        {form: [
            {
                url: "/data/save",
                model: "register12",
                method: "post",
                fieldsColumn: 2,
                fields: [
                    {
                        field: "input",
                        type: "text",
                        model: "ttd",
                        label: "Дугаар",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    },
                    {
                        field: "input",
                        type: "text",
                        model: "ttt",
                        label: "Нэр",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    }
                    // ,{
                    //     field: "input",
                    //     type: "text",
                    //     model: "rd",
                    //     label: "Нэр",
                    //     value: "",
                    //     select: "",
                    //     disable: false,
                    //     validation: []
                    // }
                    // {
                    //     field: "input",
                    //     type: "date",
                    //     model: "text",
                    //     label: "Бүртгүүлсэн огноо",
                    //     value: "",
                    //     select: "",
                    //     disable: true,
                    //     validation: []
                    // },
                   // , {
                   //      field: "select",
                   //      type: "",
                   //      model: "aann",
                   //      label: "Салбар",
                   //      value: "",
                   //      select: "/ttturul",
                   //      disable: false,
                   //      validation: []
                   //  },
                    ,
                    {
                        field: "input",
                        type: "",
                        model: "text",
                        label: "Төлөв",
                        value: "",
                        select: "/tttbaidal",
                        disable: false,
                        validation: []
                    }

                ],
                buttons: [{
                    type: "submit",
                    value: "хайх",
                    action: 'search',
                    actionModel: 'RegistrationUpdateList',
                    resultTables: [
                        {
                            service: "/data/sysUsers",
                            model: "dataList"
                        }
                    ]
                },
                    {
                        type: "button",
                        value: "нэмэх",
                        class: "btn btn-success",
                        action: '',
                        modal: [
                            {
                                form: [
                                    {
                                        url: "/",
                                        model: "locationModel",
                                        method: "post",
                                        defaultValues: {
                                            data: {},
                                            url: ""
                                        },
                                        fieldsColumn: 2,
                                        fields: [
                                            {
                                                field: "select",
                                                type: "",
                                                model: "modelId",
                                                label: "Модел",
                                                value: "",
                                                select: "/equModels",
                                                selectValue: {
                                                    id: "id",
                                                    name: "modelName"
                                                },
                                                disable: false,
                                                validation: []
                                            },
                                            {
                                                field: "select",
                                                type: "",
                                                model: "branchId",
                                                label: "Салбар",
                                                value: "",
                                                select: "/sysBranches",
                                                selectValue: {
                                                    id: "id",
                                                    name: "name"
                                                },
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "select",
                                                type: "",
                                                model: "userId",
                                                label: "Эзэмшигч",
                                                value: "",
                                                select: "/sysUsers",
                                                selectValue: {
                                                    id: "id",
                                                    name: "fullname"
                                                },
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "serial",
                                                label: "Сериал",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "buildYear",
                                                label: "Үйлдвэрлэсэн он",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },

                                            {
                                                field: "input",
                                                type: "Date",
                                                model: "usedDate",
                                                label: "Хэрэглэж эхэлсэн огноо",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            // {
                                            //     field: "input",
                                            //     type: "checkBox",
                                            //     model: "isNotUse",
                                            //     label: "Эвдрэлтэй эсэх",
                                            //     value: "",
                                            //     select: "",
                                            //     disable: false,
                                            //     validation: ["required"]
                                            // },
                                            {
                                                field: "textarea",
                                                type: "",
                                                model: "ecomment",
                                                label: "Тэмдэглэл",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },


                                        ],
                                        buttons: [
                                            {
                                                type: "submit",
                                                value: "Нэмэх",
                                                action: 'add',
                                                url: "/data/equEquipments",
                                                actionModel: 'locationModel',
                                                resultTables: [
                                                    {
                                                        service: "/data/equEquipments",
                                                        model: "dataList",
                                                        // temp: true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }]
            }
        ]},
        {table: [
            {
                url: "/data/equEquipments",
                header: ["Д/д", "Модел","Салбар","Эзэмшигч","Сериал","Үйлдвэрлэсэн он","Ашиглаж эхлэсэн огноо","Эвдрэлтэй эсэх","Тэмдэглэл"],
                editable: true,
                dataList: 'dataList',
                bodyModels: ["id","modelId","branchId","userId","serialNo","buildYear","usedDate","isNotUse","ecomment"],
                itemSize: 20
            }
        ]},
    ];

    //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
    templateLoad('RegistrationEquipment', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);

    $scope.add = function() {
        console.log("add");
    };
}]);
