angular.module('registration').controller('RegistrationUser', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal","notificationService",
    function($http, $location, $scope, $compile, $rootScope, $modal,notificationService) {

    $scope.JSONtemplate = [
        {
            mainModel: "tdrsUser"
        },
        {form: [
            {
                url: "/data/save",
                model: "register12",
                method: "post",
                fieldsColumn: 2,
                fields: [
                    {
                        field: "input",
                        type: "text",
                        model: "id",
                        label: "Дугаар",
                        value: "",
                        select: "",
                        disable: false,
                        validation: [""]
                    },
                    {
                        field: "input",
                        type: "text",
                        model: "fullname",
                        label: "Нэр",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    }
                    ,{
                        field: "input",
                        type: "text",
                        model: "loginname",
                        label: "Нэвтрэх нэр",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    }
                   , {
                        field: "select",
                        type: "",
                        model: "branch.id",
                        label: "Салбар",
                        value: "",
                        select: "/sysBranches",
                        selectValue: {
                            id: "id",
                            name: "name"
                        },
                        disable: false,
                        validation: []
                    },
                    {
                        field: "select",
                        type: "",
                        model: "group.id",
                        label: "Эрхийн түвшин",
                        value: "",
                        select: "/sysGroups",
                        selectValue: {
                            id: "id",
                            name: "name"
                        },
                        disable: false,
                        validation: []
                    }
                ],
                buttons: [
                    {
                        type: "submit",
                        value: "хайх",
                        action: 'search',
                        actionModel: 'RegistrationUpdateList',
                        resultTables: [
                        {
                            service: "/data/sysUsers",
                            model: "dataList"
                        }
                    ]
                },
                    {
                        type: "button",
                        value: "нэмэх",
                        class: "btn btn-success",
                        action: '',
                        modal: [
                            {
                                form: [
                                    {
                                        url: "/",
                                        model: "locationModel",
                                        method: "post",
                                        defaultValues: {
                                            data: {},
                                            url: ""
                                        },
                                        fieldsColumn: 2,
                                        fields: [
                                            {
                                                field: "input",
                                                type: "checkbox",
                                                model: "status",
                                                label: "Төлөв",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: []
                                            },
                                            {
                                                field: "select",
                                                type: "",
                                                model: "branchId",
                                                label: "Салбар",
                                                value: "",
                                                select: "/sysBranches",
                                                selectValue: {
                                                    id: "id",
                                                    name: "name"
                                                },
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "select",
                                                type: "",
                                                model: "groupId",
                                                label: "Эрхийн түвшин",
                                                value: "",
                                                select: "/sysGroups",
                                                selectValue: {
                                                    id: "id",
                                                    name: "name"
                                                },
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "fullname",
                                                label: "Нэр",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "loginname",
                                                label: "Нэвтрэх нэр",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            //
                                            // {
                                            //     field: "input",
                                            //     type: "Date",
                                            //     model: "registerDate",
                                            //     label: "Бүртгүүлсэн огноо",
                                            //     value: "",
                                            //     select: "",
                                            //     disable: false,
                                            //     validation: ["required"]
                                            // },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "password",
                                                label: "Нууц үг",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                        ],
                                        buttons: [
                                            {
                                                type: "submit",
                                                value: "Нэмэх",
                                                action: 'add',
                                                url: "/data/sysUsers",
                                                actionModel: 'locationModel',
                                                resultTables: [
                                                    {
                                                        service: "/data/sysUsers",
                                                        model: "dataList",
                                                        // temp: true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }]
            },

        ]},
        {table: [
            {
                url: "/data/sysUsers",
                header: ["Д/д", "Салбар", "Нэр", "Нэвтрэх нэр", "Эрхийн түвшин", "Төлөв"],
                editable: true,
                modal: [
                    {
                        form: [
                            {
                                url: "/",
                                model: "locationModel",
                                method: "post",
                                defaultValues: {
                                    data: {},
                                    url: "",
                                    fromTable: true
                                },
                                fieldsColumn: 2,
                                fields: [
                                    {
                                        field: "input",
                                        type: "checkbox",
                                        model: "status",
                                        label: "Төлөв",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: []
                                    },
                                    {
                                        field: "select",
                                        type: "",
                                        model: "branch",
                                        label: "Салбар",
                                        value: "",
                                        select: "/sysBranches",
                                        selectValue: {
                                            replaceValues: [
                                                {
                                                    sourceModel: "branches.id",
                                                    replaceModel: "name"
                                                }
                                            ],
                                            id: "id",
                                            name: "name"
                                        },
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "select",
                                        type: "",
                                        model: "group",
                                        label: "Эрхийн түвшин",
                                        value: "",
                                        select: "/sysGroups",
                                        selectValue: {
                                            replaceValues: [
                                                {
                                                    sourceModel: "group.id",
                                                    replaceModel: "name"
                                                }
                                            ],
                                            id: "id",
                                            name: "name"
                                        },
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "fullname",
                                        label: "Нэр",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "loginname",
                                        label: "Нэвтрэх нэр",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },

                                    // {
                                    //     field: "input",
                                    //     type: "Date",
                                    //     model: "registerDate",
                                    //     label: "Бүртгүүлсэн огноо",
                                    //     value: "",
                                    //     select: "",
                                    //     disable: false,
                                    //     validation: ["required"]
                                    // },
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "password",
                                        label: "Нууц үг",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                ],
                                buttons: [
                                    {
                                        type: "submit",
                                        value: "Хадгалах",
                                        action: 'update',
                                        url: "/data/sysUsers/",
                                        actionModel: 'locationModel',
                                        resultTables: [
                                            {
                                                service: "/data/sysUsers",
                                                model: "dataList",
                                                // temp: true
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ],
                dataList: 'dataList',
                bodyModels: ["id","branch.name","fullname","loginname","group.name","status"],
                // param: {
                //     params: [
                //         {
                //             name: "id",
                //             value: $scope.id,
                //             operator: "="
                //         }
                //     ]
                // },
                pageIndex: 1,
                itemSize: 20
            }
        ]},
    ];

    //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
    templateLoad('RegistrationUser', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);

    $scope.add = function() {
        console.log("add");
    };
    $rootScope.$on("notificationServiceSuccess", function(event, data) {
            console.log("success");
            notificationService.notify({
                title: "Амжилттай",
                text: data,
                type: 'success',
                hide: false,
                closer: true,
                styling: 'bootstrap3'
            });
        });
    $rootScope.$on("notificationServiceError", function(event, data) {
            console.log("FAILS");
            notificationService.notify({
                title: "Амжилтгүй",
                text: data,
                type: 'error',
                hide: false,
                closer: true,
                styling: 'bootstrap3'
            });
        });
}]);
