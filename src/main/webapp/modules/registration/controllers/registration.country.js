angular.module('registration').controller('RegistrationCountry', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal","notificationService",
        function($http, $location, $scope, $compile, $rootScope, $modal,notificationService) {

            $scope.JSONtemplate = [
                {
                    mainModel: "tdrsCountry"
                },
                {form: [
                    {
                        url: "/data/waitlist",
                        model: "RegWaitlist",
                        method: "get",
                        fieldsColumn: 2,
                        fields: [
                            {
                                field: "input",
                                type: "text",
                                model: "code",
                                label: "Товчлол",
                                value: "4124125",
                                select: "",
                                disable: false,
                                validation: []
                            },

                            {
                                field: "input",
                                type: "text",
                                model: "status",
                                label: "Төлөв",
                                value: "",
                                select: "/list",
                                disable: false,
                                validation: []
                            },
                            {
                                field: "input",
                                type: "text",
                                model: "name",
                                label: "Нэр",
                                value: "4124125",
                                select: "",
                                disable: false,
                                validation: []
                            }
                        ],
                        buttons: [
                            {
                                type: "button",
                                value: "хайх",
                                action: 'search',
                                actionModel: 'RegistrationCountryList',
                                resultTables: [
                                    {
                                        service: "/data/refCountries",
                                        model: "dataList1"
                                    }
                                ]
                            },
                            {
                                type: "button",
                                value: "нэмэх",
                                class: "btn btn-success",
                                action: '',
                                modal: [
                                    {
                                        form: [
                                            {
                                                url: "/",
                                                model: "countryModel",
                                                method: "post",
                                                defaultValues: {
                                                    data: {},
                                                    url: ""
                                                },
                                                fieldsColumn: 1,
                                                fields: [
                                                    {
                                                        field: "input",
                                                        type: "text",
                                                        model: "name",
                                                        label: "Нэр",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: ["required","text"]
                                                    },
                                                    {
                                                        field: "input",
                                                        type: "text",
                                                        model: "code",
                                                        label: "Товчлол",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: ["required","text"]
                                                    },
                                                    {
                                                        field: "input",
                                                        type: "checkbox",
                                                        model: "status",
                                                        label: "Идэвхитэй эсэх",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: []
                                                    }
                                                ],
                                                buttons: [
                                                    {
                                                        type: "submit",
                                                        value: "Нэмэх",
                                                        action: 'add',
                                                        url: "/data/refCountries",
                                                        actionModel: 'countryModel',
                                                        resultTables: [
                                                            {
                                                                service: "/data/refCountries",
                                                                model: "dataList1",
                                                                // temp: true
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]},
                {table: [
                    {
                        url: "/data/refCountries",
                        header: ["Д/д", "Товчлол", "Нэр", "Төлөв"],
                        editable: true,
                        modal: [
                            {
                                form: [
                                    {
                                        url: "/",
                                        model: "countryModel",
                                        method: "post",
                                        defaultValues: {
                                            data: {},
                                            url: "",
                                            fromTable:true
                                        },
                                        fieldsColumn: 1,
                                        fields: [
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "name",
                                                label: "Нэр",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required","text"]
                                            },
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "code",
                                                label: "Товчлол",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required","number"]
                                            },
                                            {
                                                field: "input",
                                                type: "checkbox",
                                                model: "status",
                                                label: "Идэвхитэй эсэх",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: []
                                            }
                                        ],
                                        buttons: [
                                            {
                                                type: "submit",
                                                value: "Хадгалах",
                                                action: 'update',
                                                url: "/data/refCountries/",
                                                actionModel: 'countryModel',
                                                resultTables: [
                                                    {
                                                        service: "/data/refCountries",
                                                        model: "dataList1",
                                                        // temp: true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        dataList: 'dataList1', //TODO: list model
                        bodyModels: ["id", "code", "name", "status"],
                        pageIndex: 0,
                        itemSize: 10
                    }
                ]}
            ];

            // $scope.subComponentLoad = function(data) {
            //
            // };

            // $rootScope.templateLoad('data', $scope.JSONtemplate);


            //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
            templateLoad('RegistrationCountry', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);
            $rootScope.$on("notificationServiceSuccess", function(event, data) {
                console.log("success");
                notificationService.notify({
                    title: "Амжилттай",
                    text: data,
                    type: 'success',
                    hide: false,
                    closer: true,
                    styling: 'bootstrap3'
                });
            });
            $rootScope.$on("notificationServiceError", function(event, data) {
                console.log("FAILS");
                notificationService.notify({
                    title: "Амжилтгүй",
                    text: data,
                    type: 'error',
                    hide: false,
                    closer: true,
                    styling: 'bootstrap3'
                });
            });
            $scope.az2 = {};
            $scope.az2.demomodel = "123124124";
            $scope.aann = "АОЙРЛБОЙ ХХК";

            $scope.add = function() {
                console.log("add");
            };

            $scope.remove = function() {
                console.log("remove");
            };

            // $scope.submitForm = function(isValid) {
            //
            //     // check to make sure the form is completely valid
            //     console.log(isValid);
            //     if (isValid) {
            //         alert('our form is amazing');
            //     } else {
            //
            //     }
            //
            // };

            $rootScope.modalUrl = "/aaa/bbb";
        }]);
