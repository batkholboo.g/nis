angular.module('registration').controller('RegistrationCity', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal","notificationService",
        function($http, $location, $scope, $compile, $rootScope, $modal,notificationService) {

            $scope.JSONtemplate = [
                {
                    mainModel: "tdrsCity"
                },
                {form: [
                    {
                        url: "/data/waitlist",
                        model: "RegWaitlist",
                        method: "get",
                        fieldsColumn: 2,
                        fields: [
                            {
                                field: "input",
                                type: "text",
                                model: "rd",
                                label: "Код",
                                value: "4124125",
                                select: "",
                                disable: false,
                                validation: ["required"]
                            },

                            {
                                field: "input",
                                type: "text",
                                model: "cbLegalStatusType",
                                label: "Төлөв",
                                value: "",
                                select: "/list",
                                disable: false,
                                validation: []
                            },
                            {
                                field: "input",
                                type: "text",
                                model: "rd1",
                                label: "Нэр",
                                value: "4124125",
                                select: "",
                                disable: false,
                                validation: ["required", "number"]
                            }
                        ],
                        buttons: [
                            {
                                type: "button",
                                value: "хайх",
                                action: 'search',
                                actionModel: 'RegistrationCityList',
                                resultTables: [
                                    {
                                        service: "/data/refAimagCities",
                                        model: "dataList1"
                                    }
                                ]
                            },
                            {
                                type: "button",
                                value: "нэмэх",
                                class: "btn btn-success",
                                action: '',
                                modal: [
                                    {
                                        form: [
                                            {
                                                url: "/",
                                                model: "countryModel",
                                                method: "post",
                                                defaultValues: {
                                                    data: {},
                                                    url: ""
                                                },
                                                fieldsColumn: 1,
                                                fields: [
                                                    {
                                                        field: "input",
                                                        type: "text",
                                                        model: "name",
                                                        label: "Нэр",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: []
                                                    },
                                                    {
                                                        field: "input",
                                                        type: "text",
                                                        model: "code",
                                                        label: "Товчлол",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: []
                                                    },
                                                    {
                                                        field: "input",
                                                        type: "checkbox",
                                                        model: "status",
                                                        label: "Идэвхитэй эсэх",
                                                        value: "",
                                                        select: "",
                                                        disable: false,
                                                        validation: []
                                                    }
                                                ],
                                                buttons: [
                                                    {
                                                        type: "submit",
                                                        value: "Нэмэх",
                                                        action: 'add',
                                                        url: "/data/refAimagCities",
                                                        actionModel: 'cityModel',
                                                        resultTables: [
                                                            {
                                                                service: "/data/refAimagCities",
                                                                model: "dataList1",
                                                                // temp: true
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }

                        ]
                    }
                ]},
                {table: [
                    {
                        url: "/data/refAimagCities",
                        header: ["Д/д", "Код", "Нэр", "Төлөв"],
                        editable: true,
                        dataList: 'dataList1', //TODO: list model
                        bodyModels: ["id", "code", "name", "status"],
                        pageIndex: 0,
                        itemSize: 10
                    }
                ]}
            ];

            // $scope.subComponentLoad = function(data) {
            //
            // };

            // $rootScope.templateLoad('data', $scope.JSONtemplate);

            $scope.aregister12 = {};

            $rootScope.$on('selectList', function (event, data) {
                console.log(data);
                $scope.aregister12.ttd = data['2'];
                $scope.aregister12.rd = data['2'];
                $scope.aregister12.aann = data['3'];
            });

            //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
            templateLoad('RegistrationCity', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);

            $scope.az2 = {};
            $scope.az2.demomodel = "123124124";
            $scope.aann = "АОЙРЛБОЙ ХХК";

            $scope.add = function() {
                console.log("add");
            };

            $scope.remove = function() {
                console.log("remove");
            };

            $scope.submitForm = function(isValid) {

                // check to make sure the form is completely valid
                console.log(isValid);
                if (isValid) {

                } else {

                }

            };

            $rootScope.modalUrl = "/aaa/bbb";

            // $scope.url = function(url){
            //     $location.path(url);
            //     if (url == '/debtcall_home') {
            //         $cookies.debtCallPage = 0;
            //     }
            // };
            //
            // $scope.openNewTabUrl = function(url) {
            //     window.open(url);
            // };

            // $scope.relativeDetail = function(id) {
            //     window.location.assign("#!/registration_relative_detail/2");
            //     console.log(id);
            // }
        }]);
