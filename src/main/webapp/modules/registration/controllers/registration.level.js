angular.module('registration').controller('RegistrationLevel', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal","notificationService",
    function($http, $location, $scope, $compile, $rootScope, $modal,notificationService) {

    $scope.JSONtemplate = [
        {
            mainModel: "tdrsLevel"
        },
        {form: [
            {
                url: "/data/save",
                model: "register12",
                method: "post",
                fieldsColumn: 2,
                fields: [
                    {
                        field: "input",
                        type: "text",
                        model: "id",
                        label: "Дугаар",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    },
                    {
                        field: "input",
                        type: "text",
                        model: "name",
                        label: "Нэр",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    },
                    {
                        field: "input",
                        type: "",
                        model: "status",
                        label: "Төлөв",
                        value: "",
                        select: "/tttbaidal",
                        disable: false,
                        validation: []
                    }

                ],
                buttons: [{
                    type: "button",
                    value: "Хайх",
                    action: 'search',
                    actionModel: 'RegistrationLevelList',
                    resultTables: [
                        {
                            service: "/data/sysLevels",
                            model: "dataList"
                        }
                    ]
                },
                    {
                        type: "button",
                        value: "нэмэх",
                        class: "btn btn-success",
                        action: '',
                        modal: [
                            {
                                form: [
                                    {
                                        url: "/",
                                        model: "levelModel",
                                        method: "post",
                                        defaultValues: {
                                            data: {},
                                            url: ""
                                        },
                                        fieldsColumn: 1,
                                        fields: [
                                            {
                                                field: "input",
                                                type: "text",
                                                model: "name",
                                                label: "Нэр",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: ["required"]
                                            },
                                            {
                                                field: "input",
                                                type: "checkbox",
                                                model: "status",
                                                label: "Идэвхитэй эсэх",
                                                value: "",
                                                select: "",
                                                disable: false,
                                                validation: []
                                            }
                                        ],
                                        buttons: [
                                            {
                                                type: "submit",
                                                value: "Нэмэх",
                                                action: 'add',
                                                url: "/data/sysLevels",
                                                actionModel: 'levelModel',
                                                resultTables: [
                                                    {
                                                        service: "/data/sysLevels",
                                                        model: "dataList",
                                                        // temp: true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }]
            }
        ]},
        {table: [
            {
                url: "/data/sysLevels",
                header: ["Д/д", "Нэр","Төлөв"],
                editable: true,
                modal: [
                    {
                        form: [
                            {
                                url: "/",
                                model: "levelModel",
                                method: "post",
                                defaultValues: {
                                    data: {},
                                    url: "",
                                    fromTable: true
                                },
                                fieldsColumn: 1,
                                fields: [
                                    {
                                        field: "input",
                                        type: "text",
                                        model: "name",
                                        label: "Нэр",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: ["required"]
                                    },
                                    {
                                        field: "input",
                                        type: "checkbox",
                                        model: "status",
                                        label: "Идэвхитэй эсэх",
                                        value: "",
                                        select: "",
                                        disable: false,
                                        validation: []
                                    }
                                ],
                                buttons: [
                                    {
                                        type: "submit",
                                        value: "Хадгалах",
                                        action: 'update',
                                        url: "/data/sysLevels/",
                                        actionModel: 'levelModel',
                                        resultTables: [
                                            {
                                                service: "/data/sysLevels",
                                                model: "dataList",
                                                // temp: true
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ],
                dataList: 'dataList',
                bodyModels: ["id","name","status"],
                itemSize: 20
            }
        ]},
    ];

    //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
    templateLoad('RegistrationLevel', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);

        $rootScope.$on("notificationServiceSuccess", function(event, data) {
            console.log("success");
            notificationService.notify({
                title: "Амжилттай",
                text: data,
                type: 'success',
                hide: false,
                closer: true,
                styling: 'bootstrap3'
            });
        });
        $rootScope.$on("notificationServiceError", function(event, data) {
            console.log("FAILS");
            notificationService.notify({
                title: "Амжилтгүй",
                text: data,
                type: 'error',
                hide: false,
                closer: true,
                styling: 'bootstrap3'
            });
        });
    $scope.add = function() {
        console.log("add");
    };
}]);
