'use strict';
// Setting up route
angular.module('service').config(['$stateProvider',
    function($stateProvider) {
        $stateProvider.
        state('service', {
            url: '/service',
            abstract: true,
            templateUrl: 'modules/template/layout.html'
        }).
        state('service.dashboard', {
            url: '',
            templateUrl: 'modules/service/views/service.user.html',
            controller: 'ServiceUser'
        }).
        state('report.umchlul12', {
            url: '/umchlul1',
            templateUrl: 'modules/report/views/report.umchlul.html',
            controller: 'ReportUmchlul'
        })
        ;
    }
]);

