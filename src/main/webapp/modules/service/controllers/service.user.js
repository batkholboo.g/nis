angular.module('service').controller('ServiceUser', ["$http", "$location", "$scope", "$compile", "$rootScope", "$modal",
    function($http, $location, $scope, $compile, $rootScope, $modal) {

    $scope.JSONtemplate = [
        {form: [
            {
                url: "/data/save",
                model: "register12",
                method: "post",
                fieldsColumn: 2,
                fields: [
                    {
                        field: "input",
                        type: "date",
                        model: "ttd",
                        label: "Эхлэх огноо",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    },
                    {
                        field: "select",
                        type: "text",
                        model: "ttt",
                        label: "Салбар",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    },
                    {
                        field: "input",
                        type: "date",
                        model: "ttt",
                        label: "Дуусах огноо",
                        value: "",
                        select: "",
                        disable: false,
                        validation: []
                    }

                ],
                buttons: []
            },
            {
                url: "",
                model: "a1",
                method: "",
                fieldsColumn: 2,
                fields: [],
                buttons: [
                    {
                        type: "button",
                        value: "Хайх",
                        action: 'ubeghaih()'
                    }
                ]
            }
        ]},
        {table: [
            {
                url: "/sysOrders/?page=0&size=10",
                header: ["Д/д", "Салбар", "Захиалсан огноо", "Суудал", "Ачаа","Автобус","Тусгай","Механизм","Чиргүүл","Мотоцикл","Захиалагчийн нэр","Захиалагчийн утас"],
                editable: true,
                dataList: 'dataList',
                bodyModels: ["id","branchId","fullname","loginname","levelId","status","levelId","status","levelId","status","levelId"],
                itemSize: 20
            }
        ]},
        // {accordion: [
        //     {
        //         title: 'Хаягийн бүртгэл',
        //         data: [
        //             {form: [
        //                 {
        //                     url: "",
        //                     model: "z2",
        //                     method: "dd",
        //                     fieldsColumn: 2,
        //                     fields: [],
        //                     buttons: [
        //                         {
        //                             type: "button",
        //                             value: "нэмэх",
        //                             action: 'addressAdd()'
        //                         }
        //                     ]
        //                 }
        //             ]},
        //             {table: [
        //                 {
        //                     url: "/data/table",
        //                     header: ["Хаягийн төрөл", "Хаяг", "Шуудангийн хайрцаг", "Үндсэн хаяг", "Бүртгүүлсэн огноо", "Бүртгэл дуусгавар болсон огноо"],
        //                     editable: true,
        //                     dataList: 'dataList',
        //                     bodyModels: [1,2,3,4,5,6],
        //                     itemSize: 20
        //                 }
        //             ]}
        //         ]
        //     },
        //     // {
        //     //     title: 'Холбоо барих этгээдийн бүртгэл',
        //     //     data: [
        //     //         {form: [
        //     //             {
        //     //                 url: "",
        //     //                 model: "d2",
        //     //                 method: "aa",
        //     //                 fieldsColumn: 2,
        //     //                 fields: [],
        //     //                 buttons: [
        //     //                     {
        //     //                         type: "button",
        //     //                         value: "нэмэх",
        //     //                         action: ''
        //     //                     }
        //     //                 ]
        //     //             }
        //     //         ]},
        //     //         {table: [
        //     //             {
        //     //                 url: "/data/table",
        //     //                 header: ["Холбоо барих этгээдийн нэр", "Татвар төлөгчийн юу болох", "Холбоо барих хэрэгсэл", "Утасны дугаар,факс, цахим шуудан", "Бүртгүүлсэн огноо", "Бүртгэл дуусгавар болсон огноо", "Үндсэн эсэх"],
        //     //                 editable: true,
        //     //                 dataList: 'dataList',
        //     //                 bodyModels: [1,2,3,4,5,6,7],
        //     //                 itemSize: 20
        //     //             }
        //     //         ]}
        //     //     ]
        //     // }
        // ]}
    ];

    //TODO: $scope.JSONtemplate-g service-s duudna service amjiltt bolvol template load ajilluulna
    templateLoad('ServiceUser', $scope.JSONtemplate, $scope, $rootScope, $compile, $modal, $http, true);

    $scope.add = function() {
        console.log("add");
    };
}]);
