angular.module('standard_elements', []).controller('standard_elements', function($rootScope, $http, $scope, $location) {
	//var self = this;
	//$rootScope.$emit('menu', ['standard', 'forms']);

    $scope.url = function(url){
        $location.path(url);
    };

    // Accordion
    $(document).ready(function() {
        $(".expand").on("click", function () {
            $(this).next().slideToggle(200);
            $expand = $(this).find(">:first-child");

            if ($expand.text() == "+") {
                $expand.text("-");
            } else {
                $expand.text("+");
            }
        });
    });

    // Tooltip
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });
    });

    $scope.showModal = function (id) {
        $('#' + id).modal('show');
    };
});
