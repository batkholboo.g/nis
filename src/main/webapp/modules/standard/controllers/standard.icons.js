angular.module('standard_icons', []).controller('standard_icons', function($rootScope, $http, $scope, $location) {
	//var self = this;
	//$rootScope.$emit('menu', ['standard', 'forms']);

    $scope.url = function(url){
        $location.path(url);
    };

    $scope.showModal = function (id) {
        $('#' + id).modal('show');
    };
});
