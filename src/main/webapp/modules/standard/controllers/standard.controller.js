angular.module('standard', []).controller('StandardController', function($http, $location, $scope, $rootScope) {
    //var self = this;
    console.log('standard');
    $rootScope.$emit('menu', ['home', 'home']);

    $scope.url = function(url){
        $location.path(url);
    };

    $scope.showModal = function (id) {
        $('#' + id).modal('show');
    };
});
