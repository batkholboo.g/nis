angular.module('standard_tables', []).controller('standard_tables', function($rootScope, $http, $scope, $location) {
	//var self = this;
	//$rootScope.$emit('menu', ['standard', 'forms']);

    $scope.url = function(url){
        $location.path(url);
    };

    $scope.showModal = function (id) {
        $('#' + id).modal('show');
    };
});
