angular.module('standard_forms', []).controller('standard_forms', function($rootScope, $http, $scope, $location) {
	//var self = this;
	//$rootScope.$emit('menu', ['standard', 'forms']);
    $scope.url = function(url){
        $location.path(url);
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        height: 500
    };

    $scope.validate = function () {
        if (formValidBeforeSubmit('validate1', '', '') == false) {
            //formvalidSearch = false;
        }
    };



    // Bar chart

    if ($('#mybarChart').length ){

        var ctx = document.getElementById("mybarChart");
        var mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: 'data 1',
                    backgroundColor: "#26B99A",
                    data: [51, 30, 40, 28, 92, 50, 45]
                }, {
                    label: 'data 2',
                    backgroundColor: "#03586A",
                    data: [41, 56, 25, 48, 72, 34, 12]
                }]
            },

            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

    }


    // Pie chart
    if ($('#pieChart').length ){

        var ctx = document.getElementById("pieChart");
        var data = {
            datasets: [{
                data: [120, 50, 140, 180, 100],
                backgroundColor: [
                    "#455C73",
                    "#9B59B6",
                    "#BDC3C7",
                    "#26B99A",
                    "#3498DB"
                ],
                label: 'My dataset' // for legend
            }],
            labels: [
                "data 1",
                "data 2",
                "data 3",
                "data 4",
                "data 5"
            ]
        };

        var pieChart = new Chart(ctx, {
            data: data,
            type: 'pie',
            otpions: {
                legend: false
            }
        });

    }

    $scope.officeList = [
        { code: '01', name: 'Архангай' },
        { code: '02', name: 'Баян-Өлгий' },
        { code: '04', name: 'Булган' },
        { code: '03', name: 'Баянхонгор' },
        { code: '05', name: 'Говь-Алтай' },
        { code: '06', name: 'Дорноговь' },
        { code: '08', name: 'Дундговь' },
        { code: '07', name: 'Дорнод' },
        { code: '09', name: 'Завхан' },
        { code: '10', name: 'Өвөрхангай' },
        { code: '11', name: 'Өмнөговь' },
        { code: '12', name: 'Сүхбаатар' },
        { code: '13', name: 'Сэлэнгэ' },
        { code: '14', name: 'Төв' },
        { code: '15', name: 'Увс' },
        { code: '16', name: 'Ховд' },
        { code: '17', name: 'Хөвсгөл' },
        { code: '18', name: 'Хэнтий' },
        { code: '19', name: 'Дархан-Уул' },
        { code: '20', name: 'Орхон' },
        { code: '21', name: 'ҮТЕГазар' },
        { code: '25', name: 'Сүхбаатар дүүрэг' },
        { code: '24', name: 'Баянзүрх' },
        { code: '27', name: 'Багануур' },
        { code: '35', name: 'Чингэлтэй' },
        { code: '34', name: 'Сонгинохайрхан' },
        { code: '23', name: 'Хан-Уул' },
        { code: '29', name: 'Налайх' },
        { code: '26', name: 'Баянгол' },
        { code: '28', name: 'Багахангай' },
        { code: '22', name: 'Нийслэл' },
        { code: '32', name: 'Говьсүмбэр' },
        { code: '36', name: 'ТЕГ' }
    ]
});
