'use strict';
// Setting up route
angular.module('standard').config(['$stateProvider',
    function($stateProvider) {
        $stateProvider.
        state('standard', {
            url: '/standard',
            abstract: true,
            templateUrl: 'modules/template/layout.html'
        }).
        state('standard.dashboard', {
            url: '',
            templateUrl: 'modules/standard/views/standard.html',
            controller: 'StandardController'
        });
    }
]);

