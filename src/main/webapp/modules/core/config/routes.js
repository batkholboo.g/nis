'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found

		$urlRouterProvider.otherwise('/home');


		// Home state routing
		$stateProvider.
			state('home', {
				url: '/home',
            	abstract: true,
				templateUrl: 'modules/template/layout.html',
			}).
			state('home.dashboard', {
				url: '',
				templateUrl: 'modules/core/views/home.html',
				controller: 'HomeController'
			}).
			state('test', {
				url: '/test',
				templateUrl: 'modules/core/views/test.html',
				controller: 'HomeController'
			});
	}
]);
