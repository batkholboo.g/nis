'use strict';

angular.module('core').controller('SidebarController', ['$rootScope', '$scope', '$http', '$cookies', '$location',
	function($rootScope, $scope, $http, $cookies, $location) {
        $scope.sidemenus = [];

        $rootScope.$on("$locationChangeStart", function(event, next, current) {
            $scope.setMenuDefination($scope.sidemenus);
        });

        $scope.setSideMenu = function(menuId) {
            if($scope.sidemenus.length == 0) {
                $http.get('dist/sidemenu' + menuId + '.json').success(function (data) {
                    $scope.setMenuDefination( data.dtoList)
                });
            }
        }

        $scope.setMenuDefination = function(sidemenus) {
            $scope.sidemenus = [];
            for (var itemIndex in sidemenus) {

                if ('/'+sidemenus[itemIndex].link === $location.path()) {
                    sidemenus[itemIndex].active = true;
                } else {
                    sidemenus[itemIndex].active = false
                }

                for (var subitemIndex in sidemenus[itemIndex].items) {
                    sidemenus[itemIndex].type = "dropdown";
                    if ('/'+sidemenus[itemIndex].items[subitemIndex].link === $location.path()) {
                        sidemenus[itemIndex].items[subitemIndex].active = true;
                        sidemenus[itemIndex].active = true;
                    } else {
                        sidemenus[itemIndex].items[subitemIndex].active = true;
                    }
                }
            }
            $scope.sidemenus = sidemenus;
        }

        $scope.setSideMenu($cookies.menuId);
	}
]);