angular.module('core').controller('modalController', ["$scope", "$rootScope", "$modalInstance", "$modal", "$compile", "$http", "notificationService",
    function ($scope, $rootScope, $modalInstance, $modal, $compile, $http, notificationService) {

        var params = "";

        $scope.ok = function () {
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.modalSearch = {};

        // $scope.tableTotalItems = {};
        // $scope.tableTotalItems["dataList33"] = 33;

        setTimeout(function() {
            console.log($rootScope.selData);
            console.log("load");
            templateLoad('dataModal', selModaData, $scope, $rootScope, $compile, null, $http, true, notificationService);
        }, 10);

        $scope.selectTable = function(model, data) {
            console.log(model, data);

            $rootScope.$emit('selectTable', {model: model, data: data});
            $scope.cancel();
        };

        $scope.addData = function(serviceUrl, data) {
            console.log(serviceUrl, data);
            $rootScope.$emit('addData', {service: serviceUrl, data: data});
        };

        //TODO: modal list unshix service-s
        console.log("modal list: ", $rootScope.modalUrl);

    }
]);