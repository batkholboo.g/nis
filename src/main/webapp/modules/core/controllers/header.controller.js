'use strict';

angular.module('core').controller('HeaderController', ['$rootScope', '$scope', '$http', '$location', '$window', '$cookies', '$localStorage',
	function($rootScope, $scope, $http, $location, $window, $cookies, $localStorage) {
		$scope.currentUser = $localStorage.currentUser;
		console.log($scope.currentUser);
        $scope.topmenus = [];

        console.log("HeaderController load")

        $http.get('dist/topmenu.json').
            success(function(data) {
                $scope.topmenus = data.dtoList;
        });

        $scope.changeUrl = function(menu) {
            $cookies.menuId = menu.id;
            $location.path(menu.link);
        }

        $scope.activeMenu = function(menuId) {
            return $cookies.menuId == menuId
        }


        $scope.logout = function() {
            // remove user from local storage and clear http auth header
            delete $localStorage.currentUser;
            $http.defaults.headers.common.Authorization = '';
            $window.location.href = '/';
        }

	}
]);