angular.module('core').controller('HomeController', ['$scope',
    function ($scope) {

        $scope.schema = {
            type: "object",
            properties: {
                name: { type: "string", minLength: 2, title: "Name", description: "Name or alias" },
                title: {
                    type: "string",
                    enum: ['dr','jr','sir','mrs','mr','NaN','dj']
                }
            }
        };

        $scope.form = [
            "*",
            {
                type: "submit",
                title: "Save"
            }
        ];

        $scope.model = {};

        $scope.chart = AmCharts.makeChart("chartdiv1", {
            "type": "serial",
            "theme": "light",
            "categoryField": "year",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "Tөлөвлөгөө:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "Income",
                    "type": "column",
                    "valueField": "income"
                },
                {
                    "balloonText": "Гүйцэтгэл:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-2",
                    "lineAlpha": 0.2,
                    "title": "Expenses",
                    "type": "column",
                    "valueField": "expenses"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "top",
                    "axisAlpha": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": [
                {
                    "year": "Архангай",
                    "income": 9589,
                    "expenses": 3000
                },
                {
                    "year": "Баян-Өлгий",
                    "income": 9304,
                    "expenses": 2000
                },
                {
                    "year": "Баян-Хонгор",
                    "income": 11200,
                    "expenses": 11000
                },
                {
                    "year": "Булган",
                    "income": 9300,
                    "expenses": 8573
                },
                {
                    "year": "Говь-Алтай",
                    "income": 8573,
                    "expenses": 4000
                },
                {
                    "year": "Говьсүмбэр",
                    "income": 4249,
                    "expenses": 3800
                },
                {
                    "year": "Дархан-Уул",
                    "income": 13760,
                    "expenses": 6000
                },
                {
                    "year": "Дорнод",
                    "income": 10800,
                    "expenses": 9000
                },
                {
                    "year": "Дорноговь",
                    "income": 9000,
                    "expenses": 2000
                },
                {
                    "year": "Дундговь",
                    "income": 8900,
                    "expenses": 3400
                },
                {
                    "year": "Өвөрхангай",
                    "income": 12000,
                    "expenses": 11000
                },
                {
                    "year": "Өмнөговь",
                    "income": 11696,
                    "expenses": 1000
                },
                {
                    "year": "Завхан",
                    "income": 9500,
                    "expenses": 7000
                },
                {
                    "year": "Орхон",
                    "income": 15000,
                    "expenses": 10000
                },
                {
                    "year": "Сүхбаатар",
                    "income": 8838,
                    "expenses": 7000
                },
                {
                    "year": "Сэлэнгэ",
                    "income": 16400,
                    "expenses": 2000
                },
                {
                    "year": "Төв",
                    "income": 12500,
                    "expenses": 2500
                },
                {
                    "year": "Увс",
                    "income": 8300,
                    "expenses": 1500
                },
                {
                    "year": "Хөвсгөл",
                    "income": 15075,
                    "expenses": 10000
                },
                {
                    "year": "Ховд",
                    "income": 9600,
                    "expenses": 8000
                },
                {
                    "year": "Хэнтий",
                    "income": 9902,
                    "expenses": 4000
                },
                {
                    "year": "Замын-Үүд",
                    "income": 5724,
                    "expenses": 50000
                },
                {
                    "year": "Цогтцэций",
                    "income": 14000,
                    "expenses": 9000
                },
                {
                    "year": "Баянгол",
                    "income": 83200,
                    "expenses": 70000
                },
                {
                    "year": "Баянзүрх",
                    "income": 86000,
                    "expenses": 50000
                },
                {
                    "year": "Сонгинохайрхан",
                    "income": 89900,
                    "expenses": 36000
                }
            ],
            // "export": {
            //     "enabled": true
            // }

        });

        /**
         * This demo uses our own method of determining user's location
         * It is not public web service that you can use
         * You'll need to find your own. We recommend http://www.maxmind.com
         */
         // $scope.defaultMap = "mongoliaLow";

        // calculate which map to be used
        // $scope.currentMap = "mongoliaLow";
        var titles = [];
        $scope.map = AmCharts.makeChart( "chartdiv", {
                "type": "map",
                "theme": "light",
                "colorSteps": 10,
                "dataProvider": {
                    "mapURL": "img/" + "mongoliaHigh" + ".svg",
                    "getAreasFromMap": true,
                    "zoomLevel": 0.9,
                    "areas": []
                },
                "areasSettings": {
                    "autoZoom": true,
                    "balloonText": "[[title]]:<strong> [[value]]</strong>"
                },
                "valueLegend": {
                    "right": 10,
                    "minValue": "бага",
                    "maxValue": "их"
                },
                "zoomControl": {
                    "minZoomLevel": 0.9
                },
                "titles": titles,
                "listeners": [ {
                    "event": "init",
                    "method": updateHeatmap
                } ]
            } );


            function updateHeatmap( event ) {
                var map = event.chart;
                if ( map.dataGenerated )
                    return;
                if ( map.dataProvider.areas.length === 0 ) {
                    setTimeout( updateHeatmap, 100 );
                    return;
                }
                for ( var i = 0; i < map.dataProvider.areas.length; i++ ) {
                    map.dataProvider.areas[ i ].value = Math.round( Math.random() * 10000 );
                }
                map.dataGenerated = true;
                map.validateNow();
            }
    }
]).run(['$http', '$rootScope', '$modal', '$compile',
    function($http, $rootScope, $modal, $compile) {

        // $rootScope.loadListModal = function(url) {
        //     if (url) {
        //         console.log("loadList");
        //         // $('#dataList').modal('show');
        //
        //         $modal.open({
        //             templateUrl: '/modules/template/modal.html',
        //             controller: 'modalController'
        //         });
        //     } else {
        //         console.log("not list");
        //     }
        // };
        //
        // $rootScope.templateLoad = function(id, data) {
        //     var templateKeys = Object.keys(data);
        //     // console.log(templateKeys);
        //     // console.log("length: ",templateKeys.length);
        //
        //     for (var i=0; i<templateKeys.length; i++) {
        //         var templateSingleKey = templateKeys[i];
        //         // console.log(templateSingleKey, i);
        //
        //         // console.log($scope.templateSingleKey);
        //         if (templateSingleKey == "form") {
        //
        //             var templateForms = data[templateKeys[i]];
        //
        //             for (var e=0; e<templateForms.length; e++) {
        //                 // console.log(e);
        //
        //                 var templateSingleObj = templateForms[e];
        //
        //                 var templateFormFields = templateSingleObj.fields;
        //                 var formHTML = "<div class='col-md-12'>" +
        //                     "<form class='form-horizontal' name='"+templateSingleObj.model+"' ng-submit='attempted=true; submitForm("+templateSingleObj.model+".$isValid)' novalidate='' role='form'>";//"<form name='form' class='form-horizontal' action='"+templateSingleObj.url+"' method='"+templateSingleObj.method+"' novalidate>";
        //                 for (var a=0; a<templateFormFields.length; a++) {
        //                     var rowStart = "";
        //                     var rowEnd = "";
        //                     console.log(a, templateFormFields.length-1);
        //                     if (a%2 === 0) {
        //                         console.log(a, "neelt");
        //                         rowStart = "<div class='row'>";
        //                         rowEnd = "";
        //                     } else {
        //                         console.log(a, "haalt");
        //                         rowStart = "";
        //                         rowEnd = "</div>";
        //                     }
        //
        //                     if (a == templateFormFields.length-1) {
        //                         rowEnd = "</div>";
        //                     }
        //
        //                     formHTML += rowStart;
        //
        //                     formHTML += "<div class='col-md-5'>" +
        //
        //                         "<div class='form-group' ng-class='{ \"has-error\" : "+templateSingleObj.model+"."+templateFormFields[a].model+".$invalid && attempted }'>" +
        //                         "<label class='col-sm-4 control-label' for='"+templateFormFields[a].model+"'>"+templateFormFields[a].label+":</label>" +
        //                         "<div class='col-sm-8'>";
        //
        //                     if (templateFormFields[a].field == "input") {
        //                         var validations = templateFormFields[a].validation;
        //                         var validation = "";
        //                         if (validations.length > 0) {
        //
        //                             for (var h=0; h<validations.length; h++) {
        //                                 var validation_arr = validations[h].split("-");
        //                                 // console.log(validation_arr);
        //                                 if (validation_arr[0] == "required") {
        //                                     // console.log("required");
        //                                     validation += "required";
        //                                 } else if (validation_arr[0] == "number") {
        //                                     console.log("number");
        //                                     // validation += "";
        //                                 } else if (validation_arr.length == 2 && validation_arr[0] == "length") {
        //                                     // console.log("length");
        //                                     validation += " ng-minlength='"+validation_arr[1]+"' ng-maxlength='"+validation_arr[1]+"'";
        //                                 }
        //                             }
        //                         } else {
        //                             validation = "";
        //                         }
        //
        //                         // console.log(validation);
        //
        //                         formHTML += "<input type='"+templateFormFields[a].type+"' name='"+templateFormFields[a].model+"' id='"+templateFormFields[a].model+"' data-ng-model='a"+templateSingleObj.model+"."+templateFormFields[a].model+"' data-ng-click='loadListModal(\""+templateFormFields[a].select+"\")' ng-disabled="+templateFormFields[a].disable+" class='form-control' "+validation+">"; //popover='Should be between 1 and 10' popover-toggle='"+templateSingleObj.model+"."+templateFormFields[a].model+".$invalid' popover-placement='top'
        //                     } else if (templateFormFields[a].field == "select") {}
        //
        //                     formHTML += "<p ng-if='attempted && "+templateSingleObj.model+"."+templateFormFields[a].model+".$error.required' class='help-block'>"+templateFormFields[a].label+" талбарыг заавал бөглөнө үү.</p>" +
        //                         "<p ng-if='attempted && "+templateSingleObj.model+"."+templateFormFields[a].model+".$error.minlength' class='help-block'>"+templateFormFields[a].label+" дэндүү богино байна.</p>" +
        //                         "<p ng-if='attempted && "+templateSingleObj.model+"."+templateFormFields[a].model+".$error.maxlength' class='help-block'>"+templateFormFields[a].label+" дэндүү урт байна.</p>" +
        //                         "</div>" +
        //                         "</div>" +
        //                         "</div>";
        //                     formHTML += rowEnd;
        //                 }
        //
        //                 // formHTML += "<div class='form-group'>";
        //
        //                 var templateFormButtons = templateSingleObj.buttons;
        //                 for (var d=0; d<templateFormButtons.length; d++) {
        //                     formHTML += "<button type='"+templateFormButtons[d].type+"' style='margin-bottom: 15px !important; margin-right: 15px' class='btn btn-primary'>"+templateFormButtons[d].value+"</button>";
        //                 }
        //
        //                 formHTML += "</form></div>";
        //
        //                 // console.log(id, formHTML);
        //
        //                 angular.element(document.getElementById(id)).append($compile(formHTML)($rootScope));
        //             }
        //
        //         } else if (templateSingleKey == "table") {
        //             var templateTables = data[templateKeys[i]];
        //             // console.log("table " ,id ,templateTables);
        //
        //             for (var e=0; e<templateTables.length; e++) {
        //                 var templateTableSingleObj = templateTables[e];
        //
        //                 // console.log($scope.templateTableSingleObj);
        //                 var tableHTML = "<div class='col-md-12'>";
        //                 tableHTML += "<table class='table table-headrow'>";
        //                 tableHTML += "<thead> <tr> ";
        //
        //                 var tableBodyHTML = "";
        //
        //                 var templateTableHeaders = templateTableSingleObj.header;
        //
        //                 // console.log($scope.templateTableHeaders);
        //
        //                 for (var b=0; b<templateTableHeaders.length; b++) {
        //                     tableHTML += "<th>"+templateTableHeaders[b]+"</th>";
        //                 }
        //
        //                 tableHTML += "<th colspan='2' style='text-align: center;'>Үйлдэл</th>";
        //                 tableHTML += "</tr></thead>";
        //
        //                 var templateTableBodys = templateTableSingleObj.bodyModels;
        //
        //                 for (var c=0; c<templateTableBodys.length; c++) {
        //                     tableBodyHTML += "<td>{{data["+templateTableBodys[c]+"]}}</td>"
        //                 }
        //
        //                 tableBodyHTML += "<td style='text-align: center; width: 40px'> " +
        //                     "<a href='' data-ng-click='relativeDetail(item)'><span class='glyphicon glyphicon-pencil' data-toggle='modal' data-target='.edit-modal-1' style='color: #1f427e;'></span></a> " +
        //                     "</td> " +
        //                     "<td style='text-align: center; width: 40px'> " +
        //                     "<a href='' data-ng-click='noticeRemove(item)'><span class='glyphicon glyphicon-trash' style='color: #ee4823;'></span></a> " +
        //                     "</td>";
        //
        //                 tableHTML += "<tbody><tr data-ng-repeat='data in "+templateTableSingleObj.dataList+"'>" + tableBodyHTML + "</tr></tbody>";
        //
        //                 tableHTML += "</table>";
        //                 tableHTML += "</div>";
        //
        //                 angular.element(document.getElementById(id)).append($compile(tableHTML)($rootScope));
        //
        //                 // console.log(id, templateTables);
        //
        //                 //TODO: $scope.templateSingleObj.url-s table-n service-n avaad ugugdliig unshin $scope.dataList model-d xiine
        //                 $rootScope.dataList = [
        //                     {1:1,2:2,3:3,4:4,5:5,6:6},
        //                     {1:1,2:2,3:3,4:4,5:5,6:6},
        //                     {1:1,2:2,3:3,4:4,5:5,6:6},
        //                     {1:1,2:2,3:3,4:4,5:5,6:6}
        //                 ];
        //
        //                 $rootScope.dataList1 = [
        //                     {1:1,2:2,3:3,4:4,5:5,6:6},
        //                     {1:12,2:22,3:3,4:4,5:5,6:6},
        //                     {1:1,2:2,3:3,4:42,5:5,6:6},
        //                     {1:12,2:2,3:3,4:4,5:5,6:62}
        //                 ];
        //             }
        //
        //         } else if (templateSingleKey == "accordion") {
        //             var array_list = data[templateKeys[i]];
        //             // console.log("array_list",array_list);
        //             var accordionHTML = "<div class='col-md-12'>";
        //             accordionHTML += "<div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>";
        //
        //
        //             for (var f=0; f<array_list.length; f++) {
        //
        //                 accordionHTML += "<div class='panel panel-default'>";
        //                 accordionHTML += "<div class='panel-heading' role='tab' id='heading"+f+"'>";
        //                 accordionHTML += "<h4 class='panel-title'>";
        //                 accordionHTML += "<a role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse"+f+"' aria-expanded='false' aria-controls='collapse"+f+"'>"+array_list[f].title+"</a>";
        //                 accordionHTML += "</h4>";
        //                 accordionHTML += "</div>";
        //                 accordionHTML += "<div id='collapse"+f+"' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading"+f+"'>";
        //                 accordionHTML += "<div class='panel-body'>";
        //                 accordionHTML += "<div id='accordion_"+f+"'></div>";
        //                 accordionHTML += "</div>";
        //                 accordionHTML += "</div>";
        //                 accordionHTML += "</div>";
        //
        //             }
        //
        //             accordionHTML += "</div>";
        //             accordionHTML += "</div>";
        //             angular.element(document.getElementById(id)).append($compile(accordionHTML)($rootScope));
        //
        //             for (var g=0; g<array_list.length; g++) {
        //                 $rootScope.templateLoad('accordion_'+g, array_list[g].data);
        //             }
        //         }
        //     }
        // };

}]);