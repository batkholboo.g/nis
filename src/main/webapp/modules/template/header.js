angular.module('header', ['ngCookies']).controller('header', function($http, $location, $scope, $rootScope, $window, $cookieStore) {
    $scope.menus = [
        {
            topmenu: 'home',
            submenus: []
        },
        {
            topmenu: 'autotax',
            submenus: [
                {
                    name: 'Нүүр хуудас',
                    icon: 'home',
                    link: '/autotax_homesenior'
                },
                {
                    name: 'Баталгаажуулалт хийх',
                    icon: 'ok-circle',
                    link: '/autotax_verifysenior'
                },
                {
                    name: 'Дугааргүй мотоцикл бүртгэл',
                    icon: 'tasks',
                    link: '/autotax_unregvehicles'
                },
                {
                    name: 'Тайлан',
                    icon: 'tasks',
                    link: '/autotax_reports'
                }
            ]
        },
        {
            topmenu: 'calculation',
            submenus: [
                {
                    name: 'Нүүр хуудас',
                    icon: 'home',
                    link: '/calculation_register'
                },
                {
                    name: 'Буцаан олголт',
                    icon: 'repeat',
                    link: '/calculation_refund'
                },
                //{
                //    name: 'Буцаан олголт засварлах',
                //    icon: 'pencil',
                //    link: '/calculation_editrefund'
                //},
                {
                    name: 'Шилжүүлсэн үлдэгдэл оруулах',
                    icon: 'edit',
                    link: '/calculation_transbalance'
                },
                //{
                //    name: 'Шилжүүлсэн үлдэгдэл харах',
                //    icon: 'list-alt',
                //    link: '/calculation_transbalance1'
                //},
                {
                    name: 'Татварын өр хүлээн авах',
                    icon: 'download',
                    link: '/calculation_receivetax'
                },
                {
                    name: 'Тайлангийн ногдол харах',
                    icon: 'usd',
                    link: '/calculation_dividend'
                },
                {
                    name: 'ХШ ногдол',
                    icon: 'inbox',
                    link: '/calculation_debt'
                },
                {
                    name: 'Татварын суутган тооцоолол',
                    icon: 'cog',
                    link: '/calculation_calc'
                }
            ]
        },
        {
            topmenu: 'capital',
            submenus: [
                {
                    name: 'Хөрөнгийн бүртгэл',
                    icon: 'home',
                    link: '/capital_home'
                }
                //{
                //    name: 'Бууны бүртгэл',
                //    icon: 'record',
                //    link: '/capital_gun'
                //}
            ]
        },
        {
            topmenu: 'payment',
            submenus: [
                {
                    name: 'Нүүр хуудас',
                    icon: 'home',
                    link: '/payment_add'
                },
                {
                    name: 'Орлогын гүйлгээний түүх',
                    icon: 'calendar',
                    link: '/payment_list'
                },
                {
                    name: 'Харилцах төсөв',
                    icon: 'tasks',
                    link: '/payment_budget'
                },
                {
                    name: 'Татварын төрөл',
                    icon: 'sort-by-alphabet',
                    link: '/payment_taxtype'
                },
                {
                    name: 'Харилцах дансны мэдээлэл',
                    icon: 'credit-card',
                    link: '/payment_account'
                },
                {
                    name: 'Данс болон татварын төрөл',
                    icon: 'credit-card',
                    link: '/payment_accountandtt'
                },
                {
                    name: 'Төсвийн ангилал татварын төрөл',
                    icon: 'credit-card',
                    link: '/payment_budgetandtt'
                }
            ]
        },
        {
            topmenu: 'debt',
            submenus: [
                {
                    name: 'Тайлан',
                    icon : 'duplicate',
                    link: '/debt_report'
                },
                {
                    name: 'Өрийн тайлан',
                    icon: 'duplicate',
                    link: '/debt_reportxls'
                },
                {
                    name: 'Үйл ажиллагааны бүртгэл',
                    icon: 'copy',
                    link: '/debt_activity'
                },
                {
                    name: 'Мэдэгдэл шаардлага',
                    icon: 'alert',
                    link: '/debt_notice'
                },
                {
                    name: 'Өр барагдуулалт',
                    icon: 'piggy-bank',
                    link: '/debt_payment'
                }
            ]
        },
        {
            topmenu: 'debtcall',
            submenus: [
                {
                    name: 'Нүүр хуудас',
                    icon: 'home',
                    link: '/debtcall_home'
                }
            ]
        },
        {
            topmenu: 'etax',
            submenus: [
                {
                    name: 'Тайлан',
                    icon: 'copy',
                    link: '/etax_home'
                },
                {
                    name: 'Хүсэлт',
                    icon: 'comment',
                    link: '/etax_request'
                },
                {
                    name: 'Татвар төлөгч',
                    icon: 'user',
                    link: '/etax_taxpayer'
                },
                {
                    name: 'Тоон гарын үсэг',
                    icon: 'hand-up',
                    link: '/etax_signature'
                }
            ]
        },
        {
            topmenu: 'standard',
            submenus: [
                {
                    name: 'Нүүр хуудас',
                    icon: 'home',
                    link: '/standard'
                }
            ]
        }

    ];
    //console.log('header');

    $rootScope.$on('menu', function(event, data) {
        //console.log(data);
        $scope.menu = _.findWhere($scope.menus, {topmenu: data[0]});
        $scope.selMenuLink = "/"+data[1];

        $('.nav-tabs li a').click(function (e) {
            e.preventDefault();
            $('a[href="' + $(this).attr('href') + '"]').tab('show');
        })
    });
    $scope.url = function(url){
        $location.path(url);
    };

    $scope.logout = function() {
        $window.location.href = '/auth-logout';
    };

    $rootScope.selectedPayers = $cookieStore.get('selectedTaxPayers');

    $scope.selectPayer = function(item) {
        //$rootScope.$emit('selectedPayer', item);
        $rootScope.selectedPayer =  _.findWhere($cookieStore.get('selectedTaxPayers'), {tripsTin: item.tripsTin}) ;
    };

    $scope.removePayer = function(item) {
        for(var i = $rootScope.selectedPayers.length - 1; i >= 0; i--) {
            if($rootScope.selectedPayers[i].tin === item.tin) {
                $rootScope.selectedPayers.splice(i, 1);
            }
        }

        $cookieStore.put('selectedTaxPayers', $rootScope.selectedPayers);
    };
}).directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});
