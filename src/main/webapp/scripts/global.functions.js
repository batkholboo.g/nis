'use strict';

function replaceAll(str, find, replace) {
    var i = str.indexOf(find);
    if (i > -1) {
        str = str.replace(find, replace);
        i = i + replace.length;
        var st2 = str.substring(i);
        if(st2.indexOf(find) > -1) {
            str = str.substring(0,i) + replaceAll(st2, find, replace);
        }
    }
    return str;
}

$( document ).ready(function() {
    $('.panel-title > a').click(function() {
        $(this).find('i').toggleClass('fa-plus fa-minus')
            .closest('panel').siblings('panel')
            .find('i')
            .removeClass('fa-minus').addClass('fa-plus');
    });
});

function selectService($http, $scope, model, url, params, nextSelect, mainModel, mMainModel) {
    //TODO: url-s duuddag bolgox (service belen bolson ued)
    // console.log(url, params, nextSelect, mainModel, mMainModel);
    // console.log(model, nextSelect);
    // console.log(params);
    var queryString = "";
    if (!_.isEmpty(params)) {
        for (var ts=0; ts<params.params.length; ts++) {
            if (ts == 0) { queryString += "?"; } else { queryString += "&" }
            queryString += "filter="+params.params[ts].name+","+params.params[ts].operator+","+(params.params[ts].value == undefined ? '' : params.params[ts].value);
        }
    }

    $http.get("/data"+url+queryString || {}) //+"?page=0&size=100"
        .success(function (response) {
            var obj = response._embedded;
            // console.log("obj", obj);
            $scope.selectArray[model.toString()] = response._embedded[Object.keys(obj)[0]] || [];
            // console.log($scope.selectArray[model.toString()]);
            // console.log($scope.selectArray, $scope.selectArray["objLocation.countryId"]);
            // console.log($scope.selectArray[model.toString()]);
            if (!_.isEmpty(nextSelect)) {
                // console.log($scope[mMainModel][mainModel]);
                var findRep = _.findWhere(replaceValues, {model: model});
                if (findRep) {
                    // console.log(model, findRep.data[0].sourceModel);
                    model = findRep.data[0].sourceModel;
                }
                console.log(model);
                var modelArray = model.split(".");
                var tempParamV = $scope[mMainModel][mainModel];
                // console.log(tempParamV, modelArray, replaceValues);
                for (var kj22=0; kj22<modelArray.length; kj22++) {
                    // console.log(tempParamV, modelArray, kj22);
                    tempParamV = tempParamV[modelArray[kj22]];
                    if (kj22 == 0) {
                        tempParamV = tempParamV;
                    }
                    // console.log(tempParamV);
                }
                // console.log(modelArray, tempParamV);
                var tempParam = {
                    params: [
                        {
                            name: "parentId",
                            operator: "=",
                            value: tempParamV
                        }
                    ]};
                // console.log(selectWithParams, nextSelect.model);
                var tempParamSub = _.findWhere(selectWithParams, {model: nextSelect.model});
                // console.log(tempParamSub);
                if (tempParamSub != undefined && ("param" in tempParamSub)) {
                    for (var lk=0; lk<tempParamSub.param.params.length; lk++) {
                        tempParam.params.push(tempParamSub.param.params[lk]);
                    }
                }
                // console.log(tempParam);
                var nextSelectSub = {};
                if (!_.isEmpty(tempParamSub)) {
                    nextSelectSub = tempParamSub.onChangeModel
                }
                // console.log(tempParam);
                selectService($http, $scope, nextSelect.model, nextSelect.url, tempParam, nextSelectSub, mainModel, mMainModel);
                // console.log("next");
                // console.log(selectWithParams, $scope[mainModel][model]);
            } else {}

        }).error(function (response) {
        $scope.$emit("notificationServiceError", model + " дуудахад алдаа гарлаа!");
        $scope.selectArray[model] = [];
    });
}

function tableService($http, $scope, mainModel, model, url, page, size, sort,  param, blockUI, nopage) {
    //TODO: url-s duuddag bolgox (service belen bolson ued) && index , size oruulax

    var fndTSL = _.findWhere(tableServiceList, {model: model});

    if (fndTSL != undefined) {
        // console.log("found");
        fndTSL.mainModel = mainModel;
        fndTSL.model = model;
        fndTSL.url = url;
        fndTSL.page = page;
        fndTSL.size = size;
        fndTSL.sort = sort;
        fndTSL.param = param;
        fndTSL.nopage = nopage;
    } else {
        tableServiceList.push({
            mainModel: mainModel,
            model: model,
            url: url,
            page: page,
            size: size,
            sort: sort,
            param: param,
            nopage: nopage
        });
        // console.log("not found");
    }
    // console.log(tableServiceList);

    // if (setModelData["table_"+model] != undefined) {
    //     var repTempModelData = $scope;
    //     var repModelArr = setModelData["table_"+model].split(".");
    //     for (var rm=1; rm<repModelArr.length; rm++) {
    //         repTempModelData = repTempModelData[repModelArr[rm]];
    //     }
    //     console.log($scope[mainModel]);
    //     findAndReplace($scope[mainModel], setModelData["table_"+model], repTempModelData);
    // }

    //TODO: table-m model array-g modal-n controller-lu damjuulj ugux (tegj baij xailt xiixed table-g refresh xiine)
    page = page || 1;
    size = size || 5;

    if (url != "/" && !_.isEmpty(url)) {

        if (nopage != true) {
            url+="?page="+(page-1)+"&size="+size;
            if(sort && typeof sort !== "undefined") {
                url+="&sort="+sort;
            }
        }

        var queryString = "";
        if (!_.isEmpty(param)) {
            for (var ts=0; ts<param.params.length; ts++) {
                if (ts == 0 && nopage == true) { queryString += "?"; } else { queryString += "&" }
                queryString += "filter="+param.params[ts].name+","+param.params[ts].operator+","+(param.params[ts].value == undefined ? '' : param.params[ts].value);
            }
        }


        // console.log(queryString);
        // blockUI.start();
        $http.get(url+queryString) // , param || {}
            .success(function (response) {
                // blockUI.stop();
                // console.log(response);
                var obj = response._embedded;
                // console.log(obj, mainModel, model);
                $scope[mainModel][model] = response._embedded[Object.keys(obj)[0]] || [];
                // console.log($scope[mainModel][model], mainModel, model);
                $scope.tableTotalItems[model] = response.page.totalElements || 0;
                $scope.tablePerItems[model] = size || 5;
                $scope.tableCurrentPage[model] = page || 1;
                $scope.tableCurrentParams[model] = param || {};
                $scope.tableCurrentSort[model] = sort;
            }).error(function (response) {
            // blockUI.stop();
            $scope.$emit("notificationServiceError", model + " дуудахад алдаа гарлаа!");
            $scope[mainModel][model] = [];
            $scope.tableTotalItems[model] = 0;
            $scope.tablePerItems[model] = 5;
            $scope.tableCurrentPage[model] = 0;
            $scope.tableCurrentParams[model] = param || {};
            $scope.tableCurrentSort[model] =null;
        });
    }
}

function postService($http, $scope, url, data, modalClose, model, sModel, $rootScope) {
    // console.log(data, model, _.findWhere(notificationList, {model: model}));
    var notifFound = _.findWhere(notificationList, {model: model});
    $http.post(url, data)
        .success(function(response) {
            // console.log("success", response);
            if (modalClose) {
                $scope.cancel();
            }
            console.log("notifFound", notifFound);

            if (notifFound) {

                $scope.$emit("notificationServiceSuccess", notifFound.data.success);
            } else {
                $scope.$emit("notificationServiceSuccess", url + " амжилттай дуудлаа");
            }

            postAfterService($http, $scope, $rootScope, null, sModel, data);

        })
        .error(function(response) {
            if (notifFound) {
                $scope.$emit("notificationServiceError", notifFound.data.error);
            } else {
                $scope.$emit("notificationServiceError", url + " мэдээлэл нэмэхэд алдаа гарлаа!");
            }
            // $scope.$emit("notificationServiceError", url + " мэдээлэл нэмэхэд алдаа гарлаа!");
            // console.log("error", response);
        });
}

function updateService($http, $scope, url, data, modalClose) {
    $http.put(url, data)
        .success(function(response) {
            if (modalClose) {
                $scope.cancel();
            }
            console.log(response);
            $scope.$emit("notificationServiceSuccess", url + " амжилттай засагдлаа");
        })
        .error(function(response) {
            $scope.$emit("notificationServiceError", url + " засвар хийхэд алдаа гарлаа!");
            console.log(response);
        })
}

function setData($http, $scope, url, model, mainModel) {
    // var tempUrl = "";
    // console.log($scope.selData, url, model, mainModel);
    var tempUrl = url;
    if ($scope.selData != undefined) {
        if (url.slice(-1) == "/") {
            tempUrl = url+$scope.selData;
        }
    }
    // var tempUrl = $scope.selData == undefined ? url : url+$scope.selData;
    $http.get(tempUrl)
        .success(function(response) {
            if ((mainModel in $scope)) {
                // console.log("baina");
            } else {
                $scope[mainModel] = {};
            }


            if (("_embedded" in response)) {
                var obj = response._embedded;
                $scope[mainModel][model] = response._embedded[Object.keys(obj)[0]][0] || {};
                // console.log($scope[mainModel][model]);
            } else {
                $scope[mainModel][model] = response;
                $scope[mainModel][model]["id"] = $scope.selData;
            }


        })
        .error(function(response) {
            $scope.$emit("notificationServiceError", model + " мэдээлэл дуудахад алдаа гарлаа!");
            $scope[mainModel] = {};
            $scope[mainModel][model] = {};
        })
}

function closeMessages(id, $compile, $scope) {

    // $scope.closeAllMessages = function () {
    //     $scope.$emit("notificationServiceClose");
    //     $('.msg-close').fadeOut('slow');
    // };
    //
    // $scope.closeDialog = function () {
    //     $('.msg-close').fadeOut('slow');
    // };
    //
    // var notifyClose = "<div class=\"msg-close\"><div class=\"ui-pnotify ui-pnotify-fade-normal ui-pnotify-in ui-pnotify-fade-in ui-pnotify-move\" aria-live=\"assertive\" aria-role=\"alertdialog\" style=\"display: block; width: 300px; left: 36px; top: 36px;z-index: 100040\"><div class=\"alert ui-pnotify-container alert-warning ui-pnotify-shadow\" role=\"alert\" style=\"min-height: 16px;\"><div class=\"ui-pnotify-sticker\" aria-role=\"button\" aria-pressed=\"true\" tabindex=\"0\" title=\"Unstick\" style=\"cursor: pointer;\"><button style=\"float:left;\" class=\"btn btn-danger\" ng-click=\"closeAllMessages()\">Тийм</button><button style=\"float:right; margin-left:10px;\" class=\"btn btn-primary\" ng-click=\"closeDialog()\">Үгүй</button></div><div class=\"ui-pnotify-icon\"><span class=\"glyphicon glyphicon-warning-sign\"></span></div><h4 class=\"ui-pnotify-title\">Бүх мессежийг хаах уу!</h4><div class=\"ui-pnotify-text\" aria-role=\"alert\"></div></div></div></div>";
    //
    // angular.element(document.getElementById(id)).append($compile(notifyClose)($scope));

}

function postAfterService($http, $scope, $rootScope, blockUI, Model, data) {
    // console.log(Model, data);
    var params = {
        params: []
    };

    var searchResults = searchResult[Model] || [];

    // console.log(searchResults, params, modelName, $scope.tablePerItems, $scope.tablePerItems[modelName]);
    for (var a=0; a<searchResults.length; a++) {
        var tableInitParams = $scope.tableInitParams[searchResults[a].model];
        // console.log($scope.tableInitParams, searchResults[a].model)
        // console.log(tableInitParams);
        if(tableInitParams != undefined && ("params" in tableInitParams)) {
            if (tableInitParams.params) {
                for(var b = 0; b < tableInitParams.params.length; b ++) {
                    params.params.push(tableInitParams.params[b]);
                }
            }
        }

        _.extend(params, data);

        // var nopage = false;

        var tempUrl = searchResults[a].service;
        if (("addUrl" in searchResults[a])) {
            // nopage = true;
            // var tModel = searchResults[a]["addUrl"]["fromModel"];
            // // var scopeTemp = $rootScope;
            // // console.log($rootScope);
            // var tModelArr = tModel.split(".");
            // var tScope = $rootScope;
            // for (var doo=0; doo< tModelArr.length; doo++) {
            //     tScope = tScope[tModelArr[doo]];
            //     // console.log();
            //     // console.log(tModelArr[doo]);
            // }
            // // console.log(tModel.split("."));
            // tempUrl += tScope;

            setData($http, $scope, searchResults[a].service, searchResults[a].model, mainModel);
        } else {
            // console.log(searchResults[a],params);
            tableService($http, $scope, mainModel, searchResults[a].model, tempUrl, $scope.tableCurrentPage[searchResults[a].model], $scope.tablePerItems[searchResults[a].model], $scope.tableCurrentSort[searchResults[a].model], params, blockUI);
        }

        // console.log(searchResults[a]);




    }
}

// }

var modalData = {};
var setModelData = {};
var selModaData = {};
var searchResult = {};
var addResult = {};
var selectWithParams = [];
var mainModel = "";
var replaceValues = [];
var searchDataParams = [];
var notificationList = [];
var panelId = 0;
var tabId = 0;
var maps = [];
var setModelList = [];
var copyModelList = [];
var tabLoadList = [];
var tableServiceList = [];
var tableServiceLoaded = [];

function findAndReplace(object, value, replacevalue) {
    for (var x in object) {
        if (object.hasOwnProperty(x)) {
            if (typeof object[x] == 'object') {
                findAndReplace(object[x], value, replacevalue);
            }
            if (object[x] == value) {
                // object["name"] = replacevalue;
                object[x] = replacevalue;
                // break; // uncomment to stop after first replacement
            }
        }
    }
}

var formStart = false;

function templateLoad(id, data, $scope, $rootScope, $compile, $modal, $http, firstLoad, blockUI) {
    var selectParentList = [];
    var nextID = 0;
    var formStartIndex = 0;

    var tempID = id;
    // notificationService.notice('Хоосон!!!');

    if ($modal != null) {
        $scope.loadModal = function(key, data) {
            // console.log(key, data);

            $rootScope.selData = _.isEmpty(data) ? '' : data.id;
            // console.log($rootScope.selData);
            $rootScope.selDataObject = data;
            // console.log(modalData[key], $scope, $rootScope);
            selModaData = modalData[key];

            if (setModelData[key] != undefined) {
                var repTempModelData = $scope;
                var repModelArr = setModelData[key].split(".");
                for (var rm=1; rm<repModelArr.length; rm++) {
                    repTempModelData = repTempModelData[repModelArr[rm]];
                }
                findAndReplace(modalData[key], setModelData[key], repTempModelData);
            }

            // console.log(repTempModel);
            // console.log(modalData[key]);
            if (1) {
                $modal.open({
                    templateUrl: '/modules/template/modal.html',
                    controller: 'modalController'
                });
            } else {
                console.log("not list");
            }
        };
    }

    if ($scope["submitForm"] == undefined) {

        $scope.form_valid = false;
        $scope.submitForm = function (form) {

            // console.log(1);
            // check to make sure the form is completely valid
            console.log(form);
            if(form.$valid) {
                // Code here if valid
                console.log("valid");
                $scope.form_valid = true;
            } else {
                console.log("invalid");
                $scope.form_valid = false;
            }

        };
    }

    if ($scope["search"] == undefined) {
        $scope.search = function(object, mainModel, modelName, url, buttonValue) {
            if ($scope.form_valid) {

                var params = {
                    params: []
                };

                // console.log(object, modelName, $scope[mainModel][modelName]);
                // console.log(typeof ($scope[mainModel][modelName]));
                if (typeof ($scope[mainModel][modelName]) != "undefined") {
                    // console.log($scope[mainModel][modelName]);
                    var tempModel = $scope[mainModel][modelName];
                    if (typeof (tempModel) != undefined && typeof (tempModel) != null) {
                        // console.log("tempModel", tempModel);
                        var keys = Object.keys(tempModel);
                    }
                    // console.log(keys, value);
                    for (var i=0; i<keys.length; i++) {
                        var key = keys[i];
                        var value = "";
                        // var checkObject = "";
                        if ((typeof tempModel[key]) == "object") {
                            if(tempModel[key] != null){
                                var obj1 = tempModel[key];
                                var key2 = Object.keys(tempModel[key])[0];
                                key = keys[i]+"."+key2;
                                value = tempModel[keys[i]][Object.keys(obj1)[0]];
                                // console.log(object[keys[i]][key2], key2);
                                // checkObject = object[keys[i]][key2];
                                // console.log("object", checkObject, key, value);
                            }
                        } else {
                            // console.log("not object");
                            key = keys[i];
                            value = tempModel[key];
                            // checkObject = object[key];
                            // console.log(key, value, object[key]);
                        }
                        // console.log(value);
                        if (value != null && value != "") {
                            console.log("push", value);
                            params.params.push({
                                name: key,
                                value: value,
                                operator: "="
                            });
                        }
                    }
                }

                var searchResults = searchResult[modelName+buttonValue] || [];

                // console.log(searchResults, params, modelName, $scope.tablePerItems, $scope.tablePerItems[modelName]);
                for (var a=0; a<searchResults.length; a++) {
                    var tableInitParams = $scope.tableInitParams[searchResults[a].model];
                    if(tableInitParams.params) {
                        for(var b = 0; b < tableInitParams.params.length; b ++) {
                            params.params.push(tableInitParams.params[b]);
                        }
                    }

                    tableService($http, $scope, mainModel, searchResults[a].model, searchResults[a].service, $scope.tableCurrentPage[searchResults[a].model], $scope.tablePerItems[searchResults[a].model], $scope.tableCurrentSort[searchResults[a].model], params, blockUI);
                }
            }

        };
    }

    if ($scope["add"] == undefined) {
        // console.log($scope.taxpayerUpdate);
        // $scope.$on("add", function (event, data) {
        //     console.log("on add", $scope.taxpayerUpdate);
        // });

        // $scope.$on("setDataFromController", function(event, data) {
        //     console.log("set data from $scope", $scope[mainModel], data);
        // });

        $scope.add = function(object, mainModel, model, url, value, modalClose) {

            // console.log($scope, $scope[mainModel]);
            if ($scope.form_valid) {
                // console.log(modalClose);
                // console.log(model, notificationList, _.findWhere(notificationList, {model: model}));
                // console.log($scope, $scope[mainModel]);
                var addTempMainModel = $scope[mainModel];
                var tempResult = searchResult[model+value];
                // console.log(tempResult);

                for (var um=0; um<maps.length; um++) {
                    var singleMap = maps[um];
                    // console.log(singleMap);
                    // console.log(singleMap, maps[um].map.getCenter().lat(), singleMap.map.getCenter().lat());
                    // console.log(singleMap.map.markers[0]);

                    addTempMainModel[model][singleMap.latitudeModel] = singleMap.map.markers[0].getPosition().lat();
                    addTempMainModel[model][singleMap.longitudeModel] = singleMap.map.markers[0].getPosition().lng();
                    maps = [];
                }

                var singleSetModel = _.findWhere(setModelList, {model: model});

                if (singleSetModel != undefined) {
                    var singleSetModelDatas = singleSetModel.datas;

                    for (var op=0; op<singleSetModelDatas.length; op++) {
                        if (singleSetModelDatas[op].fromModel) {
                            addTempMainModel[model][singleSetModelDatas[op].model] = addTempMainModel[model][singleSetModelDatas[op].model];
                        } else {
                            addTempMainModel[model][singleSetModelDatas[op].model] = singleSetModelDatas[op].value;
                            // console.log(singleSetModelDatas[op]);
                        }
                    }

                    setModelList = _.reject(setModelList, {model: model});
                }


                var singleCopyModel = _.findWhere(copyModelList, {model: model});

                if (singleCopyModel != undefined) {
                    var singleCopyModelDatas = singleCopyModel.datas;

                    for (var cp=0; cp<singleCopyModelDatas.length; cp++) {
                        // console.log(singleCopyModelDatas[cp]);
                        var fromModelArr = singleCopyModelDatas[cp].fromModel.split(".");
                        var toModelArr = singleCopyModelDatas[cp].toModel.split(".");

                        var fromModelTemp = addTempMainModel;
                        for (var fa=0; fa<fromModelArr.length; fa++) {
                            fromModelTemp = fromModelTemp[fromModelArr[fa]];
                        }

                        // var toModelTemp = addTempMainModel;
                        if (addTempMainModel[toModelArr[0]] == undefined) {
                            addTempMainModel[toModelArr[0]] = {};
                        }

                        addTempMainModel[toModelArr[0]][toModelArr[1]] = fromModelTemp;

                        model = toModelArr[0];

                        // for (var ta=0; ta<toModelArr.length; ta++) {
                        //     console.log(addTempMainModel);
                        //     toModelTemp = toModelTemp[toModelArr[ta]] || {};
                        // }
                        //
                        // toModelTemp = fromModelTemp;
                    }

                    // console.log(addTempMainModel, toModelArr);

                    copyModelList = _.reject(copyModelList, {model: model});
                }

                // console.log(toModelTemp, addTempMainModel);

                if (("temp" in tempResult[0]) && tempResult[0].temp == true) {
                    $rootScope.$emit("add", {model: tempResult[0].model, data: addTempMainModel[model]});
                    if (modalClose) {
                        $scope.cancel();
                    }
                } else {
                    postService($http, $scope, url, addTempMainModel[model], modalClose, model, model+value, $rootScope);

                    // tableService($http, $scope, mainModel, tempResult[0].model, tempResult[0].service, $scope.tableCurrentPage[tempResult[0].model], $scope.tablePerItems[tempResult[0].model], $scope.tableCurrentSort[tempResult[0].model],  addTempMainModel[model])
                }
            }
        };
    }

    if ($scope["update"] == undefined) {
        $scope.update = function(object, mainModel, model, url, value, modalClose) {
            if ($scope.form_valid) {
                // console.log(object, model, url, $scope.selData);
                var addTempMainModel = $scope[mainModel];
                var tempResult = searchResult[model+value];
                // console.log(tempResult, addTempMainModel[model]);
                // console.log(maps);

                for (var um=0; um<maps.length; um++) {
                    var singleMap = maps[um];

                    addTempMainModel[model][singleMap.latitudeModel] = singleMap.map.markers[0].getPosition().lat();
                    addTempMainModel[model][singleMap.longitudeModel] = singleMap.map.markers[0].getPosition().lng();
                    maps = [];
                }

                var singleSetModel = _.findWhere(setModelList, {model: model});

                if (singleSetModel != undefined) {
                    var singleSetModelDatas = singleSetModel.datas;

                    for (var op=0; op<singleSetModelDatas.length; op++) {
                        if (singleSetModelDatas[op].fromModel) {
                            addTempMainModel[model][singleSetModelDatas[op].model] = addTempMainModel[model][singleSetModelDatas[op].model];
                        } else {
                            addTempMainModel[model][singleSetModelDatas[op].model] = singleSetModelDatas[op].value;
                            // console.log(singleSetModelDatas[op]);
                        }
                    }

                    setModelList = _.reject(setModelList, {model: model});
                }


                var singleCopyModel = _.findWhere(copyModelList, {model: model});

                if (singleCopyModel != undefined) {
                    var singleCopyModelDatas = singleCopyModel.datas;

                    for (var cp=0; cp<singleCopyModelDatas.length; cp++) {
                        // console.log(singleCopyModelDatas[cp]);
                        var fromModelArr = singleCopyModelDatas[cp].fromModel.split(".");
                        var toModelArr = singleCopyModelDatas[cp].toModel.split(".");

                        var fromModelTemp = addTempMainModel;
                        for (var fa=0; fa<fromModelArr.length; fa++) {
                            fromModelTemp = fromModelTemp[fromModelArr[fa]];
                        }

                        // var toModelTemp = addTempMainModel;
                        if (addTempMainModel[toModelArr[0]] == undefined) {
                            addTempMainModel[toModelArr[0]] = {};
                        }

                        addTempMainModel[toModelArr[0]][toModelArr[1]] = fromModelTemp;

                        object = toModelArr[0];

                        // for (var ta=0; ta<toModelArr.length; ta++) {
                        //     console.log(addTempMainModel);
                        //     toModelTemp = toModelTemp[toModelArr[ta]] || {};
                        // }
                        //
                        // toModelTemp = fromModelTemp;
                    }

                    console.log(addTempMainModel, toModelArr);

                    copyModelList = _.reject(copyModelList, {model: model});
                }


                if (tempResult[0].temp == true) {
                    // console.log("temp update");
                    $rootScope.$emit("update", {model: tempResult[0].model, data: addTempMainModel[model]});
                    if (modalClose) {
                        $scope.cancel();
                    }
                } else {
                    updateService($http, $scope, url+$scope.selData, addTempMainModel[model], modalClose);
                }
            }

        }
    }

    // if (("setDataFromController" in $rootScope.$$listeners) == false) {
    //     $rootScope.$on("setDataFromController", function(event, data) {
    //         console.log("setDataFromController", data, $scope);
    //     });
    // }

    // if (("setDataFromController" in $scope.$$listeners) == false) {
    //     $scope.$on("setDataFromController", function(event, data) {
    //         console.log("set data from $scope", $scope[mainModel], data);
    //     });
    // }

    if ($scope["selectChange"] == undefined) {
        $scope.selectChange = function(value, changeModel, changeUrl) {
            // console.log(value, changeModel, changeUrl);
            var tempSelect = _.findWhere(selectWithParams, {model: changeModel});
            // console.log(selectWithParams, tempSelect, changeModel);
            // _.findWhere(selectWithParams, )
            // console.log("select change", value, changeModel, changeUrl);
            var selectParams = {
                params: []
            };
            selectParams.params.push({
                name: "parentId",
                value: value,
                operator: "="
            });

            if (("param" in tempSelect)) {
                for (var ki=0; ki< tempSelect.param.params.length; ki++) {
                    selectParams.params.push(tempSelect.param.params[ki]);
                }
            }

            // console.log(selectParams, tempSelect.param.params[0]);

            selectService($http, $scope, changeModel, changeUrl, selectParams);
        }
    }

    if ($scope["replaceModel"] == undefined) {
        $scope.replaceModel = function (formModel, model) {
            // console.log(replaceValues, model);
            var selReplaceModel = _.findWhere(replaceValues, {model: model});
            if (selReplaceModel) {
                for (var ko=0; ko<selReplaceModel.data.length; ko++) {
                    var tempSourceModel = selReplaceModel.data[ko].sourceModel;
                    var tempSourceModelArr = tempSourceModel.split(".");
                    // console.log(tempSourceModelArr);
                    var tempData = $scope[mainModel][formModel];
                    for (var ji=0; ji<tempSourceModelArr.length; ji++) {
                        // console.log(tempData, tempSourceModelArr[ji]);
                        tempData = tempData[tempSourceModelArr[ji]];
                    }
                    // console.log(tempData);
                    $scope[mainModel][formModel][selReplaceModel.data[ko].replaceModel] = tempData;
                }

                // console.log(mainModel,$scope[mainModel][formModel], model, selReplaceModel);
            }
            // console.log(model);
        };
    }

    if ($scope["searchData"] == undefined) {
        $scope.searchData = function(url, length, data, model) {

            // console.log(data.split("."), model.split("."));

            var tempSearchModelArr = model.split(".");

            var tempModel = $scope;
            for (var sd=0; sd<tempSearchModelArr.length; sd++) {
                tempModel = tempModel[tempSearchModelArr[sd]];
            }
            // console.log(tempModel);

            var tempSearchDataArr = data.split(".");

            var tempData = $scope;
            for (var sk=0; sk<tempSearchDataArr.length-1; sk++) {
                // console.log(tempSearchDataArr[sk]);
                // console.log($scope+"[taxpayerUpdate]");
                tempData = tempData[tempSearchDataArr[sk]];
            }

            tempData[tempSearchDataArr[tempSearchDataArr.length-1]] = {};

            var tempTopModel = $scope;
            for (var vp=0; vp<tempSearchModelArr.length-1; vp++) {
                tempTopModel = tempTopModel[tempSearchModelArr[vp]];
            }

            // console.log(url, length, data, model);
            var tempParams = {
                params: []
            };
            var tempSearchData = _.findWhere(searchDataParams, {model: model});
            if (tempSearchData) {
                // console.log(tempSearchData.params.param);
                for (var lp=0; lp<tempSearchData.params.param.params.length; lp++) {
                    // console.log(tempSearchData.params.param.params[lp]);
                    // console.log(tempTopModel[tempSearchData.params.param.params[lp].value]);
                    tempParams.params.push({
                        name: tempSearchData.params.param.params[lp].name,
                        value: tempTopModel[tempSearchData.params.param.params[lp].value],
                        operator: tempSearchData.params.param.params[lp].operator
                    });
                }
            }
            // console.log(tempSearchData);

            if (length == tempModel.length) {

                $http.post(url, tempParams)
                    .success(function(response) {
                        var obj = response._embedded;
                        var resp = response._embedded[Object.keys(obj)[0]] || [];
                        // console.log(resp.length);
                        if (resp.length == 1) {
                            tempData[tempSearchDataArr[tempSearchDataArr.length-1]] = resp[0];
                        } else {
                            tempData[tempSearchDataArr[tempSearchDataArr.length-1]] = {};
                            $scope.$emit("notificationServiceError", url + " байцаагчийн мэдээлэл олдсонгүй!");
                        }

                    })
                    .error(function(response) {});
            }

        }
    }

    if ($scope["tabLoad"] == undefined) {
        $scope.tabLoad = function(tab) {
            // console.log(tab, tabLoadList);
            var foundTabLoad = _.findWhere(tabLoadList, {tab: tab});
            console.log(tabLoadList, tab);
            if (foundTabLoad != undefined) {
                // console.log(setModelData[tab]);
                if (setModelData[tab] != undefined) {
                    var repTempModelData = $scope;
                    var repModelArr = setModelData[tab].split(".");
                    for (var rm=1; rm<repModelArr.length; rm++) {
                        repTempModelData = repTempModelData[repModelArr[rm]];
                    }
                    findAndReplace(foundTabLoad.data, setModelData[tab], repTempModelData);
                }
                // console.log(foundTabLoad.data);
                $('#'+tab).html("");
                templateLoad(tab, foundTabLoad.data, $scope, $rootScope, $compile, $modal, $http, false, blockUI);
                //     for (var ftl=0; ftl<foundTabLoad.service.length; ftl++) {
                //         tableService($http, $scope, mainModel, foundTabLoad.service[ftl].model, foundTabLoad.service[ftl].url, foundTabLoad.service[ftl].pageIndex, foundTabLoad.service[ftl].itemSize, undefined, foundTabLoad.service[ftl].param);
                //     }
            }


        };
    }

    if ($scope["refreshTable"] == undefined) {
        $scope.refreshTable = function(model) {
            console.log("refresh load");
            var fndTSL = _.findWhere(tableServiceList, {model: model});
            tableService($http, $scope, fndTSL.mainModel, fndTSL.model, fndTSL.url, fndTSL.page, fndTSL.size, fndTSL.sort, fndTSL.param, null, fndTSL.nopage);
        }
    }

    if (firstLoad) {
        $scope.selectArray = {};
        $scope.tableArray = {};
        $scope.tableTotalItems = {};
        $scope.tablePerItems = {};
        $scope.tableCurrentPage = {};
        $scope.tableCurrentSort = {};
        $scope.tableCurrentParams = {};
        $scope.tableInitParams = {};
        $scope.tableServiceAngular = function(mainModel, model, url, page, size, sort, param){
            // console.log(model, url, index, size, param);
            tableService($http, $scope, mainModel, model, url, page, size, sort, param, blockUI);
            // console.log("tableServiceAngular");
        }
    }

    // var templateKeys = Object.keys(data);
    // console.log(templateKeys, data);
    // console.log("length: ",templateKeys.length);

    for (var i=0; i<data.length; i++) {
        // console.log(Object.keys(data[i])[0]);
        var templateSingleKey = Object.keys(data[i])[0];

        if (templateSingleKey == "mainModel") {
            mainModel = data[i][templateSingleKey];
            // console.log("mainModel", mainModel);
        } else if (templateSingleKey == "title") {
            var title = data[i][templateSingleKey];
            var formHTML = "<div class='t-title'>" +
                "<h2>"+title.name+"</h2>" +
                "<div class='clearfix'></div>" +
                "</div>";
            angular.element(document.getElementById(id)).append($compile(formHTML)($scope));
        } else if (templateSingleKey == "form") {

            var templateForms = data[i][templateSingleKey];

            for (var e=0; e<templateForms.length; e++) {
                // console.log(e);

                var templateSingleObj = templateForms[e];
                // console.log(templateSingleObj);
                // console.log(templateSingleObj.defaultValues, !_.isEmpty(templateSingleObj.defaultValues));
                $scope[mainModel] = $scope[mainModel] || {};

                if (!_.isEmpty(templateSingleObj.defaultValues) && !_.isEmpty(templateSingleObj.defaultValues.url)) {
                    // console.log(templateSingleObj.defaultValues.url, templateSingleObj.model);
                    //TODO: setData default zasax
                    // console.log(templateSingleObj.model);
                    setData($http, $scope, templateSingleObj.defaultValues.url, templateSingleObj.model, mainModel);
                }

                // console.log(templateSingleObj, templateSingleObj.defaultValues);
                // console.log(!_.isEmpty(templateSingleObj.defaultValues));

                if (!_.isEmpty(templateSingleObj.defaultValues) && ("fromTable" in templateSingleObj.defaultValues) && templateSingleObj.defaultValues.fromTable == true) {
                    $scope[mainModel][templateSingleObj.model] = $rootScope.selDataObject;
                    // console.log($scope, mainModel, templateSingleObj.model, $scope[mainModel][templateSingleObj.model]);
                }

                if (!_.isEmpty(templateSingleObj.defaultValues) && !_.isEmpty(templateSingleObj.defaultValues.data)) {
                    // console.log("defaultValues", mainModel, templateSingleObj);
                    $scope[mainModel][templateSingleObj.model] = {};
                    $scope[mainModel][templateSingleObj.model] = templateSingleObj.defaultValues != undefined ? _.clone(templateSingleObj.defaultValues.data) : {};
                    // console.log($scope[mainModel][templateSingleObj.model]);
                }

                var templateFormFields = templateSingleObj.fields;
                // console.log(templateSingleObj,templateSingleObj.fieldsColumn);
                console.log(formStart);
                var formHTML = "<div class='col-md-12'>";
                if (formStart == false) {
                    formHTML += "<form class='form-horizontal' name='form_"+templateSingleObj.model+"' novalidate='' role='form'>";//"<form name='form' class='form-horizontal' action='"+templateSingleObj.url+"' method='"+templateSingleObj.method+"' novalidate>";
                }

                for (var a=0; a<templateFormFields.length; a++) {
                    var rowStart = "";
                    var rowEnd = "";
                    var rowClass = "";
                    // var formGroupClass = "";

                    var ngif = templateFormFields[a].show == undefined ? "" : "data-ng-if='"+templateFormFields[a].show+"'";

                    if (templateSingleObj.fieldsColumn == 2) {
                        // formGroupClass = "col-md-5";

                        rowClass = "<div class='col-md-5' "+ngif+">";
                        if (a%2 === 0) {
                            rowStart = "<div class='row'>";
                            rowEnd = "";
                        } else {
                            rowStart = "";
                            rowEnd = "</div>";
                        }

                        if (a == templateFormFields.length-1) {
                            rowEnd = "</div>";
                        }
                    } else {
                        rowClass = "<div class='col-md-10' "+ngif+">";
                        rowStart = "<div class='row'>";
                        rowEnd = "</div>";
                        // formGroupClass = "col-md-10";
                    }

                    formHTML += rowStart;

                    formHTML += rowClass +
                        // formHTML +=

                        "<div class='form-group' ng-class='{ \"has-error\" : form_"+templateSingleObj.model+"."+templateFormFields[a].model+".$invalid && attempted }'>" +
                        "<label class='col-sm-5 control-label' for='"+templateFormFields[a].model+"' style='font-weight: bold; color: #195ea2;'>"+templateFormFields[a].label+":</label>" +
                        "<div class='col-sm-7'>";

                    if (templateFormFields[a].field == "input") {
                        var fieldType = templateFormFields[a].type;
                        var validations = templateFormFields[a].validation;
                        var regex = templateFormFields[a].regex;
                        var regexHtml = "";

                        var mask = templateFormFields[a].mask;
                        var maskHtml = "";

                        var validation = "";
                        // console.log(validations);
                        if (validations != undefined && validations.length > 0) {

                            for (var h=0; h<validations.length; h++) {
                                var validation_arr = validations[h].split("-");
                                // console.log(validation_arr);
                                if (validation_arr[0] == "required") {
                                    // console.log("required");
                                    validation += "required";
                                } else if (validation_arr[0] == "number") {
                                    //TODO: number validation oruulax
                                    console.log("number");
                                    // validation += "";
                                } else if (validation_arr.length == 2 && validation_arr[0] == "length") {
                                    // console.log("length");
                                    validation += " ng-minlength='"+validation_arr[1]+"' ng-maxlength='"+validation_arr[1]+"'";
                                }
                            }
                        } else {
                            validation = "";
                        }

                        if (regex) {
                            regexHtml += "ng-pattern='/"+regex+"/'";
                            // console.log(regexHtml);
                        } else {
                            regexHtml = "";
                        }

                        if (mask) {
                            maskHtml += "mask='"+mask+"' restrict='reject' validate='false'";
                        } else {
                            maskHtml = "";
                        }

                        var modalAction = "";
                        if (!_.isEmpty(templateFormFields[a].modal)) {
                            modalData[a] = templateFormFields[a].modal;
                            modalAction = "data-ng-click='loadModal("+a+")'"
                        } else {
                            modalAction = "";
                        }

                        var fieldParam = "";
                        if (templateFormFields[a].type == "date") {
                            fieldType = "text";
                            fieldParam = 'jqdatepicker="" mask="9999-19-39 29:59:59" restrict="reject" validate="false" placeholder="9999-19-39 23:59:59"';
                        }

                        var classV = "";
                        if (templateFormFields[a].type != "checkbox") {
                            classV = "form-control"
                        } else {
                            fieldParam = ' ng-true-value="1" ng-false-value="0" ng-checked="'+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].model+' == 1"';
                        }

                        // var inputID = "";
                        // if (templateFormFields[a].id != undefined) {
                        //     inputID = "id='"+templateFormFields[a].id+"'";
                        // }

                        if (templateFormFields[a].type == "file") {
                            console.log("file");
                        }

                        var isModal = "";
                        if ($modal == null) {
                            isModal = "modal";
                        }

                        var onChange = "";
                        // console.log(templateFormFields[a]);
                        if (("searchData" in templateFormFields[a])) {
                            // console.log("searchData");
                            searchDataParams.push({
                                model: mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].model,
                                params: templateFormFields[a].searchData
                            });
                            // console.log(searchDataParams);
                            onChange = "ng-change='searchData(\""+templateFormFields[a].searchData.url+"\", "+templateFormFields[a].searchData.length+", \""+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].searchData.data+"\", \""+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].model+"\")'";

                        }

                        formHTML += "<input type='"+fieldType+"' name='"+templateFormFields[a].model+"' id='"+isModal+templateFormFields[a].model+"' data-ng-model='"+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].model+"' "+modalAction+" ng-disabled="+templateFormFields[a].disable+" class='"+classV+"' "+fieldParam+" "+validation+" "+regexHtml+" " +maskHtml+" " +onChange+">"; //popover='Should be between 1 and 10' popover-toggle='"+templateSingleObj.model+"."+templateFormFields[a].model+".$invalid' popover-placement='top'
                    } else if (templateFormFields[a].field == "a") {
                        // if (templateFormFields[a].file) {
                        //     $scope.tempAddUrl = "/312421";
                        // }
                        $scope.tempUrl = "/tais-file-service/file/{{taxpayerUpdate.taxpayerDocModel.fileStore.id}}/{{taxpayerUpdate.taxpayerDocModel.fileStore.fileUuid}}";
                        $scope.tempAddUrl = "";

                        $scope[mainModel][templateSingleObj.model][templateFormFields[a].file] = $scope[mainModel][templateSingleObj.model][templateFormFields[a].file] == undefined ? {} : $scope[mainModel][templateSingleObj.model][templateFormFields[a].file];
                        var fileType = $scope[mainModel][templateSingleObj.model][templateFormFields[a].file]["fileType"];
                        if (fileType != undefined) {
                            var fileTypeArr = fileType.split("/");
                            if (fileTypeArr[0] == "image" || fileTypeArr[1] == "pdf") {
                                // $scope.tempAddUrl = "/"+$scope[mainModel][templateSingleObj.model][templateFormFields[a].file]["fileName"];
                                $scope.tempAddUrl = "/{{taxpayerUpdate.taxpayerDocModel.fileStore.fileName}}";
                            }
                        }

                        formHTML += "<a target='_blank' id='"+templateFormFields[a].model+"' href='"+$scope.tempUrl+$scope.tempAddUrl+"'>{{taxpayerUpdate.taxpayerDocModel.fileStore.fileName || 'empty link'}}</a>";
                    } else if (templateFormFields[a].field == "textarea") {
                        formHTML += "<textarea rows='3' name='"+templateFormFields[a].model+"' id='"+isModal+templateFormFields[a].model+"' data-ng-model='"+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].model+"' "+modalAction+" ng-disabled="+templateFormFields[a].disable+" class='form-control' "+validation+"></textarea>";
                    } else if (templateFormFields[a].field == "select") {
                        // console.log(templateFormFields[a].onChange);

                        var onChangeAction = "";
                        var onChangeModel = templateFormFields[a].onChangeModel || {};

                        var selectParam = {};
                        if (templateFormFields[a].param) {
                            selectParam = templateFormFields[a].param;
                            // console.log("param", templateFormFields[a].param);
                        }

                        if (!_.isEmpty(onChangeModel)) {
                            var tempModelData = mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].model;
                            if (("replaceValues" in templateFormFields[a].selectValue)) {
                                var tempSelectSourceModel = templateFormFields[a].selectValue.replaceValues[0].sourceModel.split(".");
                                // var tempData = $scope[mainModel][templateSingleObj.model];
                                //     console.log(tempData);
                                for (var ji=0; ji<tempSelectSourceModel.length; ji++) {
                                    // console.log(tempSelectSourceModel[ji]);
                                    if (ji > 0) {
                                        tempModelData += "."+tempSelectSourceModel[ji];
                                    }

                                    //         tempData = tempData[tempSelectSourceModel[ji]];
                                }
                                //     console.log(tempData);
                                //     // tempModelData =
                            }
                            // console.log(tempModelData);
                            onChangeAction = "selectChange("+tempModelData+",'"+onChangeModel.model+"','"+onChangeModel.url+"');";

                            // $scope.$watch(templateSingleObj.model+"."+templateFormFields[a].model, function(nv, ov) {
                            //     if (nv == undefined) {
                            //         var onChange = onChangeModel.model;
                            //         console.log(onChange);
                            //     } else {}
                            //     console.log(nv, ov, onChange);
                            // });
                            selectParentList.push(
                                {
                                    model: templateSingleObj.model+"."+templateFormFields[a].model,
                                    changeModel: onChangeModel.model,
                                    changeUrl: onChangeModel.url
                                }
                            );


                        }
                        selectWithParams.push(templateFormFields[a]);

                        var findParent = _.findWhere(selectParentList, {changeModel: templateFormFields[a].model});
                        // console.log(findParent);
                        if (findParent == undefined) {
                            // console.log(templateFormFields[a]);
                            selectService($http, $scope, templateFormFields[a].model, templateFormFields[a].select, selectParam, templateFormFields[a].onChangeModel, templateSingleObj.model, mainModel);
                        } else {
                            // selectWithParams.push(templateFormFields[a]);

                            // console.log(templateFormFields[a]);
                            // var modelArray = findParent.model.split(".");
                            // var modelAppend = "";
                            // for (var ij=0; ij<modelArray.length; ij++) {
                            //     modelAppend += "["+modelArray[ij]+"]";
                            //     console.log(modelArray[ij]);
                            //     console.log($scope["locationModel"]);
                            //     // var mainModel = $scope[modelArray[ij]];
                            // }


                            // selectWithParams.push(
                            //     {
                            //         model: templateFormFields[a].model,
                            //         url: templateFormFields[a].select,
                            //         dataModel: ''
                            //     }
                            // );

                            // selectService($http, $scope, templateFormFields[a].model, templateFormFields[a].select);
                        }
                        // console.log("found");
                        // console.log("selectParentList",selectParentList);



                        if (templateFormFields[a].selectValue == undefined) {
                            alert("selectValue хоосон байна");
                            // templateFormFields[a].selectValue = {};
                        }
                        var tempName = templateFormFields[a].selectValue.name;
                        var selectNames = tempName.split(" ");
                        var selectSingleName = "";
                        for (var lk=0; lk<selectNames.length; lk++) {
                            selectSingleName += "v."+selectNames[lk];
                            // console.log(selectNames.length);
                            if (selectNames.length > 1 && lk < selectNames.length-1) {
                                selectSingleName += "+' '+";
                            }
                        }

                        var selectModel = "v."+templateFormFields[a].selectValue.id;
                        var trackBy = "";

                        if (("replaceValues" in templateFormFields[a].selectValue)) {
                            // console.log(templateFormFields[a], templateFormFields[a].model);
                            onChangeAction += "replaceModel('"+templateSingleObj.model+"','"+templateFormFields[a].model+"');";
                            selectModel = "v";
                            // var tempArr = tempModelData.split(".");
                            // var tempId = tempArr[tempArr.length-1];
                            // console.log(tempArr[tempArr.length-1]);
                            trackBy = "track by (v.id+$index)";
                            // console.log(templateFormFields[a].selectValue.replaceValues);
                            replaceValues.push({
                                model: templateFormFields[a].model,
                                data: templateFormFields[a].selectValue.replaceValues
                            });
                            // console.log(replaceValues);
                            // $scope[mainModel][templateSingleObj.model][templateFormFields[a].selectValue.object["addressType"]] = $scope[mainModel][templateSingleObj.model][templateFormFields[a].model];
                            // $scope[mainModel][templateSingleObj.model][templateFormFields[a].model] = "1";
                            // console.log($scope[mainModel]);
                        }


                        // console.log(templateFormFields[a]);
                        // console.log(templateSingleObj.model, templateFormFields[a]);
                        formHTML += "<select class='form-control' ng-change=\""+onChangeAction+"\" data-ng-model='"+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].model+"' ng-disabled="+templateFormFields[a].disable+" ng-options=\""+selectModel+" as "+selectSingleName+" for v in selectArray['"+templateFormFields[a].model+"'] "+trackBy+"\"><option value=\"\">--Сонгох--</option></select>";
                    } else if (templateFormFields[a].field == "label") {
                        // console.log(templateSingleObj.model+"."+templateFormFields[a].model);
                        formHTML += "<p class='form-control-static'>{{"+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].model+"}}</p>";
                    }

                    formHTML += "<p ng-if='attempted && form_"+templateSingleObj.model+"."+templateFormFields[a].model+".$error.required' class='help-block'>"+templateFormFields[a].label+" талбарыг заавал бөглөнө үү.</p>" +
                        "<p ng-if='attempted && form_"+templateSingleObj.model+"."+templateFormFields[a].model+".$error.minlength' class='help-block'>"+templateFormFields[a].label+" дэндүү богино байна.</p>" +
                        "<p ng-if='attempted && form_"+templateSingleObj.model+"."+templateFormFields[a].model+".$error.maxlength' class='help-block'>"+templateFormFields[a].label+" дэндүү урт байна.</p>" +
                        "<p ng-if='attempted && form_"+templateSingleObj.model+"."+templateFormFields[a].model+".$error.pattern' class='help-block'>"+templateFormFields[a].label+" талбарт " + templateFormFields[a].regexMsg +".</p>" +
                        // "<p ng-if='attempted && form_accRequests.entEntity.tin.$error.pattern' class='help-block'>"+templateFormFields[a].label+" талбарт " + templateFormFields[a].regexMsg +".</p>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
                    formHTML += rowEnd;

                    if (templateFormFields[a].field == "map") {
                        // formHTML += "<p>{{"+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].latitude+"}} {{"+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].longitude+"}}</p>";
                        formHTML += "<ng-map center='[{{"+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].latitude+" || 47.9186408}}, {{"+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].longitude+" || 106.9174761}}]' zoom='{{"+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].zoom+"}}' style='height: 300px;'><marker position='[{{"+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].latitude+" || 47.9186408}}, {{"+mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].longitude+" || 106.9174761}}]' title='how' draggable='true'></marker></ng-map> </br>";
                        // console.log(mainModel+"."+templateSingleObj.model+"."+templateFormFields[a].latitude);

                        // console.log("$emit");
                        $scope.$emit("mapLoad", {model: templateFormFields[a].model, latitudeModel: templateFormFields[a].latitude, longitudeModel: templateFormFields[a].longitude});
                    }
                }

                var templateFormButtons = templateSingleObj.buttons;
                for (var d=0; d<templateFormButtons.length; d++) {
                    // console.log(templateSingleObj, templateFormButtons[d]);
                    searchResult[templateSingleObj.model+templateFormButtons[d].value] = templateFormButtons[d].resultTables || [];
                    // console.log(searchResult);

                    if (!_.isEmpty(templateFormButtons[d].modal)) {
                        // console.log(a, d, templateFormButtons);
                        modalData['button'+templateFormButtons[d].action+templateFormButtons[d].modal[0].form[0].model+a+''+d] = templateFormButtons[d].modal;
                        setModelData['button'+templateFormButtons[d].action+templateFormButtons[d].modal[0].form[0].model+a+''+d] = templateFormButtons[d].setDataModel;
                        // console.log(modalData["button0"]);
                        // console.log(templateFormButtons[d].modal[0].form[0].model);
                        modalAction = "data-ng-click='loadModal(\"button"+templateFormButtons[d].action+templateFormButtons[d].modal[0].form[0].model+a+''+d+"\")'"
                    } else {
                        modalAction = "";
                    }

                    // console.log(templateFormButtons[d]);
                    // var closeModal = "";
                    // if (templateFormButtons[d].closeModal) {
                    //     closeModal = "cancel()";
                    // }
                    if (templateFormButtons[d].responseValues) {
                        if (templateFormButtons[d].responseValues.notif) {
                            // console.log("notif");
                            notificationList.push({
                                model: templateSingleObj.model,
                                data: templateFormButtons[d].responseValues.notif
                            });
                        }
                    }

                    //TODO: xooson baigaa ued aldaat ajilxaar bna zasax
                    if (templateFormButtons[d].setModel) {
                        setModelList.push(
                            {
                                model: templateSingleObj.model,
                                datas: templateFormButtons[d].setModel
                            }
                        );
                        // console.log("setModel", templateFormButtons[d].setModel);
                    }

                    if (templateFormButtons[d].copyModel) {
                        copyModelList.push(
                            {
                                model: templateSingleObj.model,
                                datas: templateFormButtons[d].copyModel
                            }
                        );
                        // console.log(copyModelList);
                    }

                    console.log(templateSingleObj);

                    var action = templateFormButtons[d].action!='' ? 'ng-click="attempted=true; submitForm(form_'+templateSingleObj.model+');'+templateFormButtons[d].action+'('+templateFormButtons[d].actionModel+',\''+mainModel+'\',\''+templateSingleObj.model+'\',\''+templateFormButtons[d].url+'\',\''+templateFormButtons[d].value+'\', \''+templateFormButtons[d].closeModal+'\', \''+templateFormButtons[d].actionModel+'\');"' : '';
                    var buttonClass = "btn btn-primary";
                    // console.log(("class" in templateFormButtons[d]));
                    if (("class" in templateFormButtons[d]) && !_.isEmpty(templateFormButtons[d].class)) {
                        // console.log("with class");
                        buttonClass = "btn btn-success";
                    }
                    var ngIfButton = templateFormButtons[d].show == undefined ? "" : "data-ng-if='"+templateFormButtons[d].show+"'";
                    // ng-disabled='attempted && form_"+ templateSingleObj.model +".$invalid'
                    formHTML += "<button type='"+templateFormButtons[d].type+"' "+ngIfButton+" "+action+" "+modalAction+" style='margin-bottom: 15px !important; margin-right: 15px' class='"+buttonClass+"'>"+templateFormButtons[d].value+"</button>";
                }

                var templateSeperators = templateSingleObj.seperator;
                if (templateSeperators) {
                    console.log("sep");
                    formHTML += "<hr>";
                }

                if (formStart == false) {
                    formHTML += "</form>";
                }

                formHTML += "</div";

                // console.log(id);
                if (formStart == false) {
                    angular.element(document.getElementById(id)).append($compile(formHTML)($scope));
                } else {
                    angular.element(document.getElementById(id)).append(formHTML);
                }

                // console.log($scope["locationModel"]);
            }
            // console.log($scope, mainModel, $scope[mainModel], templateSingleObj.model);

            // $scope[mainModel] = $scope[mainModel] || {};
            // $scope[mainModel][templateSingleObj.model] = templateSingleObj.defaultValues != undefined ? templateSingleObj.defaultValues.data : {};

            // console.log(selectWithParams);
            // console.log("selectParentList",selectParentList);
            //
            // for (var kj=0;kj<selectParentList.length;kj++) {
            //     console.log(selectParentList[kj].model, $scope["locationModel"][selectParentList[kj].model.split(".")[1]], $scope[selectParentList[kj].model]);
            // }
            //selectService($http, $scope, templateFormFields[a].model, templateFormFields[a].select);

        } else if (templateSingleKey == "table") {
            var templateTables = data[i][templateSingleKey];
            // console.log("table " ,id ,templateTables);

            for (var e=0; e<templateTables.length; e++) {
                var templateTableSingleObj = templateTables[e];
                // console.log(templateTableSingleObj)
                $scope.tableInitParams[templateTableSingleObj.dataList] =  templateTableSingleObj.param || {};

                var tableHTML = "<div class='col-md-12'>";
                tableHTML += "<table class='table table-headrow'>";
                tableHTML += "<thead> <tr> ";

                var isCheckable = false;
                if (templateTableSingleObj.checkable) {
                    isCheckable = true;
                    tableHTML += "<th>"+"Сонгох"+"</th>";
                }

                var isRadio = false;
                if (templateTableSingleObj.radio) {
                    isRadio = true;
                    tableHTML += "<th>Сонгох</th>";
                }

                var isSymbol = false;
                var symbolFields = [];
                if (templateTableSingleObj.symbol) {
                    isSymbol = true;
                    for (var s=0;s<templateTableSingleObj.symbol.length;s++) {
                        symbolFields.push(templateTableSingleObj.symbol[s].field);
                    }
                }

                var tableBodyHTML = "";

                var templateTableHeaders = templateTableSingleObj.header;


                for (var b=0; b<templateTableHeaders.length; b++) {
                    tableHTML += "<th>"+templateTableHeaders[b]+"</th>";
                }

                // if (templateTableSingleObj.editable) {
                //     tableHTML += "<th colspan='3' style='text-align: center;'>Үйлдэл</th>";
                // }


                var actionCount = 0;
                if (templateTableSingleObj.editable) {
                    actionCount++;
                }
                if (templateTableSingleObj.viewable) {
                    actionCount++;
                }
                if (templateTableSingleObj.deletable) {
                    actionCount++;
                }
                if (templateTableSingleObj.moreActions) {
                    for (var mac = 0; mac<templateTableSingleObj.moreActions.length;mac++) {
                        actionCount++;
                    }
                }
                var templateTableBodys = templateTableSingleObj.bodyModels;

                var tableHeadLength = _.clone(templateTableBodys.length);

                if (actionCount > 0) {
                    tableHTML += "<th colspan='"+actionCount+"' style='text-align: center;'>Үйлдэл</th>";
                    tableHeadLength = _.clone(templateTableBodys.length) + actionCount;
                }

                // console.log(actionCount);

                tableHTML += "</tr></thead>";


                if (isCheckable) {
                    tableHeadLength = tableHeadLength+1;
                    tableBodyHTML += "<td> <input type=\"checkbox\" ng-true-value=\"1\" ng-false-value=\"0\" ng-checked=\"{{data.isMain}}\" data-ng-click=\""+templateTableSingleObj.checkFunction+"(data, "+mainModel+"."+templateTableSingleObj.dataList+"); $event.stopPropagation();\"></td>";
                }

                if (isRadio) {
                    // if (templateTableSingleObj.radioShow) {
                    var tempRadioShow = "ng-if='"+templateTableSingleObj.radioShow+"'";
                    // }
                    tableHeadLength = tableHeadLength+1;
                    tableBodyHTML += "<td><input type='radio' name='radio' "+tempRadioShow+" data-ng-click='"+templateTableSingleObj.checkFunction+"(data);$event.stopPropagation();'></td>";
                }

                for (var c=0; c<templateTableBodys.length; c++) {
                    var angularTemplate = "";

                    var headModelArray = templateTableBodys[c].split(" ");

                    //TODO: dateformat service-s irne gd comment bolgov
                    // if (tempHead.toString().search("Date") != -1) {
                    //     angularTemplate = "| date: 'yyyy-MM-dd hh:MM:ss'";
                    // }

                    var angularTemplateTD = "<td>";

                    for (var cj=0; cj<headModelArray.length; cj++) {
                        // var headArray = headModelArray[cj].split(".");
                        var headAddArray = headModelArray[cj].split("+");
                        for (var hj=0; hj<headAddArray.length; hj++) {
                            var headArray = headAddArray[hj].split(".");

                            var dataModel = "";
                            for (var c1=0; c1<headArray.length; c1++) {
                                dataModel += "['"+headArray[c1]+"']";
                            }

                            var symbolObject = {};
                            var symbolCharacter = "";
                            if ((symbolFields.indexOf(headModelArray[cj])) > -1) {
                                symbolObject = _.findWhere(templateTableSingleObj.symbol, {field: headModelArray[cj]});
                                if (symbolObject.filter == 'currency') {
                                    dataModel += "| " + symbolObject.filter + ": " + "''";
                                }

                                if (symbolObject.filter == 'number') {
                                    dataModel += "| " + symbolObject.filter;
                                }
                                symbolCharacter = "" + symbolObject.character ;
                            }

                            if (headAddArray.length > 1) {
                                if (hj == 0) {
                                    angularTemplateTD += "{{data"+dataModel;
                                } else if (hj == headAddArray.length-1) {
                                    angularTemplateTD += "+data"+dataModel+"}}";
                                } else {
                                    angularTemplateTD += "+data"+dataModel;
                                }
                            } else if (headModelArray.length > 1 && headModelArray.length - 2 == cj) {
                                angularTemplateTD += "{{data"+dataModel+"}} - ";
                            } else {
                                angularTemplateTD += "{{data"+dataModel+"}}";
                            }

                            angularTemplateTD += symbolCharacter ;

                        }

                        // angularTemplateTD += "{{data"+dataModel+" "+angularTemplate+"}} ";
                    }
                    angularTemplateTD += "</td>";

                    tableBodyHTML += angularTemplateTD;
                }

                // console.log(templateTableSingleObj.editable);
                // var tableEditable =


                if (actionCount > 0) {
                    var directUrl = "";
                    if (templateTableSingleObj.editUrl) {
                        // console.log("url-tai", templateTableSingleObj.editUrl);
                        directUrl = templateTableSingleObj.editUrl;
                    }

                    if (!_.isEmpty(templateTableSingleObj.modal)) {
                        // console.log(templateTableSingleObj.modal[0].form[0].model);
                        modalData['table'+templateTableSingleObj.modal[0].form[0].model+a] = templateTableSingleObj.modal;
                        modalAction = "data-ng-click='loadModal(\"table"+templateTableSingleObj.modal[0].form[0].model+a+"\", data)'"
                    } else {
                        modalAction = "";
                    }

                    tableBodyHTML += "<td data-ng-if='"+templateTableSingleObj.viewable+"' style='text-align: center; width: 40px' "+modalAction+"> " +
                        "<a href=\""+directUrl+"\"><button type='button' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Харах\" class='btn btn-sm btn-primary'><span class='fa fa-eye'></span></button></a> " +
                        "</td> " +
                        "<td data-ng-if='"+templateTableSingleObj.editable+"' style='text-align: center; width: 40px' "+modalAction+"> " +
                        "<a href=\""+directUrl+"\"><button type='button' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Засах\" class='btn btn-sm btn-success'><span class='icon-pencil'></span></button></a> " +
                        "</td> " +
                        "<td data-ng-if='"+templateTableSingleObj.deletable+"' style='text-align: center; width: 40px'> " +
                        "<a href='' data-ng-click='tableRemove(data)'><button type='button' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Устгах\" class='btn btn-sm btn-danger'><span class='glyphicon glyphicon-trash'></span></button></a> " +
                        "</td>";

                    if (templateTableSingleObj.moreActions) {

                        for (var ma=0; ma<templateTableSingleObj.moreActions.length; ma++) {

                            if (!_.isEmpty(templateTableSingleObj.moreActions[ma].modal)) {
                                // console.log(templateTableSingleObj.modal[0].form[0].model);
                                modalData['table'+templateTableSingleObj.moreActions[ma].modal[0].form[0].model+a] = templateTableSingleObj.moreActions[ma].modal;
                                modalAction = "data-ng-click='loadModal(\"table"+templateTableSingleObj.moreActions[ma].modal[0].form[0].model+a+"\", data)'"
                            } else {
                                modalAction = "";
                            }

                            // console.log(templateTableSingleObj.moreActions[ma].directUrl);
                            tableBodyHTML += "<td style='text-align: center; width: 40px' "+modalAction+"> " +
                                "<a href=\""+templateTableSingleObj.moreActions[ma].directUrl+"\"><button type='button' data-toggle=\"tooltip\" data-placement=\"top\" title='"+templateTableSingleObj.moreActions[ma].tooltip+"' class='btn btn-sm "+templateTableSingleObj.moreActions[ma].buttonClass+"'><span class='"+ templateTableSingleObj.moreActions[ma].icon+"'>" + " " + templateTableSingleObj.moreActions[ma].title + "</span></button></a> " +
                                "</td>";
                        }
                    }

                    // console.log(directUrl);

                }

                // console.log(templateTableSingleObj.dataList);
                var tableSelectable = "";
                if (templateTableSingleObj.selectable) {
                    tableSelectable = "data-ng-click='selectTable(\""+templateTableSingleObj.dataList+"\", data)'";
                }

                tableHTML += "<tbody>" +
                    "<tr data-ng-show='"+mainModel+"."+templateTableSingleObj.dataList+".length > 0' data-ng-repeat='data in "+mainModel+"."+templateTableSingleObj.dataList+"' "+tableSelectable+">" + tableBodyHTML + "</tr>" +
                    // "<tr data-ng-show='tableArray."+templateTableSingleObj.dataList+".length == 0'><td>Бичлэг байхгүй байна</td></tr>" +
                    "</tbody>";

                tableHTML += "<tfoot class='table-footrow'>";
                tableHTML += "<tr>";
                tableHTML += "<td colspan='"+tableHeadLength+"'>";
                // console.log(tableHeadLength);

                // $scope.tablePerItems[templateTableSingleObj.dataList] = templateTableSingleObj.size;
                // $scope.tableCurrentPage[templateTableSingleObj.dataList] = templateTableSingleObj.page;

                // console.log($scope.tablePerItems[templateTableSingleObj.dataList], $scope.tableCurrentPage[templateTableSingleObj.dataList]);


                tableHTML += "<div class='col-md-4'>Нийт: {{tableTotalItems['"+templateTableSingleObj.dataList+"'] || 0 |  number:0}}</div>" +
                    "<div  class='col-md-8 text-right'>" +
                    "<pagination  style='margin: 0' ng-change='tableServiceAngular(\""+mainModel+"\",\""+templateTableSingleObj.dataList+"\",\""+templateTableSingleObj.url+"\", tableCurrentPage[\""+templateTableSingleObj.dataList+"\"], tablePerItems[\""+templateTableSingleObj.dataList+"\"], tableCurrentSort[\""+templateTableSingleObj.dataList+"\"], tableCurrentParams[\""+templateTableSingleObj.dataList+"\"])' page-label='0' items-per-page='tablePerItems[\""+templateTableSingleObj.dataList+"\"]' boundary-links='true' total-items='tableTotalItems[\""+templateTableSingleObj.dataList+"\"]' max-size='10' ng-model='tableCurrentPage[\""+templateTableSingleObj.dataList+"\"]' class='pagination-sm' previous-text='&lsaquo;' next-text='&rsaquo;' first-text='&laquo;' last-text='&raquo;'></pagination>" +
                    "</div>";
                tableHTML += "</td>";
                tableHTML += "</tr>";
                tableHTML += "</tfoot>";


                tableHTML += "</table>";
                tableHTML += "</div>";

                angular.element(document.getElementById(id)).append($compile(tableHTML)($scope));

                // console.log(id, templateTables);

                //TODO: $scope.templateSingleObj.url-s table-n service-n avaad ugugdliig unshin $scope.dataList model-d xiine

                // console.log(templateTableSingleObj.url, templateTableSingleObj.dataList);
                //TODO: service duudaj duusax xurtel loading xaruulax table-n body deer
                // console.log(templateTableSingleObj.dataList);
                // console.log(templateTableSingleObj);

                // var tableTempParam = templateTableSingleObj.param;
                // if (templateTableSingleObj.replaceParam) {
                //     var paramArray = tableTempParam["params"];
                //     for (var ko=0; ko<paramArray.length; ko++) {
                //         for (var ko1=0; ko1<templateTableSingleObj.replaceParam.length; ko1++) {
                //             if (paramArray[ko].value == templateTableSingleObj.replaceParam[ko1].defValue) {
                //                 paramArray[ko].value = $rootScope.selDataObject[paramArray[ko].value];
                //             }
                //         }
                //     }
                // }

                var fndTSL = _.findWhere(tableServiceList, {model: templateTableSingleObj.dataList});
                if (fndTSL != undefined) {
                    fndTSL.mainModel = mainModel;
                    fndTSL.model = templateTableSingleObj.dataList;
                    fndTSL.url = templateTableSingleObj.url;
                    fndTSL.page = templateTableSingleObj.pageIndex;
                    fndTSL.size = templateTableSingleObj.itemSize;
                    fndTSL.sort = templateTableSingleObj.sort;
                    fndTSL.param = templateTableSingleObj.param;
                    fndTSL.nopage = null;
                } else {
                    tableServiceList.push({
                        mainModel: mainModel,
                        model: templateTableSingleObj.dataList,
                        url: templateTableSingleObj.url,
                        page: templateTableSingleObj.pageIndex,
                        size: templateTableSingleObj.itemSize,
                        sort: templateTableSingleObj.sort,
                        param: templateTableSingleObj.param,
                        nopage: null
                    });
                }

                if (templateTableSingleObj.setDataModel) {
                    console.log(templateTableSingleObj.setDataModel);
                    setModelData['table_'+templateTableSingleObj.dataList] = templateTableSingleObj.setDataModel;
                }

                if (templateTableSingleObj.firstLoad == false) {
                    // console.log(tableServiceLoaded.indexOf(templateTableSingleObj.dataList));
                    if (tableServiceLoaded.indexOf(templateTableSingleObj.dataList) == -1) {
                        console.log("not load");
                        tableServiceLoaded.push(templateTableSingleObj.dataList);
                        console.log(tableServiceLoaded);
                    } else {
                        console.log("in tableServiceLoaded");
                        tableService($http, $scope, mainModel, templateTableSingleObj.dataList, templateTableSingleObj.url, templateTableSingleObj.pageIndex, templateTableSingleObj.itemSize, templateTableSingleObj.sort, templateTableSingleObj.param);
                    }
                } else {
                    // console.log("load");
                    tableService($http, $scope, mainModel, templateTableSingleObj.dataList, templateTableSingleObj.url, templateTableSingleObj.pageIndex, templateTableSingleObj.itemSize, templateTableSingleObj.sort, templateTableSingleObj.param);
                }
            }

        } else if (templateSingleKey == "accordion") {
            var array_list = data[i][templateSingleKey];
            var accordionHTML = "<div class='col-md-12'>";
            accordionHTML += "<div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>";

            for (var f=0; f<array_list.length; f++) {
                accordionHTML += "<div class='panel panel-default'>";
                accordionHTML += "<div class='panel-heading' role='tab' id='heading"+f+"'>";
                accordionHTML += "<h4 class='panel-title'>";
                accordionHTML += "<a role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse"+f+"' aria-expanded='false' aria-controls='collapse"+f+"'>"+array_list[f].title+"</a>";
                accordionHTML += "</h4>";
                accordionHTML += "</div>";
                accordionHTML += "<div id='collapse"+f+"' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading"+f+"'>";
                accordionHTML += "<div class='panel-body'>";
                accordionHTML += "<div id='accordion_"+f+"'></div>";
                accordionHTML += "</div>";
                accordionHTML += "</div>";
                accordionHTML += "</div>";
            }

            accordionHTML += "</div>";
            accordionHTML += "</div>";
            angular.element(document.getElementById(id)).append($compile(accordionHTML)($scope));

            for (var g=0; g<array_list.length; g++) {
                // console.log(array_list[g].data);
                templateLoad('accordion_'+g, array_list[g].data, $scope, $rootScope, $compile, $modal, $http, false, blockUI);
            }
        } else if (templateSingleKey == "panel") {
            panelId++;
            var panel_list = data[i][templateSingleKey];
            var panelHTML = "<div class='col-md-12'>";
            // panelHTML += "<div class='panel panel-default'>";

            for (var zo=0; zo<panel_list.length; zo++) {
                var show = panel_list[zo].show || true;
                panelHTML += "<div class='t-panel' data-ng-show='"+show+"'>";
                panelHTML += "<div class='t-title'><h2>"+panel_list[zo].title+"</h2><div class='clearfix'></div></div>";
                panelHTML += "<div class='t-body'>";
                panelHTML += "<div id='panel_"+zo+''+panelId+"'></div>";
                panelHTML += "</div>";
                panelHTML += "</div>";
            }

            // panelHTML += "</div>";
            panelHTML += "</div>";
            angular.element(document.getElementById(id)).append($compile(panelHTML)($scope));

            for (var go=0; go<panel_list.length; go++) {
                // console.log(panel_list[go].data);
                templateLoad('panel_'+go+''+panelId, panel_list[go].data, $scope, $rootScope, $compile, $modal, $http, false, blockUI);
            }
        } else if (templateSingleKey == "formStart") {
            var formStartHTML = "";
            var formStartID = data[i][templateSingleKey];
            console.log(formStartID);
            formStartHTML += "<form class='form-horizontal' name='form_"+formStartID+"' novalidate='' role='form'>";
            var formStartIDText = "formMain_"+formStartIndex;
            formStartHTML += "<div id='"+formStartIDText+"'></div>";
            formStart = true;

            angular.element(document.getElementById(id)).append($compile(formStartHTML)($scope));

            tempID = id;
            id = formStartIDText;
            console.log(formStartHTML, id);
        } else if (templateSingleKey == "formEnd") {
            var formEndHTML = "";
            var formEndID = data[i][templateSingleKey];
            console.log(formEndID);
            if (formStart) {
                formEndHTML += "</form>";
                var nextIDText = "nextID_"+nextID;
                formEndHTML += "<div id='"+nextIDText+"'></div>";
                nextID++;

                id = tempID;

                angular.element(document.getElementById(id)).append($compile(formEndHTML)($scope));

                id = nextIDText;

                console.log(formEndHTML);

                formStart = false;
            }

        } else if (templateSingleKey == "fieldset") {
            var fieldset_list = data[i][templateSingleKey];

            var fieldsetHTML = "";

            for (var fo1=0; fo1<fieldset_list.length; fo1++) {
                fieldsetHTML += "<div class='col-xs-6 field-panel'>";
                fieldsetHTML += "<fieldset class='col-xs-12'>";
                fieldsetHTML += "<legend>"+fieldset_list[fo1].title+"</legend>";
                fieldsetHTML += "<div class='col-xs-12 ng-scope'>";
                fieldsetHTML += "<div id='fieldset_"+fo1+"'></div>";
                fieldsetHTML += "</div>";
                fieldsetHTML += "</fieldset>";
                fieldsetHTML += "</div>";
            }

            angular.element(document.getElementById(id)).append($compile(fieldsetHTML)($scope));

            for (var fo=0; fo<fieldset_list.length; fo++) {
                templateLoad('fieldset_'+fo, fieldset_list[fo].data, $scope, $rootScope, $compile, $modal, $http, false, blockUI);
            }
        } else if (templateSingleKey == "tab") {
            tabId++;
            var tab_list = data[i][templateSingleKey];
            // console.log(tab_list);

            var tabHTML = "<div class='col-md-12'>";
            tabHTML += "<ul class='nav nav-tabs tablist' role='tablist'>";

            for (var tf=0; tf<tab_list.length; tf++) {

                if (tab_list[tf].setDataModel) {
                    // console.log(tab_list[tf].setDataModel);
                    setModelData['tab_'+tf+''+tabId] = tab_list[tf].setDataModel;
                }
                // setModelData['button'+templateFormButtons[d].action+templateFormButtons[d].modal[0].form[0].model+a+''+d] = tab_list[tf].setDataModel;
                tabHTML += "<li role='presentation' data-ng-click='tabLoad(\""+('tab_'+tf+''+tabId)+"\")' ng-class='{ active: " + tab_list[tf].active + "}'><a href='#tab_"+tf+''+tabId+"' aria-controls='tab_"+tf+''+tabId+"' role='tab' data-toggle='tab'>"+tab_list[tf].title+"</a></li>";

                // if (tab_list[tf].service != undefined) {
                //
                // }

                tabLoadList.push(
                    {
                        tab: 'tab_'+tf+''+tabId,
                        data: tab_list[tf].data
                    }
                );

            }
            tabHTML += "</ul>";

            tabHTML += "<div class='tab-content'>";
            for (var tc=0; tc<tab_list.length; tc++) {
                tabHTML += "<div role='tabpanel' ng-class='{active: " + tab_list[tc].active + "}' class='tab-pane' id='tab_"+tc+''+tabId+"'>";
                tabHTML += "</div>";
            }
            tabHTML += "</div>";
            tabHTML += "</div>";

            angular.element(document.getElementById(id)).append($compile(tabHTML)($scope));

            for (var tg=0; tg<tab_list.length; tg++) {
                // console.log();
                if (tab_list[tg].active && tab_list[tg].load != false) {
                    templateLoad('tab_'+tg+''+tabId, tab_list[tg].data, $scope, $rootScope, $compile, $modal, $http, false, blockUI);
                }
            }
        }
    }
}