package mn.gov.crc.nis;

import mn.gov.crc.nis.data.DataMvcConfiguration;
import mn.gov.crc.nis.data.DataMvcRegistrationConfiguration;
import mn.gov.crc.nis.security.SecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "mn.gov.crc.nis.dao")
@SpringBootApplication(scanBasePackages = {"mn.gov.crc.nis"})
@Import({SecurityConfig.class, DataMvcConfiguration.class, DataMvcRegistrationConfiguration.class})
public class NisApplication {
	public static void main(String[] args) {
		SpringApplication.run(NisApplication.class, args);
	}
}
