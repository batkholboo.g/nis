package mn.gov.crc.nis.service;

import mn.gov.crc.nis.model.*;
import mn.gov.crc.nis.sql.*;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

/**
 * Created by Ганбат Баяр /О.Монгол нэр/
 * Date: 2018/04/17 5:23 PM
 */
public interface SqlService {

    String getUserName();

    SqlModel getModel(String code);
    List<Map<String, Object>> listSQL(String code, Map<String, Object> params);
    ResponseEntity list(String code, Map<String, Object> params);

    Long totalSQL(String code, Map<String, Object> params);

    ResponseEntity total(String code, Map<String, Object> params);

    ResponseEntity reload() ;

    ResponseEntity generate(String table);

    ResponseEntity generate(String database, String table);

    Map getPkSQL(String code, Map<String, Object> params);

    ResponseEntity getPk(String code, Map<String, Object> params);

    Map getSQL(String code, Map<String, Object> params);
    ResponseEntity get(String code, Map<String, Object> params);

    Map saveSQL(String code, Map<String, Object> params);
    ResponseEntity save(String code, Map<String, Object> params);

    List<Map<String, Object>> patchSQL(String code, List<Map<String, Object>> list);

    ResponseEntity patch(String code, List<Map<String, Object>> list);

    Map deleteSQL(String code, Map<String, Object> params) ;

    ResponseEntity delete(String code, Long id);

    ResponseEntity execute(String code, Map<String, Object> params);
}
