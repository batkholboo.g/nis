package mn.gov.crc.nis.service.impl;

import lombok.extern.log4j.Log4j;
import mn.gov.crc.nis.model.SqlModel;
import mn.gov.crc.nis.service.SqlService;
import mn.gov.crc.nis.sql.SqlRepository;
import mn.gov.crc.nis.util.FuncUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ганбат Баяр /О.Монгол нэр/
 * Date: 2018/04/17 5:23 PM
 */
@Service
@Log4j
public class SqlServiceImpl implements SqlService {

    public SqlRepository sqlRepository;
    public String getUserName() {
        SecurityContext context = SecurityContextHolder.getContext();
        if (context.getAuthentication() != null && !(context.getAuthentication() instanceof AnonymousAuthenticationToken)) {
            User user = (User) context.getAuthentication().getPrincipal();
            return user.getUsername();
        }
        return null;
    }
    @Autowired
    public SqlServiceImpl(SqlRepository sqlRepository) {
        this.sqlRepository = sqlRepository;
        this.sqlRepository.reloadSQL();
    }

    private ResponseEntity getResponseEntity(Map result) {
        if (result != null) {
            FuncUtils.debug("get.end");
            return ResponseEntity.ok(result);
        } else {
            FuncUtils.debug("get.end");
            return ResponseEntity.ok(new HashMap<>());
        }
    }

    private ResponseEntity getResponseList(List<Map<String, Object>> result) {
        if (result != null && result.size() > 0) {
            FuncUtils.debug("get.end");
            return ResponseEntity.ok(result);
        } else {
            FuncUtils.debug("get.end");
            return ResponseEntity.ok(new ArrayList<>());
        }
    }

    public SqlModel getModel(String code) {
        System.out.println("getModel " + code);
        Long id;
        try {
            id = Long.parseLong(code);
            return sqlRepository.getModel(id);
        } catch (Exception e) {
            return sqlRepository.getModel(code);
        }
    }

    public List<Map<String, Object>> listSQL(String code, Map<String, Object> params) {
        SqlModel model = getModel(code);
        if (model == null) return null;
        return sqlRepository.listSQL(model, params);
    }

    public ResponseEntity list(String code, Map<String, Object> params) {
        FuncUtils.debug("list.start");
        try {
            List<Map<String, Object>> list = listSQL(code, params);
            if (list != null && list.size() > 0) {
                System.out.println("list " + list.size());
                FuncUtils.debug("list.end");
                return ResponseEntity.ok(list);
            } else {
                FuncUtils.debug("list.end");
                return ResponseEntity.ok(new ArrayList<>());
            }
        } catch (Exception ex) {
            log.error("errors", ex);
            FuncUtils.debug("list.end");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public Long totalSQL(String code, Map<String, Object> params) {
        SqlModel model = getModel(code);
        if (model == null) return null;
        return sqlRepository.totalSQL(model, params);
    }

    public ResponseEntity total(String code, Map<String, Object> params) {
        try {
            FuncUtils.debug("total.start");
            Long total = totalSQL(code, params);
            if (total != null) {
                System.out.println("findByDiretoryList " + total);
                FuncUtils.debug("total.end");
                return ResponseEntity.ok(total);
            } else {
                FuncUtils.debug("total.end");
                return ResponseEntity.ok(0);
            }
        } catch (Exception ex) {
            log.error("errors", ex);
            FuncUtils.debug("total.end");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity reload() {
        try {

            return ResponseEntity.ok(sqlRepository.reloadSQL());
        } catch (Exception ex) {
            log.error("errors", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity generate(String table) {
        try {
            SqlModel sqlModel = sqlRepository.generateSqlTable(table);
            if (sqlModel != null) {
                System.out.println("sqlModel " + sqlModel);
                return ResponseEntity.ok(sqlModel);
            } else {
                return ResponseEntity.notFound().build();
            }

        } catch (Exception ex) {
            log.error("errors", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity generate(String database, String table) {
        try {
            SqlModel sqlModel = sqlRepository.generateSqlTable(database, table);
            if (sqlModel != null) {
                System.out.println("sqlModel " + sqlModel);
                return ResponseEntity.ok(sqlModel);
            } else {
                return ResponseEntity.notFound().build();
            }

        } catch (Exception ex) {
            log.error("errors", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public Map getPkSQL(String code, Map<String, Object> params) {
        SqlModel model = getModel(code);
        System.out.println("getModel " + model);
        if (model == null) return null;
        if (model.getMapper() != null && model.getMapper().getPk() != null) {
            return sqlRepository.objectSQL(model, params);
        }
        return null;
    }

    public ResponseEntity getPk(String code, Map<String, Object> params) {
        try {
            FuncUtils.debug("getPk.start " + params);
            Map result = getPkSQL(code, params);
            return getResponseEntity(result);
        } catch (Exception ex) {
            FuncUtils.debug("getPk.end");
            log.error("errors", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public Map getSQL(String code, Map<String, Object> params) {
        SqlModel model = getModel(code);
        if (model == null) return null;
        return sqlRepository.objectSQL(model, params);
    }

    public ResponseEntity get(String code, Map<String, Object> params) {
        try {
            FuncUtils.debug("get.start");
            Map result = getSQL(code, params);
            return getResponseEntity(result);
        } catch (Exception ex) {
            FuncUtils.debug("get.end");
            log.error("errors", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public Map saveSQL(String code, Map<String, Object> params) {
        SqlModel model = getModel(code);
        if (model == null) return null;
        if (model.getMapper() != null) {
            return sqlRepository.saveSQL(model, params);
        } else {
            return null;
        }

    }

    public ResponseEntity save(String code, Map<String, Object> params) {
        try {
            FuncUtils.debug("save.start" + params);
            Map result = saveSQL(code, params);
            if (result == null) return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            return getResponseEntity(result);
        } catch (Exception ex) {
            FuncUtils.debug("save.end");
            log.error("errors", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public List<Map<String, Object>> patchSQL(String code, List<Map<String, Object>> list) {
        SqlModel model = getModel(code);
        System.out.println("model " + model);
        if (model == null) return null;
        if (model.getMapper() != null) {
            return sqlRepository.patchSQL(model, list);
        } else {
            return null;
        }

    }

    public ResponseEntity patch(String code, List<Map<String, Object>> list) {
        try {
            FuncUtils.debug("patch.start" + list);
            List<Map<String, Object>> result = patchSQL(code, list);
            if (result == null) return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            return getResponseList(result);
        } catch (Exception ex) {
            FuncUtils.debug("save.end");
            log.error("errors", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public Map deleteSQL(String code, Map<String, Object> params) {
        SqlModel model = getModel(code);
        System.out.println("model " + model);
        if (model == null) return null;
        if (model.getMapper() != null) {
            return sqlRepository.deleteSQL(model, params);
        } else {
            return null;
        }

    }

    public ResponseEntity delete(String code, Long id) {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("id", id);
            FuncUtils.debug("delete.start" + params);
            Map result = deleteSQL(code, params);
            return getResponseEntity(result);
        } catch (Exception ex) {
            FuncUtils.debug("delete.end");
            log.error("errors", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    public ResponseEntity execute(String code, Map<String, Object> params) {
        try {
            SqlModel model = getModel(code);
            if (model == null) return null;
            String sql = model.getTableName() != null ? model.getTableName() : model.getSqlQuery();
            int result = sqlRepository.executeSQL(sql, params);
            return ResponseEntity.ok(result);
        } catch (Exception ex) {
            log.error("errors", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
