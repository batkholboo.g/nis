package mn.gov.crc.nis.util;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mn.gov.crc.nis.model.SqlModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class FuncUtils {

    public final static SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");

    public final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public final static SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    public final static ObjectMapper json = new ObjectMapper();

    public static void debug(String value) {
        System.out.println(sdfd.format(new Date()) + ": " + value);
    }

    private static List<String> ignoreFields = Arrays.asList(new String[]{
            "version", "actionFlag", "createdBy", "createdDate", "updatedBy", "updatedDate", "_rowState",
            "accessLevel", "primaryId"
    });
    private static List<String> ignoreFieldActiveFlags = Arrays.asList(new String[]{
            "version", "activeFlag", "actionFlag", "createdBy", "createdDate", "updatedBy", "updatedDate", "_rowState",
            "accessLevel", "primaryId"
    });


    public static Map<String, Object> toJsonMapper(String str) {
        Map<String, Object> value = new HashMap<>();
        try {
            value = json.readValue(str, HashMap.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String toJsonString(Object value) {
        try {
            return json.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String nowDate() {
        return sdf.format(new Date());
    }

    private static Object parseValue(String value) {
        String[] split = value.split("[,]");
        if (split.length == 1) return split[0];
        return Arrays.stream(split).collect(Collectors.toList());
    }

    public static Map<String, Object> requestToMap(Map<String, String[]> request) {
        Map<String, Object> param = new HashMap<>();
        for (String key : request.keySet()) {
            String[] values = request.get(key);
            param.put(key, parseValue(values[0]));
        }
        return param;
    }

    public static Date toDate(String date) {
        try {
            return sd.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Boolean isOperation(String operator) {
        operator = operator.toUpperCase();
        return operator.equals("IN") || operator.equals("NOT_IN")
                || operator.equals(">") || operator.equals("<") ||
                operator.equals(">=") || operator.equals("<=")
                || operator.equals("=") || operator.equals("BETWEEN");
    }

    private static String toList(List<String> list, String type) {
        List<String> params = new ArrayList<>();
        for (String aList : list) {
            if (!isOperation(aList)) {
                params.add(aList);
            }
        }
        String value;
        switch (type) {
            case "NUMBER":
                value = String.join(",", params);
                break;

            default:
                value = String.join(",", params.stream().map(n -> "'" + n + "'").collect(Collectors.toList()));
        }
        if (params.size() > 0) value = "(" + value + ")";
        return value;
    }

    public static Object valueToSQL(Map<String, Object> value, String key, String type, Boolean select) {
        String resultValue = getValueMap(value, key, String.class);
        if (resultValue != null && (resultValue.toUpperCase().equals("NULL")
                || resultValue.toUpperCase().equals("!NULL"))) {
            value.put(key, null);
            return "NULL";
        }
        if (value.get(key) instanceof List) {
            List list = (List) value.get(key);
            return toList(list, type);
        }

        switch (type) {
            case "BOOL":
                Boolean bool = getValueMap(value, key, Boolean.class);
                return bool != null ? (bool ? 1 : 0) : null;
            case "NUMBER":
            case "INT":
                return getValueMap(value, key, Object.class);
            case "DATE":
                if (select) {
                    if (resultValue.length() > 10)
                        return "TO_DATE('" + getValueMap(value, key, String.class) + "','YYYY-MM-DD HH24:MI:SS') AND "
                                + "TO_DATE('" + getValueMap(value, key, String.class) + "','YYYY-MM-DD HH24:MI:SS') + 1";
                    else
                        return "TO_DATE('" + getValueMap(value, key, String.class) + "','YYYY-MM-DD') AND "
                                + "TO_DATE('" + getValueMap(value, key, String.class) + "','YYYY-MM-DD') + 1";
                } else {
                    return "TO_DATE('" + getValueMap(value, key, String.class) + "','YYYY-MM-DD HH24:MI:SS')";
                }

            case "DATE_TIME":
                if (select) {
                    if (resultValue.length() > 10)
                        return "TO_TIMESTAMP('" + getValueMap(value, key, String.class) + "','YYYY-MM-DD HH24:MI:SS.FF3') AND "
                                + "TO_TIMESTAMP('" + getValueMap(value, key, String.class) + "','YYYY-MM-DD HH24:MI:SS.FF3') + 1";
                    else
                        return "TO_TIMESTAMP('" + getValueMap(value, key, String.class) + "','YYYY-MM-DD') AND "
                                + "TO_TIMESTAMP('" + getValueMap(value, key, String.class) + "','YYYY-MM-DD') + 1";
                } else {
                    return "TO_TIMESTAMP('" + getValueMap(value, key, String.class) + "','YYYY-MM-DD HH24:MI:SS')";
                }

            case "CLOB":
            case "BLOB":
//                System.out.println("BlOB " + key);
                return ":" + key;
            case "STR":
                return "'" + getValueMap(value, key, String.class).replaceAll("'", "''") + "'";
            default:
                return "'" + getValueMap(value, key, Object.class) + "'";

        }
    }

    public static Object fieldToSQL(Map<String, Object> value, String key, String type) {
//        String resultValue = getValueMap(value, key, String.class);
//        if (resultValue != null && (resultValue.toUpperCase().equals("NULL")
//                || resultValue.toUpperCase().equals("!NULL"))) {
//            value.put(key, null);
//            return "NULL";
//        }
        if (value.get(key) instanceof List) {
            List list = (List) value.get(key);
            return toList(list, type);
        }
        switch (type) {
            case "BOOL":
                Boolean bool = getValueMap(value, key, Boolean.class);
                value.put(key, bool != null ? (bool ? 1 : 0) : null);
                return ":" + key;

            case "STR":
                return "TO_CHAR(:" + key + ")";
            case "DATE":
                return "TO_DATE(TO_CHAR(:" + key + "),'YYYY-MM-DD HH24:MI:SS')";
            case "DATE_TIME":
                return "TO_TIMESTAMP(TO_CHAR(:" + key + "),'YYYY-MM-DD HH24:MI:SS.FF3')";
            default:
                return ":" + key;
        }
    }

    public static Boolean rowEquas(Map<String, Object> oldRow, Map<String, Object> newRow, SqlModel sqlModel, Boolean activeflag) {
        for (String key : newRow.keySet()) {
//            System.out.println("key " + key);
            if ((activeflag ? !ignoreFieldActiveFlags.contains(key) : !ignoreFields.contains(key))
                    && ((sqlModel.getMapper().getTypeMapper().containsKey(key) && !sqlModel.getMapper().getTypeMapper().get(key).equals("LIST"))
                    || (sqlModel.getMapper().getTypeMapper().containsKey(key) && !sqlModel.getMapper().getTypeMapper().get(key).equals("MAP")))
                    && !Objects.equals(getValueMap(oldRow, key, String.class), getValueMap(newRow, key, String.class))) {
                return false;
            }

        }
        return true;
    }

    public static <T> T getValueMap(Map<String, Object> value, String key, Class<T> valueClass) {
        String[] props = key.split("[.]", 2);
        if (props.length > 1) {
            if (value.containsKey(props[0])) {
                Object parent = value.get(props[0]);
                if (parent instanceof Map) {
                    return (T) getValueMap((Map<String, Object>) parent, props[1], valueClass);
                } else {
                    System.out.println("not map " + parent);
                    return null;
                }
            } else {
                return null;
            }


        } else {
            if (value.containsKey(key) && value.get(key) != null && value.get(key) != "") {
                Object returnValue = value.get(key);
                switch (valueClass.getSimpleName()) {
                    case "Boolean":
                        if (returnValue.toString().toUpperCase().equals("TRUE") || returnValue.toString().equals("1"))
                            return (T) Boolean.valueOf(true);
                        else return (T) Boolean.valueOf(false);
                    case "Long":
                        return (T) Long.valueOf(returnValue.toString());
                    case "Integer":
                        return (T) Integer.valueOf(returnValue.toString());
                    case "Date":
                        try {
                            return (T) sdf.parse(returnValue.toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                            return null;
                        }
                    case "String":
                        return (T) returnValue.toString();
                    default:
                        return (T) returnValue;
                }
            } else {
                return null;
            }

        }
    }

    public static String toType(int dataType, int decimaldigits, int size) {
        switch (dataType) {
            case 3:
                if (size == 1) return "BOOL";
                if (size == 10) return "INT";
                if (decimaldigits > 0) return "DECIMAL";
                return "LONG";
            case 93:
                return size == 7 ? "DATE" : "DATE_TIME";

            case 2004:
                return "BLOB";
            case 2005:
                return "CLOB";
            default:
                return "STR";
        }
    }

    public static String toColumn(String field, SqlModel sqlModel) {
        if (sqlModel != null &&
                sqlModel.getMapper() != null &&
                sqlModel.getMapper().getFieldMapper().containsKey(field)) {
            return sqlModel.getMapper().getFieldMapper().get(field);
        } else {
            String[] columns = field.split("(?=\\p{Upper})");
            return String.join("_", columns).toUpperCase();
        }

    }

    public static String toField(String column) {
        StringBuilder fieldName = new StringBuilder(column.toLowerCase());
        String[] columnNames = fieldName.toString().split("[_]");
        fieldName = getStringBuilder(fieldName, columnNames);
        return fieldName.toString().replaceAll("_", "");
    }

    public static StringBuilder getStringBuilder(StringBuilder fieldName, String[] columnNames) {
        if (columnNames.length > 1) {
            fieldName = new StringBuilder();
            for (int j = 0; j < columnNames.length; j++) {
                if (j == 0) {
                    fieldName = new StringBuilder(columnNames[j]);
                } else {
                    fieldName.append(columnNames[j].substring(0, 1).toUpperCase());
                    fieldName.append(columnNames[j], 1, columnNames[j].length());
                }
            }
        }
        return fieldName;
    }
}
