package mn.gov.crc.nis.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/25 2:26 PM
 */
@EnableGlobalMethodSecurity(securedEnabled = true)
@Configuration
public class SecurityMethodConfig extends GlobalMethodSecurityConfiguration {

}
