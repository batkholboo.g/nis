package mn.gov.crc.nis.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import mn.gov.crc.nis.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/25 4:52 PM
 */
@Service
public class JwtTokenService {

    @Autowired
    SecurityService securityService;

    private Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();

    @Transactional
    public String generateToken(SysUser user, Date expireDate) throws UnsupportedEncodingException {
        String secret = passwordEncoder.encodePassword(user.getLoginpass(), user.getSalt());
//        Integer branchId = Optional.ofNullable(user.getBranch().getId().intValue()).orElse(0);
        //System.out.println(secret+branchId);
        return JWT.create()
                .withSubject(user.getLoginname())
//                .withClaim("branchId", branchId)
                .withClaim("fullname", user.getFullname())
                .withExpiresAt(expireDate)
                .sign(Algorithm.HMAC512(secret));
    }

    private String getSubject(String strToken) {
        return JWT.decode(strToken).getSubject();
    }

    @Transactional
    public Authentication getToken(String strToken) throws UnsupportedEncodingException {
        SysUser user = securityService.getSysUserByUsername(getSubject(strToken));
        String secret = passwordEncoder.encodePassword(user.getLoginpass(), user.getSalt());
        JWTVerifier verifier = JWT.require(Algorithm.HMAC512(secret)).build();
        DecodedJWT jwt = verifier.verify(strToken);
        if (jwt.getExpiresAt().getTime() > System.currentTimeMillis()) {
            List<RoleAuthority> authorities = securityService.getPermissions(user);
            //System.out.println("----"+authorities);
            return new UsernamePasswordAuthenticationToken(jwt.getSubject(), jwt.getSubject(), authorities);
        }

        throw new JWTVerificationException("Token expired!!!");
    }
}
