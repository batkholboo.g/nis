package mn.gov.crc.nis.security;

import lombok.extern.log4j.Log4j;
import mn.gov.crc.nis.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/25 12:58 PM
 */
@RestController
@RequestMapping("/auth")
@Log4j
public class SecurityController {

    @Autowired
    SecurityService securityService;

    @PostMapping("login")
    public ResponseEntity<Token> login(@RequestBody AuthRequest authRequest) {
        Token token = securityService.login(authRequest.getUsername(), authRequest.getPassword());
        if (token != null)
            return ResponseEntity.ok(token);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @PostMapping("change-password")
    @Transactional
    public ResponseEntity<Token> changePassword(@RequestBody ChangePasswordRequest changePasswordRequest) {
        try {
            SysUser sysUser = securityService.changePassword(
                    changePasswordRequest.getPassword(),
                    changePasswordRequest.getNewPassword()
            );
            Token token = securityService.login(sysUser.getLoginname(), changePasswordRequest.getNewPassword());
            if (token != null)
                return ResponseEntity.ok(token);
        } catch (AuthenticationException e) {
            log.error(e.getMessage(), e);
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}
