package mn.gov.crc.nis.security;

import mn.gov.crc.nis.entity.SysUser;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class SecurityPasswordService {

    private static final Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();

    public boolean checkPassword(SysUser user, String rawPassword) {
        String encodedPassword = passwordEncoder.encodePassword(rawPassword.toLowerCase(), user.getSalt());
        if (encodedPassword.equals(user.getLoginpass()))
            return true;
        return false;
    }

    public void changePassword(SysUser user, String rawPassword) {
        user.setSalt(passwordEncoder.encodePassword(UUID.randomUUID().toString(), null));
        user.setLoginpass(passwordEncoder.encodePassword(rawPassword.toLowerCase(), user.getSalt()));
    }
}
