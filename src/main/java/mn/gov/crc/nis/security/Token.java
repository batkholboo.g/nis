package mn.gov.crc.nis.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/25 1:33 PM
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Token {
    private String username;
    private String token;
    private Date expireDate;
}
