package mn.gov.crc.nis.security;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/25 2:07 PM
 */
public class TokenHelper {
    private static final String TOKEN_NAME = "Authorization";

    private TokenHelper() {
    }

    public static String getToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(TOKEN_NAME);
        if (bearerToken != null) {
            return bearerToken.substring(7);
        }
        return null;
    }
}
