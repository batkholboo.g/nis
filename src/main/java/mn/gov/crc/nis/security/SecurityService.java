package mn.gov.crc.nis.security;

import lombok.extern.log4j.Log4j;
import mn.gov.crc.nis.dao.SysUserDAO;
import mn.gov.crc.nis.entity.SysRole;
import mn.gov.crc.nis.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/25 1:34 PM
 */
@Service
@Log4j
public class SecurityService {

    @Autowired
    SysUserDAO sysUserDAO;

    @Autowired
    JwtTokenService jwtTokenService;

    @Autowired
    SecurityPasswordService passwordService;

    public Token login(String username, String password) {
        SysUser user = sysUserDAO.findByLoginname(username);
        if (user != null && passwordService.checkPassword(user, password))
            return getToken(user);
        return null;
    }

    private Token getToken(SysUser user) {
        try {
            Date expireDate = new Date(System.currentTimeMillis() + (7 * 24 * 60 * 60 * 1000));
            String strToken = jwtTokenService.generateToken(user, expireDate);
            Token token = new Token(user.getLoginname(), strToken, expireDate);
            return token;
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public SysUser getSysUserByUsername(String username) {
        return sysUserDAO.findByLoginname(username);
    }

    public List<RoleAuthority> getPermissions(SysUser sysUser) {
        List<RoleAuthority> roleAuthorities = new ArrayList<>();
        List<String> roles = sysUser.getGroup().getSysRoles().stream().map(SysRole::getCode).collect(Collectors.toList());
        System.out.println(roles);
        for (String role : roles) {
            roleAuthorities.add(new RoleAuthority(role));
        }
        return roleAuthorities;
    }

    public SysUser changePassword(String password, String newPassword) {
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        SysUser user = sysUserDAO.findByLoginname(authentication.getPrincipal().toString());
        if (user != null && passwordService.checkPassword(user, password)) {
            passwordService.changePassword(user, newPassword);
            sysUserDAO.save(user);
            return user;
        }
        throw new UsernameNotFoundException("username or password is invalid!!!");
    }
}
