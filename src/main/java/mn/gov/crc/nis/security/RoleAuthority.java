package mn.gov.crc.nis.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/25 2:10 PM
 */
@Data
@AllArgsConstructor
public class RoleAuthority implements GrantedAuthority{
    private final String authority;
}
