package mn.gov.crc.nis.security;

import lombok.Data;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 1/14/18 5:28 PM
 */
@Data
public class ChangePasswordRequest {
    private String password;
    private String newPassword;

}
