package mn.gov.crc.nis.entity;

import lombok.Data;
import org.springframework.security.access.annotation.Secured;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Bold-Erdene on 27/11/2018.
 */

@Entity
@Table(name = "SYS_ROLE", schema = "CRC")
//@SequenceGenerator(name = "sysRoleGenerator", sequenceName = "MVIS.SEQ_SYS_ROLE", allocationSize = 1)
@Data
//@Secured("ROLE_TEST")
public class SysRole extends BaseEntity 
{
	@Id
	@Column(name = "ID")
	private String id;
	@Basic
	@Column(name = "CODE")
	private String code;
	@Basic
	@Column(name = "NAME")
	private String name;
	@Basic
	@Column(name = "TYPE")
	private Long type;
	@Basic
	@Column(name = "CREATED_BY")
	private Long createdBy;
	@Basic
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Basic
	@Column(name = "UPDATED_BY")
	private Long updatedBy;
	@Basic
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Basic
	@Column(name = "BRANCH_ID")
	private Long branchId;
	@Basic
	@Column(name = "BRANCH_BY")
	private Long branchBy;

}