package mn.gov.crc.nis.entity;

import lombok.Data;
import org.springframework.security.access.annotation.Secured;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Bold-Erdene on 27/11/2018.
 */

@Entity
@Table(name = "SYS_REPORT", schema = "CRC")
@SequenceGenerator(name = "sysReportGenerator", sequenceName = "CRC.SEQ_SYS_REPORT", allocationSize = 1)
@Data
//@Secured("ROLE_TEST")
public class SysReport extends BaseEntity 
{
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "sysReportGenerator", strategy = GenerationType.SEQUENCE)
	private Long id;
	@Basic
	@Column(name = "NAME")
	private String name;
	@Basic
	@Column(name = "REPORT")
	private String report;
	@Basic
	@Column(name = "PARENT_ID")
	private Long parentId;
	@Basic
	@Column(name = "STATUS")
	private Long status;
	@Basic
	@Column(name = "CREATED_BY")
	private Long createdBy;
	@Basic
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Basic
	@Column(name = "UPDATED_BY")
	private Long updatedBy;
	@Basic
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Basic
	@Column(name = "BRANCH_ID")
	private Long branchId;
	@Basic
	@Column(name = "BRANCH_BY")
	private Long branchBy;

}