package mn.gov.crc.nis.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import org.springframework.security.access.annotation.Secured;


/**
 * Created by Bold-Erdene on 14/03/2019.
 */

@Entity
@Table(name = "OPERATOR_DETAIL", schema = "CRC")
@SequenceGenerator(name = "operatorDetailGenerator", sequenceName = "CRC.SEQ_OPERATOR_DETAIL", allocationSize = 1)
@Data
@Secured("ROLE_TEST")
public class OperatorDetail extends BaseEntity
{
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "operatorDetailGenerator", strategy = GenerationType.SEQUENCE)
	private Long id;
	@Basic
	@Column(name = "PHONE_NUMBER")
	private Long phoneNumber;
	@Basic
	@Column(name = "DESCRIPTION")
	private String desc;
	@Column(name = "CREATED_BY")
	private Long createdBy;
	@Basic
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Basic
	@Column(name = "UPDATED_BY")
	private Long updatedBy;
	@Basic
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "REGISTERED_DATE")
	private Date branchNo;
	@ManyToOne
	@JoinColumn(name = "OPERATOR_ID")
	private RefOperator operator;
	@ManyToOne
	@JoinColumn(name = "SERVICE_TYPE_ID")
	private RefServiceType serviceType;
	@ManyToOne
	@JoinColumn(name = "OWN_TYPE_ID")
	private RefOwnType ownType;
	@ManyToOne
	@JoinColumn(name = "TECHNOLOGY_ID")
	private RefTechnology technology;
	@ManyToOne
	@JoinColumn(name = "PHONE_ID")
	private RefPhone phone;
	@ManyToOne
	@JoinColumn(name = "IS_REGISTER_ID")
	private RefIsRegister isRegister;
	@ManyToOne
	@JoinColumn(name = "STATUS")
	private RefStatus status;
}