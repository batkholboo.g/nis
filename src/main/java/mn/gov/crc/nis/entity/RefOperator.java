package mn.gov.crc.nis.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Bold-Erdene on 14/03/2019.
 */

@Entity
@Table(name = "REF_OPERATOR", schema = "CRC")
@SequenceGenerator(name = "refOperatorGenerator", sequenceName = "CRC.SEQ_REF_OPERATOR", allocationSize = 1)
@Data
//@Secured("ROLE_TEST")
public class RefOperator extends BaseEntity
{
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "refOperatorGenerator", strategy = GenerationType.SEQUENCE)
	private Long id;
//	@Basic
//	@Column(name = "CODE")
//	private String code;
	@Basic
	@Column(name = "NAME")
	private String name;
//	@Basic
//	@Column(name = "STATUS")
//	private Long status;
//	@Basic
//	@Column(name = "CREATED_BY")
//	private Long createdBy;
//	@Basic
//	@Column(name = "CREATED_DATE")
//	private Date createdDate;
//	@Basic
//	@Column(name = "UPDATED_BY")
//	private Long updatedBy;
//	@Basic
//	@Column(name = "UPDATED_DATE")
//	private Date updatedDate;

}