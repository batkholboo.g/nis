package mn.gov.crc.nis.entity;

import lombok.Data;
import org.springframework.security.access.annotation.Secured;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Bold-Erdene on 11/11/2017.
 */

@Entity
@Table(name = "SYS_USER", schema = "CRC")
@SequenceGenerator(name = "sysUserGenerator", sequenceName = "CRC.SEQ_SYS_USER", allocationSize = 1)
@Data
//@Secured("ROLE_TEST")
public class SysUser extends BaseEntity {
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "sysUserGenerator", strategy = GenerationType.SEQUENCE)
	private Long id;
	@Basic
	@Column(name = "FULLNAME")
	private String fullname;
	@Basic
	@Column(name = "STATUS")
	private Long status;
	@Basic
	@Column(name = "LOGINNAME")
	private String loginname;
	@Basic
	@Column(name = "LOGINPASS")
	private String loginpass;
	@Basic
	@Column(name = "SALT")
	private String salt;

//	@ManyToMany
//	@JoinTable(name = "SYS_BRANCH", schema = "MVIS",
//			joinColumns = @JoinColumn(name = "ID"),
//			inverseJoinColumns = @JoinColumn(name = "BRANCH_ID"))
//	private List<SysBranch> branches;
//	@ManyToOne
//	@JoinColumn(name = "BRANCH_ID")
//	private SysBranch branch;

	//	@ManyToMany
//	@JoinTable(name = "SYS_GROUP", schema = "MVIS",
//			joinColumns = @JoinColumn(name = "ID"),
//			inverseJoinColumns = @JoinColumn(name = "GROUP_ID"))
//	private List<SysGroup> sysGroups;
	@ManyToOne
	@JoinColumn(name = "GROUP_ID")
	private SysGroup group;

//	@Basic
//	@Column(name = "GROUP_ID")
//	private Long groupId;
//	@Basic
//	@Column(name = "BRANCH_ID")
//	private Long branchId;
	@Basic
	@Column(name = "LN_ENCRYPTED")
	private Long lnEncrypted;
	@Basic
	@Column(name = "CREATED_BY")
	private Long createdBy;
	@Basic
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Basic
	@Column(name = "UPDATED_BY")
	private Long updatedBy;
	@Basic
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Basic
	@Column(name = "BRANCH_BY")
	private Long branchBy;
	@Basic
	@Column(name = "OLD_ID")
	private Long oldId;
}