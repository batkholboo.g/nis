package mn.gov.crc.nis.entity;

import lombok.Data;
import org.springframework.security.access.annotation.Secured;

import javax.persistence.*;

/**
 * Created by Bold-Erdene on 14/03/2019.
 */

@Entity
@Table(name = "SYS_GROUP_ROLE", schema = "CRC")
@SequenceGenerator(name = "sysGroupRoleGenerator", sequenceName = "CRC.SEQ_SYS_GROUP_ROLE", allocationSize = 1)
@Data
//@Secured("ROLE_TEST")
public class SysGroupRole extends BaseEntity 
{
	@Basic
	@Column(name = "GROUP_ID")
	private Long groupId;
	@Basic
	@Column(name = "ROLE_ID")
	private Long roleId;
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "sysGroupRoleGenerator", strategy = GenerationType.SEQUENCE)
	private Long id;

}