package mn.gov.crc.nis.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by Bold-Erdene on 14/03/2019.
 */

@Entity
@Table(name = "REF_IS_REGISTER", schema = "CRC")
@SequenceGenerator(name = "refIsRegisterGenerator", sequenceName = "CRC.SEQ_REF_IS_REGISTER", allocationSize = 1)
@Data
//@Secured("ROLE_TEST")
public class RefIsRegister extends BaseEntity
{
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "refIsRegisterGenerator", strategy = GenerationType.SEQUENCE)
	private Long id;
//	@Basic
//	@Column(name = "CODE")
//	private String code;
	@Basic
	@Column(name = "NAME")
	private String name;
//	@Basic
//	@Column(name = "STATUS")
//	private Long status;
//	@Basic
//	@Column(name = "CREATED_BY")
//	private Long createdBy;
//	@Basic
//	@Column(name = "CREATED_DATE")
//	private Date createdDate;
//	@Basic
//	@Column(name = "UPDATED_BY")
//	private Long updatedBy;
//	@Basic
//	@Column(name = "UPDATED_DATE")
//	private Date updatedDate;

}