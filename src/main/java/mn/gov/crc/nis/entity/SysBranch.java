package mn.gov.crc.nis.entity;

import lombok.Data;
import org.springframework.security.access.annotation.Secured;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Bold-Erdene on 14/03/2019.
 */

@Entity
@Table(name = "SYS_BRANCH", schema = "CRC")
@SequenceGenerator(name = "sysBranchGenerator", sequenceName = "CRC.SEQ_SYS_BRANCH", allocationSize = 1)
@Data
//@Secured("ROLE_TEST")
public class SysBranch extends BaseEntity 
{
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "sysBranchGenerator", strategy = GenerationType.SEQUENCE)
	private Long id;
	@Basic
	@Column(name = "PARENT_ID")
	private Long parentId;
	@Basic
	@Column(name = "AIMAG_ID")
	private Long aimagId;
	@Basic
	@Column(name = "NAME")
	private String name;
	@Basic
	@Column(name = "EMAIL")
	private String email;
	@Basic
	@Column(name = "BRANCH_LEVEL")
	private Long branchLevel;
	@Basic
	@Column(name = "IS_SYNC")
	private Long isSync;
	@Basic
	@Column(name = "STATUS")
	private Long status;
	@Basic
	@Column(name = "CREATED_BY")
	private Long createdBy;
	@Basic
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Basic
	@Column(name = "UPDATED_BY")
	private Long updatedBy;
	@Basic
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Basic
	@Column(name = "BRANCH_ID")
	private Long branchId;
	@Basic
	@Column(name = "BRANCH_BY")
	private Long branchBy;
	@Basic
	@Column(name = "IS_PRIVATE")
	private Long isPrivate;
	@Basic
	@Column(name = "BRANCH_NO")
	private String branchNo;
	@Basic
	@Column(name = "OLD_ID")
	private Long oldId;
	@Basic
	@Column(name = "IS_WEIGHT_LIMITED")
	private Long isWeightLimited;

}