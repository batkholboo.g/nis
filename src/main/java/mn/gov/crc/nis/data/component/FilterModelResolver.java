package mn.gov.crc.nis.data.component;

import mn.gov.crc.nis.data.EntityMapper;
import mn.gov.crc.nis.data.filter.FilterModel;
import mn.gov.crc.nis.data.filter.Operator;
import mn.gov.crc.nis.data.util.DateTimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mapping.PersistentEntity;
import org.springframework.data.mapping.PersistentProperty;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/09 8:25 PM
 */
@Component
public class FilterModelResolver {

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    DateTimeUtils dateTimeUtils;

    public List<FilterModel> resolve(String[] filters, PersistentEntity<?, ?> persistentEntity) {
        ArrayList<FilterModel> filterModels = new ArrayList<>();
        for (String filter : Optional.ofNullable(filters).orElse(new String[]{})) {
            String[] datas = filter.split(",");
            filterModels.add(createFilterModel(datas[0], datas, persistentEntity));
        }
        return filterModels;
    }

    private FilterModel createFilterModel(String propertyName, String[] datas, PersistentEntity<?, ?> persistentEntity) {
        if (propertyName.contains(".")) {
            String parentPropertyName = propertyName.substring(0, propertyName.indexOf("."));
            propertyName = propertyName.substring(propertyName.indexOf(".") + 1);
            PersistentProperty<?> persistentProperty = persistentEntity.getPersistentProperty(parentPropertyName);
            Class<?> type = persistentProperty.getActualType();
            return createFilterModel(propertyName, datas, entityMapper.getPersistentEntity(type));
        }

        Class<?> type = persistentEntity.getPersistentProperty(propertyName).getActualType();

        Operator operator = Operator.getFromString(datas[1]);
        operator.verify(type);

        Object value = convertValue(operator, datas, type);

        return new FilterModel(datas[0], operator, value);
    }

    private Object convertValue(Operator operator, String[] datas, Class<?> type) {
        switch (operator) {
            case BETWEEN:
            case IN:
                return convertValueByList(datas, type);
            default:
                return convertValue(datas[2], type);
        }
    }

    private List convertValueByList(String[] datas, Class<?> type) {
        ArrayList values = new ArrayList();
        for (int i = 2; i < datas.length; i++) {
            values.add(convertValue(datas[i], type));
        }
        return values;
    }

    private Object convertValue(String value, Class type) {
        if (value.toLowerCase().equals("null")) return null;
        if (String.class.isAssignableFrom(type)) return value;
        if ("".equals(value)) return null;

        if (Date.class.isAssignableFrom(type)) return dateTimeUtils.parseDate(value);
        if (Boolean.class == type || Boolean.TYPE == type) return Boolean.parseBoolean(value);
        if (Byte.class == type || Byte.TYPE == type) return Byte.parseByte(value);
        if (Short.class == type || Short.TYPE == type) return Short.parseShort(value);
        if (Integer.class == type || Integer.TYPE == type) return Integer.parseInt(value);
        if (Long.class == type || Long.TYPE == type) return Long.parseLong(value);
        if (Float.class == type || Float.TYPE == type) return Float.parseFloat(value);
        if (Double.class == type || Double.TYPE == type) return Double.parseDouble(value);
        if (BigDecimal.class == type) return new BigDecimal(value);
        if (BigInteger.class == type) return new BigInteger(value, 10);

        throw new IllegalArgumentException("\"" + value + "\" not parsable to " + type.getName());
    }
}
