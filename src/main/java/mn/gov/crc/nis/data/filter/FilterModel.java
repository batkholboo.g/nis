package mn.gov.crc.nis.data.filter;

import lombok.Data;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/10/26 3:19 PM
 */
@Data
public class FilterModel {
    private String name;
    private Operator operator;
    private Object value;
    private Object from;
    private Object to;

    public FilterModel() {
    }

    public FilterModel(String name, Operator operator, Object value) {
        this(name, operator, value, null, null);
    }

    public FilterModel(String name, Operator operator, Object from, Object to) {
        this(name, operator, null, from, to);
    }

    public FilterModel(String name, Operator operator, Object value, Object from, Object to) {
        this.name = name;
        this.operator = operator;
        this.value = value;
        this.from = from;
        this.to = to;
    }
}
