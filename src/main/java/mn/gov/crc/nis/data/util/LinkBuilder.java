package mn.gov.crc.nis.data.util;

import lombok.extern.java.Log;
import mn.gov.crc.nis.data.EntityMapper;
import mn.gov.crc.nis.data.controller.EntityResourceItcRepositoryController;
import mn.gov.crc.nis.data.controller.ResourceInformation;
import org.springframework.data.mapping.PersistentEntity;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/05 12:18 PM
 */
@Log
public class LinkBuilder {

    public static Link getResourcesSelfLink() {
        return new Link(ServletUriComponentsBuilder.fromCurrentRequest().build().toString(), "self");
    }

    public static Link getResourceSelfLink(Class domainType, EntityMapper entityMapper, Object content) {
        try {
            Method method = ReflectionUtils.findMethod(EntityResourceItcRepositoryController.class, "findOne", ResourceInformation.class);
            PersistentEntity persistentEntity = entityMapper.getPersistentEntity(content.getClass());
            Object id = persistentEntity.getIdProperty().getGetter().invoke(content);
            return ControllerLinkBuilder.linkTo(method, entityMapper.getRepositoryName(domainType), id).withRel("self");
        } catch (Exception e) {
            log.warning(e.getMessage());
            return null;
        }
    }

    public static List<Link> getEmptyLink() {
        return new ArrayList<>();
    }
}
