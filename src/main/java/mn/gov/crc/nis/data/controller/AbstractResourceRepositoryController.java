package mn.gov.crc.nis.data.controller;

import mn.gov.crc.nis.data.ResourceNotFoundException;
import mn.gov.crc.nis.data.resource.EntityResourceAssembler;
import mn.gov.crc.nis.data.resource.ResourcesAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Resources;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/04 3:22 PM
 */
public class AbstractResourceRepositoryController {

    @Autowired
    EntityResourceAssembler resourceAssembler;

    @Autowired
    ResourcesAssembler resourcesAssembler;

    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;

    void verifyResourceInformation(ResourceInformation resourceInformation) {
        if (resourceInformation.getDomainType() == null || resourceInformation.getEntity() == null)
            throw new ResourceNotFoundException();
    }

    Resources toResources(Class domainType, Iterable contents, Pageable pageable) {
        if (contents instanceof Page) {
            Page page = (Page) contents;
            if (page.hasContent())
                return pagedResourcesAssembler.toResource(page, resourceAssembler);
            else
                return pagedResourcesAssembler.toEmptyResource(page, domainType, null);
        }

        if (Optional.ofNullable(contents).orElse(new ArrayList()).iterator().hasNext())
            if (contents.iterator().next().getClass().isAnnotationPresent(Entity.class))
                return resourcesAssembler.toResource(contents, resourceAssembler);
            else
                return resourcesAssembler.toObjectResource(contents, resourceAssembler);

        return toEmptyResource(domainType);
    }

    Resources toEmptyResource(Class domainType) {
        return resourcesAssembler.toEmptyResource(domainType, null);
    }

}
