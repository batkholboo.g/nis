package mn.gov.crc.nis.data.resource;

import mn.gov.crc.nis.data.EntityMapper;
import mn.gov.crc.nis.data.util.LinkBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/06 3:41 PM
 */
@Component
public class EntityResourceAssembler implements ResourceAssembler<Object, Resource> {

    @Autowired
    EntityMapper entityMapper;

    @Override
    public Resource toResource(Object entity) {
        Link resourceSelfLink = LinkBuilder.getResourceSelfLink(entity.getClass(), entityMapper, entity);
        return new Resource(entity, resourceSelfLink);
    }
}
