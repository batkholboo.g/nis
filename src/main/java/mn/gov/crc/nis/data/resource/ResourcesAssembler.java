package mn.gov.crc.nis.data.resource;

import mn.gov.crc.nis.data.util.LinkBuilder;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.core.EmbeddedWrapper;
import org.springframework.hateoas.core.EmbeddedWrappers;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/06 4:07 PM
 */
@Component
public class ResourcesAssembler implements ResourceAssembler<Iterable, Resources> {

    private final EmbeddedWrappers wrappers = new EmbeddedWrappers(false);

    @Override
    public Resources toResource(Iterable contents) {
        return null;
    }

    public Resources toEmptyResource(Class type, Link link) {
        EmbeddedWrapper wrapper = wrappers.emptyCollectionOf(type);
        List<EmbeddedWrapper> embedded = Collections.singletonList(wrapper);
        return new Resources(embedded, LinkBuilder.getResourcesSelfLink());
    }

    public Resources toResource(Iterable contents, EntityResourceAssembler resourceAssembler) {

        List<Resource> contentResource = new ArrayList();
        contents.forEach(o -> contentResource.add(resourceAssembler.toResource(o)));
        return new Resources(contentResource, LinkBuilder.getResourcesSelfLink());
    }

    public Resources toObjectResource(Iterable contents, EntityResourceAssembler resourceAssembler) {
        ObjectEmbeddedWrapper wrapper = new ObjectEmbeddedWrapper(contents);
        List<EmbeddedWrapper> embedded = Collections.singletonList(wrapper);
        return new Resources(embedded, LinkBuilder.getResourcesSelfLink());
    }
}
