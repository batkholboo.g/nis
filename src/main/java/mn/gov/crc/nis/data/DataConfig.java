package mn.gov.crc.nis.data;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/04 5:44 PM
 */
@ConfigurationProperties("nis.data")
@Data
@Component
public class DataConfig {

    private String basePath = "";
}
