package mn.gov.crc.nis.data.compress;

import org.apache.catalina.ssi.ByteArrayServletOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * @since 10/6/18 2:31 PM
 */

public class HttpServletResponseBufferWrapper extends HttpServletResponseWrapper {

    ByteArrayServletOutputStream baos = new ByteArrayServletOutputStream();

    /**
     * Constructs a response adaptor wrapping the given response.
     *
     * @param response The response to be wrapped
     * @throws IllegalArgumentException if the response is null
     */
    public HttpServletResponseBufferWrapper(HttpServletResponse response) {
        super(response);
    }


    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        return baos;
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        return new PrintWriter(baos);
    }

    public byte[] getByteArray() {
        return baos.toByteArray();
    }
}