package mn.gov.crc.nis.data.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import mn.gov.crc.nis.data.EntityMapper;
import mn.gov.crc.nis.data.component.FilterModelResolver;
import mn.gov.crc.nis.data.filter.FilterModel;
import mn.gov.crc.nis.data.repository.TdrsRepository;
import mn.gov.crc.nis.data.util.UriUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.data.mapping.PersistentEntity;
import org.springframework.data.mapping.context.PersistentEntities;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.support.Repositories;
import org.springframework.data.repository.support.RepositoryInvoker;
import org.springframework.data.repository.support.RepositoryInvokerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ResourceInformationHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

    private static final String PARAM_WHERE = "where";
    private static final String PARAM_PROPERTIES = "properties";

    private final Repositories repositories;
    private final RepositoryInvokerFactory invokerFactory;
    private final PersistentEntities persistentEntities;
    private final EntityMapper entityMapper;
    private final FilterModelResolver filterModelResolver;
    private final ObjectMapper objectMapper;

    @Autowired
    public ResourceInformationHandlerMethodArgumentResolver(Repositories repositories, RepositoryInvokerFactory invokerFactory, PersistentEntities persistentEntities, EntityMapper entityMapper, FilterModelResolver filterModelResolver, ObjectMapper objectMapper) {
        this.repositories = repositories;
        this.invokerFactory = invokerFactory;
        this.persistentEntities = persistentEntities;
        this.entityMapper = entityMapper;
        this.filterModelResolver = filterModelResolver;
        this.objectMapper = objectMapper;
    }

    /**
     * @param parameter
     * @see HandlerMethodArgumentResolver#supportsParameter(MethodParameter)
     */
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(ResourceInformation.class);
    }

    /**
     * @param parameter
     * @param mavContainer
     * @param webRequest
     * @param binderFactory
     */
    @Override
    public ResourceInformation resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String repository = UriUtils.getPathVariable("repository");
        String id = UriUtils.getPathVariable("id");
        String findMethodName = UriUtils.getPathVariable("findMethod");
        PersistentEntity<?, ?> persistentEntity = entityMapper.getPersistentEntity(repository);
        Class<?> domainType = Optional.ofNullable(persistentEntity).map(PersistentEntity::getType).orElse(null);

        TdrsRepository tdrsRepository = getItcRepository(domainType);
        Method findMethod = getFindMethod(tdrsRepository, findMethodName);

        byte[] byteContent = null;
        HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
        if (!servletRequest.getMethod().toLowerCase().equals("get"))
            byteContent = IOUtils.toByteArray(servletRequest.getInputStream());

        ResourceInformation resourceInformation = new ResourceInformation(
                domainType,
                persistentEntity,
                getRepositoryInvoker(domainType),
                tdrsRepository,
                getProperties(webRequest),
                getFilterModels(webRequest, persistentEntity),
                getResultType(webRequest),
                getWhereParameter(webRequest),
                getPath(webRequest),
                getParameters(webRequest),
                id,
                findMethodName != null ? true : false,
                findMethod,
                getContent(domainType, byteContent),
                getJsonContent(byteContent),
                getDomainRoles(domainType)
        );

        return resourceInformation;
    }

    private List<String> getDomainRoles(Class<?> domainType) {
        ArrayList<String> roles = new ArrayList<>();
        if (domainType.isAnnotationPresent(Secured.class)) {
            Secured annotation = domainType.getAnnotation(Secured.class);
            for (String role : annotation.value()) {
                roles.add(role);
            }
        }
        return roles;
    }

    private JsonNode getJsonContent(byte[] byteContent) throws IOException {
        if (byteContent != null && byteContent.length > 0)
            return objectMapper.readTree(byteContent);
        return null;
    }

    private Object getContent(Class<?> domainType, byte[] byteContent) throws IOException {
        if (byteContent != null && byteContent.length > 0)
            return objectMapper.readValue(byteContent, domainType);
        return null;
    }

    private Method getFindMethod(TdrsRepository tdrsRepository, String findMethodName) {
        if (findMethodName != null) {
            for (Class<?> repoInterface : tdrsRepository.getClass().getInterfaces()) {
                if (Repository.class.isAssignableFrom(repoInterface)) {
                    for (Method method : repoInterface.getDeclaredMethods()) {
                        if (method.getName().toLowerCase().equals(findMethodName.toLowerCase()))
                            return method;
                    }
                }
            }
        }
        return null;
    }

    private ResourceInformation.ResultType getResultType(NativeWebRequest webRequest) {
        ResourceInformation.ResultType resultType = ResourceInformation.ResultType.PAGE;
        String paramResultType = webRequest.getParameter("resultType");
        if (paramResultType != null) {
            resultType = ResourceInformation.ResultType.valueOf(paramResultType.toUpperCase());
        }
        return resultType;
    }

    private String getPath(NativeWebRequest webRequest) {
        return webRequest.getContextPath();
    }

    private List<FilterModel> getFilterModels(NativeWebRequest webRequest, PersistentEntity<?, ?> persistentEntity) {
        if (persistentEntity != null) {
            return filterModelResolver.resolve(webRequest.getParameterValues("filter"), persistentEntity);
        }
        return null;
    }

    private List<String> getProperties(NativeWebRequest webRequest) {
        String[] paramProperties = Optional
                .ofNullable(webRequest.getParameterValues(PARAM_PROPERTIES))
                .orElse(new String[]{});
        ArrayList<String> properties = new ArrayList<>();
        for (String paramProperty : paramProperties) {
            String[] rawPropertyNames = paramProperty.split(",");
            for (String rawPropertyName : rawPropertyNames) {
                String propertyName = rawPropertyName.replaceAll(" ", "");
                properties.add(propertyName);
            }
        }
        return properties;
    }

    private TdrsRepository getItcRepository(Class<?> domainType) {
        if (domainType != null) {
            Object repositoryFor = repositories.getRepositoryFor(domainType);
            if (repositoryFor != null && repositoryFor instanceof TdrsRepository)
                return (TdrsRepository) repositoryFor;
        }
        return null;
    }

    private RepositoryInvoker getRepositoryInvoker(Class<?> domainType) {
        if (domainType != null)
            return invokerFactory.getInvokerFor(domainType);
        return null;
    }

    private String getWhereParameter(NativeWebRequest webRequest) {
        return webRequest.getParameter(PARAM_WHERE);
    }

    private MultiValueMap<String, String> getParameters(NativeWebRequest webRequest) {
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap();
        Map<String, String[]> parameterMap = webRequest.getParameterMap();
        for (String paramName : parameterMap.keySet()) {
            parameters.put(paramName, Arrays.stream(parameterMap.get(paramName)).collect(Collectors.toList()));
        }
        return parameters;
    }
}