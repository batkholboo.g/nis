package mn.gov.crc.nis.data.controller;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import mn.gov.crc.nis.data.filter.FilterModel;
import mn.gov.crc.nis.data.repository.TdrsRepository;
import org.springframework.data.mapping.PersistentEntity;
import org.springframework.data.repository.support.RepositoryInvoker;
import org.springframework.util.MultiValueMap;

import java.lang.reflect.Method;
import java.util.List;

@AllArgsConstructor
public class ResourceInformation {

    public static enum ResultType {
        LIST,
        PAGE
    }

    @Getter
    private Class domainType;
    @Getter
    private PersistentEntity<?, ?> entity;
    @Getter
    private RepositoryInvoker repositoryInvoker;
    @Getter
    private TdrsRepository repository;
    @Getter
    private List<String> properties;
    @Getter
    private List<FilterModel> filters;
    @Getter
    private ResultType resultType;
    @Getter
    private String where;
    @Getter
    private String path;
    @Getter
    private MultiValueMap<String, String> parameters;
    @Getter
    private String id;
    @Getter
    private boolean search;
    @Getter
    private Method findMethod;
    @Getter
    private Object content;
    @Getter
    private JsonNode jsonContent;
    @Getter
    private List<String> domainRoles;
}