package mn.gov.crc.nis.data.filter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Date;

public enum Operator {
    EQ("=", Boolean.class, Number.class, String.class, Character.class, Date.class),
    NE("!=", Boolean.class, Number.class, String.class, Character.class, Date.class),
    GE(">=", Number.class, Date.class),
    LE("<=", Number.class, Date.class),
    GT(">", Number.class, Date.class),
    LT("<", Number.class, Date.class),
    LIKE("LIKE", String.class),
    NOT_LIKE("NOT LIKE", String.class),
    IN("IN"),
    NOT_IN("NOT IN"),
    BETWEEN("BETWEEN");

    private String value;
    private Class[] types;

    Operator(String value, Class... types) {
        this.value = value;
        this.types = types;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    public Class[] getTypes() {
        return types;
    }

    @JsonCreator()
    public static Operator getFromString(String value) {
        Operator operator = null;
        for (Operator o : values()) {
            if (o.getValue().equals(value.toUpperCase())) {
                operator = o;
                break;
            }
        }

        if (operator == null)
            operator = Operator.valueOf(value.toUpperCase());

        if (operator == null) {
            throw new NullPointerException("not valid operator!!!");
        }
        return operator;
    }

    public boolean verify(Class<?> type) {
        if (types == null || types.length == 0) return true;
        for (Class suggestedType : types) {
            if (suggestedType.isAssignableFrom(type)) return true;
        }
        throw new IllegalArgumentException(type.getName() + " class is not allowed by " + this + " operator!!!");
    }
}
