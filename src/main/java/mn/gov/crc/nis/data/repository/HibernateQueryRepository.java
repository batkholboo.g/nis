package mn.gov.crc.nis.data.repository;

import mn.gov.crc.nis.data.filter.FilterModel;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.*;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/06 4:49 PM
 */
@Repository
public class HibernateQueryRepository {
    @Autowired
    EntityManager entityManager;

    @Autowired
    SessionFactory sessionFactory;

    private Criteria createCriteria(Class domainClass, List<FilterModel> filters, String where, Sort sort) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(domainClass);

        if (where != null) {
            criteria.add(Restrictions.sqlRestriction(where));
        }

        for (FilterModel filter : Optional.ofNullable(filters).orElse(new ArrayList<>())) {
            criteria.add(convertCriterion(filter));
        }

        addOrder(criteria, sort);

        return criteria;
    }

    private Criterion convertCriterion(FilterModel filter) {
        switch (filter.getOperator()) {
            case EQ:
                return Restrictions.eq(filter.getName(), filter.getValue());
            case NE:
                return Restrictions.ne(filter.getName(), filter.getValue());
            case GE:
                return Restrictions.ge(filter.getName(), filter.getValue());
            case GT:
                return Restrictions.gt(filter.getName(), filter.getValue());
            case LE:
                return Restrictions.le(filter.getName(), filter.getValue());
            case LT:
                return Restrictions.lt(filter.getName(), filter.getValue());
            case LIKE:
                return Restrictions.like(filter.getName(), filter.getValue());
            case NOT_LIKE:
                return Restrictions.not(Restrictions.like(filter.getName(), filter.getValue()));
            case IN:
                return Restrictions.in(filter.getName(), (Collection) filter.getValue());
            case NOT_IN:
                return Restrictions.not(Restrictions.in(filter.getName(), (Collection) filter.getValue()));
            case BETWEEN:
                return Restrictions.between(filter.getName(), filter.getFrom(), filter.getTo());
        }
        throw new UnsupportedOperationException(filter.getOperator() + " not defined!!!");
    }

    private void addOrder(Criteria criteria, Sort sort) {
        if (sort != null)
            for (Sort.Order order : sort) {
                Order o;
                if (order.getDirection() == Sort.Direction.ASC) o = Order.asc(order.getProperty());
                else o = Order.desc(order.getProperty());
                criteria.addOrder(o);
            }
    }

    public Page find(Class domainClass, List<FilterModel> filters, String where, Pageable pageable, Sort sort) {
        Criteria criteria = createCriteria(domainClass, filters, where, sort);
        criteria.setMaxResults(pageable.getPageSize());
        criteria.setFirstResult(pageable.getOffset());
        List results = Optional.ofNullable(criteria.list()).orElse(new ArrayList());

        criteria = createCriteria(domainClass, filters, where, null);
        criteria.setProjection(Projections.rowCount());
        long total = (long) criteria.uniqueResult();

        return new PageImpl(results, pageable, total);
    }

    public Iterable find(Class domainClass, List<FilterModel> filters, String where, Sort sort) {
        Criteria criteria = createCriteria(domainClass, filters, where, sort);
        return criteria.list();
    }

    public Object findNative(String sqlQuery, Map<String, Object> parameter, Class returnType) {
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sqlQuery);
        for (String key : parameter.keySet()) {
            Object value = parameter.get(key);
            if (value == null) {
                query.setParameter(key, value);
            } else if (Collection.class.isAssignableFrom(value.getClass())) {
                query.setParameterList(key, (Collection) value);
            } else if (value.getClass().isArray()) {
                query.setParameterList(key, (Collection) value);
            } else {
                query.setParameter(key, value);
            }
        }

        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        if (Collection.class.isAssignableFrom(returnType)) {
            return query.list();
        } else {
            return query.uniqueResult();
        }
    }
}
