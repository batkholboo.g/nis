package mn.gov.crc.nis.data.resource;

import org.springframework.hateoas.core.EmbeddedWrapper;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/11 6:52 PM
 */
public class ObjectEmbeddedWrapper implements EmbeddedWrapper {

    Object value;

    public ObjectEmbeddedWrapper(Object value) {
        this.value = value;
    }

    @Override
    public String getRel() {
        return null;
    }

    @Override
    public boolean hasRel(String rel) {
        return false;
    }

    @Override
    public boolean isCollectionValue() {
        return true;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public Class<?> getRelTargetType() {
        return Object.class;
    }
}
