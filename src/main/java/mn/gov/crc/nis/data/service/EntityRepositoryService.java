package mn.gov.crc.nis.data.service;

import com.fasterxml.jackson.databind.JsonNode;
import mn.gov.crc.nis.data.EntityMapper;
import mn.gov.crc.nis.data.ResourceNotFoundException;
import mn.gov.crc.nis.data.controller.ResourceInformation;
import mn.gov.crc.nis.data.repository.HibernateQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mapping.PersistentEntity;
import org.springframework.data.mapping.PersistentProperty;
import org.springframework.data.repository.support.RepositoryInvoker;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.*;
import java.lang.reflect.Method;
import java.util.Iterator;

@Service
public class EntityRepositoryService {

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    HibernateQueryRepository hibernateQueryRepository;

    public Object find(ResourceInformation resourceInformation, Pageable pageable, Sort sort) {
        if (resourceInformation.isSearch() == false)
            return findByQuery(resourceInformation, pageable, sort);
        else
            return findFindMethod(resourceInformation, pageable, sort);
    }

    private Iterable findByQuery(ResourceInformation resourceInformation, Pageable pageable, Sort sort) {
        if (resourceInformation.getResultType() == ResourceInformation.ResultType.PAGE)
            return hibernateQueryRepository.find(
                    resourceInformation.getDomainType(),
                    resourceInformation.getFilters(),
                    resourceInformation.getWhere(),
                    pageable,
                    sort);
        else
            return hibernateQueryRepository.find(
                    resourceInformation.getDomainType(),
                    resourceInformation.getFilters(),
                    resourceInformation.getWhere(),
                    sort
            );
    }

    public Object findFindMethod(ResourceInformation resourceInformation, Pageable pageable, Sort sort) {
        Method findMethod = resourceInformation.getFindMethod();
        if (findMethod != null) {
            MultiValueMap<String, String> parameters = resourceInformation.getParameters();
            return resourceInformation.getRepositoryInvoker().invokeQueryMethod(findMethod, parameters, pageable, sort);
        }
        throw new ResourceNotFoundException(ServletUriComponentsBuilder.fromCurrentRequest().build().toString());
    }

    public Object findOne(ResourceInformation resourceInformation) {
        if(resourceInformation.getRepositoryInvoker().hasFindOneMethod()){
            return resourceInformation.getRepositoryInvoker().invokeFindOne(resourceInformation.getId());
        }
        throw new ResourceNotFoundException();
    }

    public Object create(ResourceInformation resourceInformation) {
        if (resourceInformation.getRepositoryInvoker().hasSaveMethod()) {
            return resourceInformation.getRepositoryInvoker().invokeSave(resourceInformation.getContent());
        }
        throw new ResourceNotFoundException();
    }

    public Object update(ResourceInformation resourceInformation) {
        if (resourceInformation.getRepositoryInvoker().hasSaveMethod()) {
            Object entity = resourceInformation.getRepositoryInvoker().invokeFindOne(resourceInformation.getId());
            copy(entity, resourceInformation.getContent(), resourceInformation.getJsonContent(), resourceInformation.getEntity());
            return resourceInformation.getRepositoryInvoker().invokeSave(entity);
        }
        throw new ResourceNotFoundException();
    }

    public void delete(ResourceInformation resourceInformation) {
        RepositoryInvoker repositoryInvoker = resourceInformation.getRepositoryInvoker();
        if (repositoryInvoker.hasDeleteMethod()) {
            repositoryInvoker.invokeDelete(resourceInformation.getId());
            return;
        }
        throw new ResourceNotFoundException();
    }

    private void copy(Object dest, Object src, JsonNode srcJson, PersistentEntity persistentEntity) {
        Iterator<String> fieldNames = srcJson.fieldNames();
        while (fieldNames.hasNext()) {
            try {
                String fieldName = fieldNames.next();
                PersistentProperty property = persistentEntity.getPersistentProperty(fieldName);
                if (property != null && property.isWritable()) {
                    if (property.isAnnotationPresent(Column.class)) {
                        property.getSetter().invoke(dest, property.getGetter().invoke(src));
                    } else if (property.isAnnotationPresent(ManyToOne.class) || property.isAnnotationPresent(OneToOne.class)) {
                        Object newDest = property.getGetter().invoke(dest);
                        if (newDest == null) {
                            newDest = property.getActualType().newInstance();
                            property.getSetter().invoke(dest, newDest);
                        }
                        copy(newDest, property.getGetter().invoke(src), srcJson.get(fieldName), entityMapper.getPersistentEntity(property.getActualType()));
                    } else if (property.isAnnotationPresent(OneToMany.class) || property.isAnnotationPresent(ManyToMany.class)) {
                        property.getSetter().invoke(dest, property.getGetter().invoke(src));
                    }
                }
            } catch (Exception e) {
            }
        }
    }
}