package mn.gov.crc.nis.data;

import mn.gov.crc.nis.data.compress.HttpServletResponseBufferWrapper;
import org.apache.commons.io.IOUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

/**
 * @author Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * @since 10/6/18 2:10 PM
 */
@Component
@Order(0)
public class CompressFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpServletResponse httpResponse = (HttpServletResponse) response;

            String compression = httpRequest.getHeader("compression");
            if (compression != null && compression.toLowerCase().equals("true")) {
                HttpServletResponseBufferWrapper bufferWrapper = new HttpServletResponseBufferWrapper(httpResponse);
                chain.doFilter(request, bufferWrapper);
                HttpServletResponse realResponse = (HttpServletResponse) bufferWrapper.getResponse();

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                GZIPOutputStream defOutputStream = new GZIPOutputStream(out);
                defOutputStream.write(bufferWrapper.getByteArray());
                defOutputStream.close();

                realResponse.setContentLength(out.size());
                realResponse.setContentType("application/octet-stream+gzip");

                IOUtils.write(out.toByteArray(), realResponse.getOutputStream());

                return;
            }
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
