package mn.gov.crc.nis.data.util;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/11 4:09 PM
 */
@Component
public class DateTimeUtils {

    @Value("${trds.date-formats:yyyy-MM-dd HH:mm:ss,yyyy-MM-dd HH:mm,yyyy-MM-dd HH,yyyy-MM-dd}")
    private String[] dateFormats;

    public DateTimeUtils() {
    }

    public Date parseDate(String strDate) {
        try {
            return DateUtils.parseDate(strDate, dateFormats);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
