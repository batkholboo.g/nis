package mn.gov.crc.nis.data;

import mn.gov.crc.nis.data.controller.AbstractResourceRepositoryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.WebMvcRegistrationsAdapter;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/04 5:34 PM
 */
public class DataMvcRegistrationConfiguration extends WebMvcRegistrationsAdapter {

    @Autowired
    DataConfig dataConfig;

    @Override
    public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
        return new RequestMappingHandlerMapping() {
            @Override
            protected void registerHandlerMethod(Object handler, Method method, RequestMappingInfo mapping) {
                if (AbstractResourceRepositoryController.class.isAssignableFrom(method.getDeclaringClass())) {
                    PatternsRequestCondition apiPattern = new PatternsRequestCondition(dataConfig.getBasePath())
                            .combine(mapping.getPatternsCondition());
                    mapping = new RequestMappingInfo(
                            mapping.getName(), apiPattern,
                            mapping.getMethodsCondition(), mapping.getParamsCondition(),
                            mapping.getHeadersCondition(), mapping.getConsumesCondition(),
                            mapping.getProducesCondition(), mapping.getCustomCondition()
                    );
                }
                super.registerHandlerMethod(handler, method, mapping);
            }
        };
    }
}
