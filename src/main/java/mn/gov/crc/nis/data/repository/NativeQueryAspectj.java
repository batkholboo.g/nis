package mn.gov.crc.nis.data.repository;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/12 7:40 PM
 */
@Aspect
public class NativeQueryAspectj {

    @Autowired
    HibernateQueryRepository hibernateQueryRepository;

    @Around("within(mn.gov.crc.nis.data.repository.TdrsRepository+) && execution(* *.*(..))")
    public Object queryProcess(ProceedingJoinPoint pjp) throws Throwable {
        if (pjp.getSignature() instanceof MethodSignature) {
            MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
            Method method = methodSignature.getMethod();
            if (method.isAnnotationPresent(Query.class)) {
                Query query = method.getAnnotation(Query.class);
                if (query.nativeQuery() == true) {
                    Class returnType = method.getReturnType();
                    Class actualType = returnType;

                    if (Collection.class.isAssignableFrom(actualType)) {
                        ParameterizedType type = (ParameterizedType) method.getGenericReturnType();
                        actualType = (Class) type.getActualTypeArguments()[0];
                    }

                    if (Map.class.isAssignableFrom(actualType)) {

                        Map<String, Object> parameter = new HashMap<>();
                        for (int i = 0; i < method.getParameterAnnotations().length; i++) {
                            Annotation[] annotations = method.getParameterAnnotations()[i];
                            for (Annotation annotation : annotations) {
                                if (annotation instanceof Param) {
                                    parameter.put(((Param) annotation).value(), pjp.getArgs()[i]);
                                    break;
                                }
                            }
                        }

                        return hibernateQueryRepository.findNative(query.value(), parameter, returnType);
                    }

                    /*ParameterizedType type = (ParameterizedType) method.getGenericReturnType();

                    Class actualType = (Class) type.getActualTypeArguments()[0];*/



                    /*System.out.println(type);
                    System.out.println("type = " + type);*/
//                    System.out.println("OK");
                    return null;
                }
            }
        }
        return pjp.proceed();
    }

}
