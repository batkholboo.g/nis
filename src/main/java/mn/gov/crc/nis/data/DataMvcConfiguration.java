package mn.gov.crc.nis.data;

import mn.gov.crc.nis.data.repository.NativeQueryAspectj;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.data.mapping.context.PersistentEntities;
import org.springframework.data.repository.support.DefaultRepositoryInvokerFactory;
import org.springframework.data.repository.support.Repositories;
import org.springframework.data.repository.support.RepositoryInvokerFactory;
import org.springframework.orm.hibernate5.support.OpenSessionInViewFilter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/01 7:03 PM
 */
@EnableTransactionManagement
@ComponentScan
public class DataMvcConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    ApplicationContext ctx;

    @Bean
    public SessionFactory sessionFactory(HibernateEntityManagerFactory factory) {
        return factory.getSessionFactory();
    }

    @Bean
    public Repositories repositories(ApplicationContext applicationContext) {
        return new Repositories(applicationContext);
    }

    @Bean
    public RepositoryInvokerFactory repositoryInvokerFactory(Repositories repositories) {
        return new DefaultRepositoryInvokerFactory(repositories);
    }

    @Bean
    public NativeQueryAspectj nativeQueryAspectj(){
        return new NativeQueryAspectj();
    }

    @Bean
    public PersistentEntities persistentEntities(ApplicationContext applicationContext) {

        List<MappingContext<?, ?>> arrayList = new ArrayList<MappingContext<?, ?>>();

        for (MappingContext<?, ?> context : BeanFactoryUtils
                .beansOfTypeIncludingAncestors(applicationContext, MappingContext.class).values()) {
            arrayList.add(context);
        }

        return new PersistentEntities(arrayList);
    }

    @Bean
    public FilterRegistrationBean registerOpenSessionInViewFilterBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new OpenSessionInViewFilter());
        registrationBean.setOrder(5);
        return registrationBean;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        Map<String, HandlerMethodArgumentResolver> beansOfType = ctx.getBeansOfType(HandlerMethodArgumentResolver.class);
        for (HandlerMethodArgumentResolver handlerMethodArgumentResolver : beansOfType.values()) {
            if (!argumentResolvers.contains(handlerMethodArgumentResolver)) {
                argumentResolvers.add(handlerMethodArgumentResolver);
            }
        }
    }


}
