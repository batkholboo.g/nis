package mn.gov.crc.nis.data.controller;

import mn.gov.crc.nis.data.resource.EntityResourceAssembler;
import mn.gov.crc.nis.data.service.EntityRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/{repository}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class EntityResourceItcRepositoryController extends AbstractResourceRepositoryController {

    @Autowired
    EntityRepositoryService entityRepositoryService;

    @Autowired
    EntityResourceAssembler resourceAssembler;

    @GetMapping
    @ResponseBody
    public Resources find(ResourceInformation resourceInformation, Pageable pageable, Sort sort) {
        checkRoles(resourceInformation);
        verifyResourceInformation(resourceInformation);
        Iterable result = (Iterable) entityRepositoryService.find(resourceInformation, pageable, sort);
        return toResources(resourceInformation.getDomainType(), result, pageable);
    }

    @GetMapping("{id}")
    @ResponseBody
    public ResourceSupport findOne(ResourceInformation resourceInformation) {
        checkRoles(resourceInformation);
        verifyResourceInformation(resourceInformation);
        Object result = entityRepositoryService.findOne(resourceInformation);
        return resourceAssembler.toResource(result);
    }

    @GetMapping("search/{findMethod}")
    @ResponseBody
    public ResourceSupport findMethod(ResourceInformation resourceInformation, Pageable pageable, Sort sort) {
        verifyResourceInformation(resourceInformation);
        Object result = entityRepositoryService.find(resourceInformation, pageable, sort);
        if (result instanceof Iterable) {
            return toResources(resourceInformation.getDomainType(), (Iterable) result, pageable);
        } else if (result != null) {
            return resourceAssembler.toResource(result);
        } else {
            return toEmptyResource(resourceInformation.getDomainType());
        }
    }

    @PostMapping()
    @ResponseBody
    @Transactional
    public ResourceSupport create(ResourceInformation resourceInformation) {
        checkRoles(resourceInformation);
        verifyResourceInformation(resourceInformation);
        Object result = entityRepositoryService.create(resourceInformation);
        return resourceAssembler.toResource(result);
    }

    @PutMapping("{id}")
    @ResponseBody
    @Transactional
    public ResourceSupport update(ResourceInformation resourceInformation) {
        checkRoles(resourceInformation);
        verifyResourceInformation(resourceInformation);
        Object result = entityRepositoryService.update(resourceInformation);
        return resourceAssembler.toResource(result);
    }

    @DeleteMapping("{id}")
    @ResponseBody
    @Transactional
    public void delete(ResourceInformation resourceInformation) {
        checkRoles(resourceInformation);
        verifyResourceInformation(resourceInformation);
        entityRepositoryService.delete(resourceInformation);
    }

    private void checkRoles(ResourceInformation resourceInformation) {
        if (resourceInformation.getDomainRoles().size() > 0) {
            Set<String> userRoles = SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
            for (String domainRole : resourceInformation.getDomainRoles()) {
                if (userRoles.contains(domainRole)) return;
            }

            throw new AuthorizationServiceException("Role not match");
        }
    }
}