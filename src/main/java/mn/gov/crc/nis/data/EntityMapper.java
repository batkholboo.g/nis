package mn.gov.crc.nis.data;

import org.atteo.evo.inflector.English;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mapping.PersistentEntity;
import org.springframework.data.mapping.context.PersistentEntities;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/12/04 2:19 PM
 */
@Component
public class EntityMapper {

    @Autowired
    PersistentEntities persistentEntities;

    private Map<String, PersistentEntity<?, ?>> entityMap = new HashMap<>();

    @PostConstruct
    void init() {
        for (PersistentEntity<?, ?> persistentEntity : persistentEntities) {
            String repositoryName = English.plural(persistentEntity.getType().getSimpleName()).toLowerCase();
            entityMap.put(repositoryName, persistentEntity);
        }
    }

    public Class<?> getDomainType(String repositoryName) {
        repositoryName = getRepositoryName(repositoryName);
        return entityMap.containsKey(repositoryName) ? entityMap.get(repositoryName).getType() : null;
    }

    public PersistentEntity<?, ?> getPersistentEntity(String repositoryName) {
        repositoryName = getRepositoryName(repositoryName);
        return entityMap.get(repositoryName);
    }

    private String getRepositoryName(String repositoryName) {
        return repositoryName.toLowerCase();
    }

    public String getRepositoryName(Class<?> domainType) {
        for (String repositoryName : entityMap.keySet()) {
            if (entityMap.get(repositoryName).getType().equals(domainType)) {
                return repositoryName;
            }
        }
        return null;
    }

    public PersistentEntity getPersistentEntity(Class<?> domainClass) {
        return getPersistentEntity(getRepositoryName(domainClass));
    }
}
