package mn.gov.crc.nis.admin.model;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class SysGroupModel {
    private Long id;
    private String name;
    private Long status;
    private Long createdBy;
    private Date createdDate;
    private Long updatedBy;
    private Date updatedDate;
    private Long branchId;
    private Long branchBy;

    private List<Long> roles;
}
