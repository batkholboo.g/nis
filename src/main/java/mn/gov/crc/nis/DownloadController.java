package mn.gov.crc.nis;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2018/01/01 7:47 PM
 */
@Controller
@RequestMapping("/download")
public class DownloadController {
    @Value("${nis.file.path}")
    String filePath;

    @GetMapping("{fileName:.+}")
    @Secured("ROLE_TEST")
    public void donwload(@PathVariable("fileName") String fileName, HttpServletResponse response) throws IOException {
        File file = new File(filePath + File.separator + fileName);
        response.setHeader("Content-Length", file.length() + "");
        IOUtils.copy(new FileInputStream(file), response.getOutputStream());
    }
}
