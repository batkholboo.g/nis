package mn.gov.crc.nis.dto;

import lombok.Data;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 1/16/18 7:25 PM
 */
@Data
public class DashboardResultDTO {
    private long total;
    private long planned;

    public DashboardResultDTO() {
    }

    public DashboardResultDTO(long total, long planned) {
        this.total = total;
        this.planned = planned;
    }
}
