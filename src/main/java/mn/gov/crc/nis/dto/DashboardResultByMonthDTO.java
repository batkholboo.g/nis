package mn.gov.crc.nis.dto;

import lombok.Data;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 1/16/18 7:25 PM
 */
@Data
public class DashboardResultByMonthDTO extends DashboardResultDTO{
    private long month;

    public DashboardResultByMonthDTO() {

    }

    public DashboardResultByMonthDTO(long total, long planned, long month) {
        super(total, planned);
        this.month = month;
    }
}
