package mn.gov.crc.nis.controller;

import mn.gov.crc.nis.service.SqlService;
import mn.gov.crc.nis.util.FuncUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sql")
@Secured("ROLE_TEST")

public class SqlController {

    @Autowired
    SqlService sqlService;

    @GetMapping(value = "/reload")
    public ResponseEntity reload() {
        return sqlService.reload();
    }

    @PostMapping(value = "/{code:.+}")
    public ResponseEntity save(@PathVariable("code") String code, @RequestBody Map<String, Object> params) {
        //System.out.println("code.post.save " + code);
        return sqlService.save(code, params);
    }
    @RequestMapping(value = "/{code}/execute", method = RequestMethod.POST)
    @Secured("ROLE_INSERT")
    public ResponseEntity execute(@PathVariable("code") String code, @RequestBody Map<String, Object> params) {
        //System.out.println("code.execute " + code);
        return sqlService.execute(code, params);
    }

    @RequestMapping(value = "/{code:.+}/list", method = RequestMethod.POST)
    @Secured("ROLE_INSERT")
    public ResponseEntity save(@PathVariable("code") String code, @RequestBody List<Map<String, Object>> list) {
        //System.out.println("code.patch " + code);
        return sqlService.patch(code, list);
    }
    @RequestMapping(value = "/{code}/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_DELETE")
    public ResponseEntity delete(@PathVariable("code") String code, @PathVariable("id") Long id) {
        //System.out.println("code.delete " + code + ", " + id);
        return sqlService.delete(code, id);
    }

    @RequestMapping(value = "/{code}/{action}", method = RequestMethod.GET)
    @Secured("ROLE_GET")
    public ResponseEntity modelAction(@PathVariable("code") String code, @PathVariable("action") String action, WebRequest request) {
        //System.out.println("request.get " + code + "parameter :" + request.getParameterMap().toString());
        switch (action) {
            case "list":
                Map<String, Object> params = FuncUtils.requestToMap(request.getParameterMap());
                params.put("activeFlag", 1);
                return sqlService.list(code, params);
            case "listAll":
                return sqlService.list(code, FuncUtils.requestToMap(request.getParameterMap()));
            case "total":
                params = FuncUtils.requestToMap(request.getParameterMap());
                params.put("activeFlag", 1);
                return sqlService.total(code, params);
            case "totalAll":
                return sqlService.total(code, FuncUtils.requestToMap(request.getParameterMap()));
            case "get":
                params = FuncUtils.requestToMap(request.getParameterMap());
                params.put("activeFlag", 1);
                return sqlService.get(code, params);
            case "getAll":
                return sqlService.get(code, FuncUtils.requestToMap(request.getParameterMap()));
            case "schema":
                return ResponseEntity.ok(sqlService.getModel(code));
            default:
                try {
                    params = new HashMap<>();
                    params.put("id", Long.parseLong(action));
                    return sqlService.getPk(code, params);
                } catch (Exception e) {
                    params = new HashMap<>();
                    params.put("id", action);
                    return sqlService.getPk(code, params);
                }
        }
    }

    @GetMapping(value = "/generate/{tableName}")
    public ResponseEntity generate(@PathVariable("tableName") String tableName) {
        return sqlService.generate(tableName);
    }
    @GetMapping(value = "/generate/{schema}/{tableName}")
    public ResponseEntity generate(@PathVariable("schema") String schema, @PathVariable("tableName") String tableName) {
        //System.out.println("schema " + schema + ", " + tableName);
        return sqlService.generate(schema, tableName);
    }
}
