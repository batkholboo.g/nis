package mn.gov.crc.nis.dao;

import mn.gov.crc.nis.data.repository.TdrsCrudRepository;
import mn.gov.crc.nis.entity.RefTechnology;
import org.springframework.stereotype.Repository;

/**
 * Created by Bold-Erdene on 27/11/2018.
 */

@Repository()
public interface refTechnologyDAO extends TdrsCrudRepository<RefTechnology, Long> {
}