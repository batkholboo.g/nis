package mn.gov.crc.nis.dashboard;//package mn.transdep.tdrs.dashboard;
//
//import mn.gov.crc.nis.dao.InsInspresultDAO;
//import mn.gov.crc.nis.dao.RegVehicleDAO;
//import mn.gov.crc.nis.dao.SysPlanDAO;
//import mn.gov.crc.nis.dao.SysPlanMonthDAO;
//import mn.gov.crc.nis.dto.DashboardResultByMonthDTO;
//import mn.gov.crc.nis.dto.DashboardResultDTO;
//import mn.gov.crc.nis.entity.InsInspresult;
//import mn.gov.crc.nis.entity.SysPlanMonth;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//import java.util.function.ToLongFunction;
//import java.util.stream.Collectors;
//
///**
// * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
// * Date: 1/16/18 7:43 PM
// */
//
//@Service
//public class DashboardService {
//
//    private final static SimpleDateFormat DF = new SimpleDateFormat("yyyy/MM/dd");
//
//    @Autowired
//    InsInspresultDAO insInspresultDAO;
//
//    @Autowired
//    RegVehicleDAO vehicleDAO;
//
//    @Autowired
//    SysPlanMonthDAO sysPlanMonthDAO;
//
//    @Autowired
//    SysPlanDAO sysPlanDAO;
//
//    public DashboardResultDTO getDashboardResultTotal() {
//        Date fromDate = getFromDate();
//        return new DashboardResultDTO(
//                insInspresultDAO.countByStatusAndPassedAndTestDateBetween(1l, 1l, fromDate, new Date()),
//                vehicleDAO.countByStatus(1l)
//
//        );
//    }
//
//    public DashboardResultDTO getDashboardResultByVehicleType(String vtype) {
//        long planned = 0;
//
//        Long[][] results = sysPlanDAO.sumCountsByYear(Calendar.getInstance().get(Calendar.YEAR));
//
//        switch (vtype) {
//            case "new_car":
//                planned = results[0][0];
//                break;
//            case "car":
//                planned = results[0][1];
//                break;
//            case "lorry":
//                planned = results[0][2];
//                break;
//            case "bus":
//                planned = results[0][3];
//                break;
//            case "special":
//                planned = results[0][4];
//                break;
//            case "machine":
//                planned = results[0][5];
//                break;
//            case "trailer":
//                planned = results[0][6];
//                break;
//            case "motorcycle":
//                planned = results[0][7];
//                break;
//        }
//
//        return new DashboardResultDTO(0, planned);
//    }
//
//
//    public Date getFromDate() {
//        try {
//            Date parse = DF.parse(Calendar.getInstance().get(Calendar.YEAR) + "/01/01");
//            return parse;
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    public List<DashboardResultByMonthDTO> getDashboardResultByMonth() {
//        ArrayList<DashboardResultByMonthDTO> results = new ArrayList<>();
//
//        List<SysPlanMonth> sysPlanMonths = this.sysPlanMonthDAO.sumByPlanYearOrder(Calendar.getInstance().get(Calendar.YEAR));
//        for (SysPlanMonth sysPlanMonth : sysPlanMonths) {
//            results.add(new DashboardResultByMonthDTO(
//                    0,
//                    sysPlanMonth.getCount(),
//                    sysPlanMonth.getPlanMonth()
//            ));
//        }
//
//        return results;
//    }
//}
