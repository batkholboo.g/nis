package mn.gov.crc.nis.dashboard;//package mn.transdep.tdrs.dashboard;
//
//import mn.gov.crc.nis.dto.DashboardResultByMonthDTO;
//import mn.gov.crc.nis.dto.DashboardResultDTO;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//
///**
// * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
// * Date: 1/16/18 7:18 PM
// */
//@RestController
//@RequestMapping("/dashboard")
//public class DashboardController {
//
//    @Autowired
//    DashboardService dashboardService;
//
//    @GetMapping("implementation/total")
//    public DashboardResultDTO getWorkImplementation() {
//        return dashboardService.getDashboardResultTotal();
//    }
//
//    @GetMapping("implementation/{vtype}")
//    public DashboardResultDTO getWorkImplementation(@PathVariable String vtype) {
//        return dashboardService.getDashboardResultByVehicleType(vtype);
//    }
//
//    @GetMapping("implementation/total/month")
//    public List<DashboardResultByMonthDTO> getWorkImplementations() {
//        return dashboardService.getDashboardResultByMonth();
//    }
//
//}
