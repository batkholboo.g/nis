package mn.gov.crc.nis.sql.impl;

import lombok.extern.log4j.Log4j;
import mn.gov.crc.nis.model.SqlMapper;
import mn.gov.crc.nis.model.SqlModel;
import mn.gov.crc.nis.sql.NamedParameterStatement;
import mn.gov.crc.nis.sql.SqlRepository;
import mn.gov.crc.nis.util.FuncUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

@Log4j
@Repository
public class SqlRepositoryImpl implements SqlRepository {
    private static final Map<String, SqlModel> modelMapper = new HashMap<>();
    private static final Map<Long, String> modelIdMapper = new HashMap<>();
    private static final Map<String, SqlMapper> mapperMapper = new HashMap<>();
    private static final Map<Long, String> mapperIdMapper = new HashMap<>();
    private static final Map<String, String> modulePortMapper = new HashMap<>();

    @Value("${spring.application.name}")
    private String currentModuleName;

    @Value("${spring.datasource.username}")
    private String currentSchema;
    //    @Value("${spring.db.modelTable}")
    private String modelTable = "SQL_MODEL";
    //    @Value("${spring.db.mapperTable}")
    private String mapperTable = "SQL_MAPPER";
    //    @Value("${spring.db.mapperFieldTable}")
    private String mapperFieldTable = "SQL_MAPPER_FIELD";
    //    @Value("${spring.db.mapperFieldRelationTable}")
    private String mapperFieldRelationTable = "SQL_MAPPER_FIELD_RELATION";

    //    @Qualifier("dataSource")
    @Autowired
    DataSource dataSource;
//    @Bean
//    public ServletRegistrationBean statViewServlet() {
//        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
////        servletRegistrationBean.addInitParameter("allow","*.*.*.*");  //Set the IP white list
////        servletRegistrationBean.addInitParameter("deny","192.168.0.19");//Set IP blacklist, higher priority than white list//Setting up the console to manage the user
//        servletRegistrationBean.addInitParameter("loginUsername", "root");
//        servletRegistrationBean.addInitParameter("loginPassword", "root");
//        //Can you reset the data
//        servletRegistrationBean.addInitParameter("resetEnable", "false");
//        return servletRegistrationBean;
//    }
//
//    @Bean
//    public FilterRegistrationBean statFilter() {
//        //Create a filter
//        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new WebStatFilter());
//        //Setting filter filter path
//        filterRegistrationBean.addUrlPatterns("/*");
//        //Ignore the form of filtering
//        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
//        return filterRegistrationBean;
//    }

    private SqlModel reloadId(Long id) {
        return null;
    }

    @Override
    public Map<String, SqlModel> reloadSQL() {
        mapperMapper.clear();
        mapperIdMapper.clear();
        modelMapper.clear();
        modelIdMapper.clear();
        Connection connection = null;
        List<Map<String, Object>> list = null;
        try {
            System.out.println("reloadSQl.start");
            connection = this.dataSource.getConnection();
            Statement statement = connection.createStatement();
            System.out.println("start Mapper");
            List<Map<String, Object>> mapperList = listSQL("SELECT * FROM " + mapperTable + " ORDER BY EXTEND_ID DESC", new HashMap<>(), connection, null, false);
            for (Map<String, Object> mapper : mapperList) {
                Long extendId = FuncUtils.getValueMap(mapper, "extendId", Long.class);
                SqlMapper extend = null;
                if (extendId != null) {

                    extend = getMapper(extendId);
                    System.out.println("is extend id " + extendId + ", " + extend);
                }
                SqlMapper sqlMapper = new SqlMapper();
                sqlMapper.setId(FuncUtils.getValueMap(mapper, "id", Long.class));
                sqlMapper.setCode(FuncUtils.getValueMap(mapper, "code", String.class));
                sqlMapper.setName(FuncUtils.getValueMap(mapper, "name", String.class));
                mapperMapper.put(sqlMapper.getCode(), sqlMapper);
                mapperIdMapper.put(sqlMapper.getId(), sqlMapper.getCode());
                if (extend != null) {
                    sqlMapper.fromExtend(extend);
                }
               // System.out.println("mapper " + sqlMapper);
                List<Map<String, Object>> mapperFieldList = listSQL("SELECT * FROM " + mapperFieldTable + " WHERE MAPPER_ID=:id ORDER BY COLUMN_NAME", mapper, connection, null, false);

                List<Map<String, Object>> mapperFieldRelationList = listSQL("SELECT * FROM " + mapperFieldRelationTable + " WHERE MAPPER_FIELD_ID IN ("
                        + "SELECT ID FROM " + mapperFieldTable + " WHERE MAPPER_ID=:id)", mapper, connection, null, false);
                Map<Long, List<Map<String, Object>>> fieldRelationMap = new HashMap<>();
                if (mapperFieldRelationList != null && mapperFieldRelationList.size() > 0) {
                    for (Map<String, Object> mapperFieldRelation : mapperFieldRelationList) {
                        Long mapperFieldId = FuncUtils.getValueMap(mapperFieldRelation, "mapperFieldId", Long.class);
                       // System.out.println("mapperFieldId : " + mapperFieldId);
                        List<Map<String, Object>> rlist;
                        if (!fieldRelationMap.containsKey(mapperFieldId)) {
                            rlist = new ArrayList<>();
                            //System.out.println("new rList " + rlist);
                        } else {
                            //System.out.println("map " + fieldRelationMap);
                            rlist = fieldRelationMap.get(mapperFieldId);
                            //System.out.println("get rList " + rlist);
                        }
                        rlist.add(mapperFieldRelation);
                        fieldRelationMap.put(mapperFieldId, rlist);
                    }
                }
                //System.out.println("fieldRelationMap " + fieldRelationMap);
                for (Map<String, Object> mapperField : mapperFieldList) {
                    Long mapperFieldId = FuncUtils.getValueMap(mapperField, "id", Long.class);
                    String field = FuncUtils.getValueMap(mapperField, "fieldName", String.class);
                    sqlMapper.getTypeMapper().put(field, FuncUtils.getValueMap(mapperField, "type", String.class));
                    sqlMapper.getFieldMapper().put(field, FuncUtils.getValueMap(mapperField, "columnName", String.class));
                    Map<String, Object> settings = new LinkedHashMap<>();
                    Boolean isPrimary = FuncUtils.getValueMap(mapperField, "isPrimary", Boolean.class);
                    if (isPrimary != null && isPrimary) {
                        sqlMapper.setPk(field);
                    }

                    settings.put("isPrimary", isPrimary);
                    settings.put("isUnique", FuncUtils.getValueMap(mapperField, "isUnique", Boolean.class));
                    settings.put("size", FuncUtils.getValueMap(mapperField, "size", Integer.class));
                    settings.put("digits", FuncUtils.getValueMap(mapperField, "digits", Integer.class));
                    settings.put("modelId", FuncUtils.getValueMap(mapperField, "modelId", Long.class));
                    settings.put("isList", FuncUtils.getValueMap(mapperField, "isList", Boolean.class));
                    sqlMapper.getSettingsMapper().put(field, settings);
//                    List<Map<String, Object>> fieldRelationList = listSQL("SELECT * FROM " + mapperFieldRelationTable + " WHERE MAPPER_FIELD_ID=:id ", mapperField, connection, null, false);
//                    System.out.println("fieldRelationList " + fieldRelationList);
                    if (fieldRelationMap.containsKey(mapperFieldId)) {
                        //System.out.println("fieldRelation " + fieldRelationMap.get(mapperFieldId));
                        sqlMapper.getRelationMapper().put(field, fieldRelationMap.get(mapperFieldId));
                    }


                }
            }
            //System.out.println("end.mapper");
            list = listSQL("SELECT * FROM " + this.modelTable, new HashMap<>(), connection, null, false);
            for (Map<String, Object> model : list) {
                SqlModel sqlModel = new SqlModel();
                sqlModel.setId(FuncUtils.getValueMap(model, "id", Long.class));
                sqlModel.setCode(FuncUtils.getValueMap(model, "code", String.class));
                sqlModel.setTableName(FuncUtils.getValueMap(model, "tableName", String.class));
                sqlModel.setSqlQuery(FuncUtils.getValueMap(model, "sqlQuery", String.class));
                sqlModel.setMapperId(FuncUtils.getValueMap(model, "mapperId", Long.class));
                sqlModel.setSeqName(FuncUtils.getValueMap(model, "seqName", String.class));
                sqlModel.setSchemaName(FuncUtils.getValueMap(model, "schemaName", String.class));
                if (mapperIdMapper.containsKey(sqlModel.getMapperId())) {
                    sqlModel.setMapper(mapperMapper.get(mapperIdMapper.get(sqlModel.getMapperId())));
                }
                modelMapper.put(model.get("code").toString(), sqlModel);
                modelIdMapper.put(sqlModel.getId(), sqlModel.getCode());
            }
            System.out.println("reloadSql.end");
            this.modulePortReload(currentModuleName);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                assert connection != null;
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return modelMapper;

    }

    @Override
    public void modulePortReload(String moduleName) {
//        // HttpHeaders
//        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Arrays.asList(new MediaType[]{MediaType.APPLICATION_JSON}));
//        // Request to return JSON format
//        headers.setContentType(MediaType.APPLICATION_JSON);
//
//        HttpEntity<String> entity = new HttpEntity<String>(headers);
//
//        RestTemplate restTemplate = new RestTemplate();
//
//        ResponseEntity<String> response = restTemplate.exchange(configUrl + "/" + moduleName + ".properties",
//                HttpMethod.GET, entity, String.class);
//
//        String result = response.getBody();
//
//        System.out.println(result);
//        Properties p = new Properties();
//        try {
//            p.load(new StringReader(response.getBody()));
//            modulePortMapper.put(moduleName, p.getProperty("server.port"));
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public String getModulePort(String moduleName) {
        if (modulePortMapper.containsKey(moduleName)) {
            return modulePortMapper.get(moduleName);
        }
        return null;
    }

    public SqlModel getModel(String code) {
        if (code.contains(".")) {
            return modelMapper.get(code);
        } else {
            return modelMapper.get(FuncUtils.toField(currentSchema) + "." + code);
        }
    }

    public SqlModel getModel(Long id) {
        String code = modelIdMapper.get(id);
        if (code != null) return getModel(code);
        return null;
    }

    public SqlMapper getMapper(String code) {
        return mapperMapper.get(code);
    }

    public SqlMapper getMapper(Long id) {
        String code = mapperIdMapper.get(id);
        if (code != null) return getMapper(code);
        return null;
    }

    @Override
    public List<Map<String, Object>> listSQL(SqlModel sqlModel, Map<String, Object> params) {
        return listSQL(generateSQL(sqlModel, params), params, sqlModel);
    }

    @Override
    public List<Map<String, Object>> listSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection, Boolean list) throws SQLException {
        return listSQL(generateSQL(sqlModel, params), params, connection, sqlModel, list);
    }

    private String generateSQL(SqlModel sqlModel, Map<String, Object> params) {
        String sql = sqlModel.getMapper() != null ? whereSQL(sqlModel.getMapper(), params) :
                whereSQLNative(sqlModel.getTableName() != null ? (sqlModel.getSchemaName() != null ? (sqlModel.getSchemaName() + ".") : "") + sqlModel.getTableName() : sqlModel.getSqlQuery(), params);
        sql = "SELECT * FROM (" + (sqlModel.getTableName() != null ? (sqlModel.getSchemaName() != null ? (sqlModel.getSchemaName() + ".") : "") + sqlModel.getTableName() : sqlModel.getSqlQuery()) + ") " + sql;
//        System.out.println("generateSQL " + sql + ", params " + params);
        return sql;
    }

    @Override
    public List<Map<String, Object>> listSQL(String sql, Map<String, Object> params, SqlModel sqlModel) {
        Connection connection = null;
        List list = null;
        try {
            connection = this.dataSource.getConnection();
            assert connection != null;
            list = listSQL(sql, params, connection, sqlModel, true);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("error " + sql);
        } finally {
            try {
                assert connection != null;
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public List<Map<String, Object>> listSQL(String sql, Map<String, Object> params, Connection connection, SqlModel sqlModel, Boolean list) throws SQLException {
        List<Map<String, Object>> rows = new ArrayList<>();
        sql = setParameter(sql, params);
        if (params.containsKey("_order") && params.containsKey("_sort")) {
            FuncUtils.debug(" is Order By " + params.get("_order"));

            sql = "SELECT * FROM (" + sql + ") ORDER BY " + ((sqlModel.getMapper() != null && sqlModel.getMapper().getFieldMapper().containsKey(params.get("_order"))) ?
                    sqlModel.getMapper().getFieldMapper().get(params.get("_order")) : FuncUtils.toColumn(params.get("_order").toString(), sqlModel)) + " " + params.get("_sort");
            params.remove("_order");
            params.remove("_sort");
        }

        if (params.containsKey("_page") && params.containsKey("_size")) {
            FuncUtils.debug("isPagination ");
            sql = "\n" +
                    "SELECT * FROM\n" +
                    "(\n" +
                    "    SELECT a.*, rownum r__\n" +
                    "    FROM\n" +
                    "    (\n" +
                    "        SELECT * FROM ( " + sql + " )\n" +
                    "    ) a\n" +
                    "    WHERE rownum < (((:_page + 1) * :_size) + 1 )\n" +
                    ")\n" +
                    "WHERE r__ >= (((:_page) * :_size) + 1)\n";
        }

//        System.out.println("runSQL " + sql);
        NamedParameterStatement statement = new NamedParameterStatement(connection, sql);
        setParameterStatement(params, statement);
        ResultSet rs = statement.executeQuery();
        Map<String, String> fieldMapper = new HashMap<>();
        Map<String, String> typeMapper = new HashMap<>();
        if (sqlModel == null || sqlModel.getMapper() == null) {
            metaDataToRow(fieldMapper, typeMapper, rs.getMetaData());
        } else {
            fieldMapper = sqlModel.getMapper().getFieldMapper();
            typeMapper = sqlModel.getMapper().getTypeMapper();
        }
        while (rs.next()) {
            Map<String, Object> row = new LinkedHashMap<>();
            resultToMap(rs, row, fieldMapper, typeMapper, connection, sqlModel, list);
            rows.add(row);
        }
        rs.close();
        statement.close();
        return rows;
    }


    @Override
    public Long totalSQL(SqlModel sqlModel, Map<String, Object> params) {
        return totalSQL(generateSQL(sqlModel, params), params);
    }

    @Override
    public Long totalSQL(String sql, Map<String, Object> params) {
        Connection connection = null;
        Long total = null;
        try {
            connection = this.dataSource.getConnection();
            assert connection != null;
            total = totalSQL(sql, params, connection);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("error " + sql);
        } finally {
            try {
                assert connection != null;
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return total;
    }

    @Override
    public Long totalSQL(String sql, Map<String, Object> params, Connection connection) throws SQLException {
        sql = setParameter(sql, params);
        sql = "SELECT COUNT(*) CNT FROM (" + sql + ")";
        NamedParameterStatement statement = new NamedParameterStatement(connection, sql);
        setParameterStatement(params, statement);
        ResultSet rs = statement.executeQuery();
        Long total = null;
        if (rs.next()) {
            total = rs.getLong(1);
        }
        rs.close();
        statement.close();
        return total;
    }

    private void setParameterStatement(Map<String, Object> params, NamedParameterStatement statement) throws SQLException {
        if (!params.isEmpty()) {
            for (String key : params.keySet())
                if (params.get(key) != null) {
                    Object values = params.get(key);
                    if (statement.isParameter(key)) {
                        if (values.getClass().getSimpleName().equals("ArrayList") || values.getClass().getSimpleName().equals("List")) {
                           // System.out.println("isList " + values);
                        }
                        statement.setObject(key, params.get(key));
                    }

                }
        }
        for (Object key : statement.getParameter().keySet()) {
            Object values = params.get(key);
            if (values != null) {
                if (values.getClass().getSimpleName().equals("ArrayList") || values.getClass().getSimpleName().equals("List")) {
                    //System.out.println("isList ..." + values);
                }

            }
            statement.setObject(key.toString(), params.getOrDefault(key, null));
        }
    }


    @Override
    public Map<String, Object> objectSQL(String sql, Map<String, Object> params, SqlModel sqlModel) {
        Connection connection = null;
        Map<String, Object> object = null;
        try {
            connection = this.dataSource.getConnection();
            assert connection != null;
            object = objectSQL(sql, params, connection, sqlModel);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("error " + sql);
        } finally {
            try {
                assert connection != null;
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return object;
    }

    private String sqlOperation(String type, Object value) {
        String equals = "=";
        if (value instanceof List) {
            System.out.println("sqlOperation " + value);
            List<String> values = (List<String>) value;
            if (values.size() > 0) {
                String firstValue = values.get(0).toUpperCase();
                if (FuncUtils.isOperation(firstValue)) {
                    equals = " " + firstValue.replaceAll("_", " ") + " ";
                } else {
                    equals = " IN ";
                }

            }

        } else {
            String strValue = value.toString();
            if (strValue.length() > 0) {
                if (strValue.substring(0, 1).equals("!")) {
                    System.out.println("not equals");
                    strValue = strValue.substring(1);
                    equals = " <> ";
                }
                if (strValue.toUpperCase().equals("NULL")) {
                    equals = equals.equals("=") ? " IS " : " IS NOT ";
                } else {
                    if (strValue.contains("%")) {
                        equals = " LIKE ";
                    } else if (strValue.substring(0, 1).equals(">") | strValue.substring(0, 1).equals("<")) {
                        equals = " " + strValue.substring(0, 1) + " ";
                    }
                }
                if (type.equals("DATE") || type.equals("DATE_TIME")) {
                    equals = " BETWEEN ";
                }

//                System.out.println("value " + value);
            }
        }
        return equals;
    }

    private String sqlDelete(SqlModel sqlModel, Map<String, Object> params) {
        return "DELETE FROM " + (sqlModel.getSchemaName() != null ? (sqlModel.getSchemaName() + ".") : "") + sqlModel.getTableName() +
                " WHERE " + sqlModel.getMapper().getPk() +
                "=:" + sqlModel.getMapper().getPk();
//                "=" + params.get(sqlModel.getMapper().getPk());
    }

    private String sqlUpdate(SqlModel sqlModel, Map<String, Object> params) {
        StringBuilder sql = new StringBuilder();
        sql.append(" UPDATE  ").append((sqlModel.getSchemaName() != null ? (sqlModel.getSchemaName() + ".") : "") + sqlModel.getTableName());
        sql.append("  SET ");
        StringBuilder values = new StringBuilder();
        for (String key : sqlModel.getMapper().getFieldMapper().keySet()) {
            if (sqlModel.getMapper().getFieldMapper().get(key) != null && FuncUtils.getValueMap(params, key, Object.class) != null
                    && !sqlModel.getMapper().getPk().equals(key)) {
                values.append(values.length() > 0 ? "," : "").append("\"").append(sqlModel.getMapper().getFieldMapper().get(key)).append("\"").append("=");
                values.append(FuncUtils.fieldToSQL(params, key, sqlModel.getMapper().getTypeMapper().get(key)));
//                values.append(FuncUtils.valueToSQL(params, key, sqlModel.getMapper().getTypeMapper().get(key), false));
            }
        }
        if (values.length() > 0) {
            sql.append(values);
            sql.append(" WHERE ");
            sql.append(sqlModel.getMapper().getFieldMapper().get(sqlModel.getMapper().getPk())).append("=:");
            sql.append(sqlModel.getMapper().getPk());
            return sql.toString();
        }
        return null;
    }

    private String sqlUpdateBatch(SqlModel sqlModel, Map<String, Object> params) {
        StringBuilder sql = new StringBuilder();
        sql.append(" UPDATE  ").append((sqlModel.getSchemaName() != null ? (sqlModel.getSchemaName() + ".") : "") + sqlModel.getTableName());
        sql.append("  SET ");
        StringBuilder values = new StringBuilder();
        for (String key : sqlModel.getMapper().getFieldMapper().keySet()) {
            if (!sqlModel.getMapper().getPk().equals(key) && sqlModel.getMapper().getFieldMapper().get(key) != null && FuncUtils.getValueMap(params, key, Object.class) != null) {
                values.append(values.length() > 0 ? "," : "").append("\"").append(sqlModel.getMapper().getFieldMapper().get(key)).append("\"").append("=");
                values.append(FuncUtils.fieldToSQL(params, key, sqlModel.getMapper().getTypeMapper().get(key)));
            }
        }
        if (values.length() > 0) {
            sql.append(values);
            sql.append(" WHERE ");
            sql.append(sqlModel.getMapper().getFieldMapper().get(sqlModel.getMapper().getPk())).append("=:");
            sql.append(sqlModel.getMapper().getPk());
            return sql.toString();
        }
        return null;
    }


    private String isNullable(SqlModel sqlModel, Map<String, Object> params) {
        StringBuilder message = new StringBuilder();
        for (String key : sqlModel.getMapper().getSettingsMapper().keySet()) {
            Boolean nullable = FuncUtils.getValueMap(sqlModel.getMapper().getSettingsMapper(), key + ".isNullable", Boolean.class);
            if (nullable != null && !nullable && params.containsKey(key) && params.get(key) == null) {
                message.append(" not null ").append(key);
            }
        }
        return message.toString();
    }

    private String sqlInsertBatch(SqlModel sqlModel, Map<String, Object> params, Boolean isHistory) {
        StringBuilder sql = new StringBuilder();
        sql.append(" INSERT INTO ").append((sqlModel.getSchemaName() != null ? (sqlModel.getSchemaName() + ".") : "") + sqlModel.getTableName());
        sql.append("  (");
        StringBuilder columns = new StringBuilder();
        StringBuilder values = new StringBuilder();
        if (sqlModel.getMapper().getFieldMapper().containsKey("createdDate") && !isHistory) {
            params.put("createdDate", FuncUtils.nowDate());
        }
        if (sqlModel.getMapper().getFieldMapper().containsKey("version") && !isHistory) {
            params.put("version", 1);
        }
        if (sqlModel.getMapper().getFieldMapper().containsKey("accessLevel") && !isHistory) {
            params.put("accessLevel", 0);
        }
        if (sqlModel.getMapper().getFieldMapper().containsKey("activeFlag") && !isHistory) {
            params.put("activeFlag", 1);
        }
        if (sqlModel.getMapper().getFieldMapper().containsKey("actionFlag") && !isHistory) {
            params.put("actionFlag", 1);
        }
        for (String key : sqlModel.getMapper().getFieldMapper().keySet()) {
            if (sqlModel.getMapper().getFieldMapper().get(key) != null && FuncUtils.getValueMap(params, key, Object.class) != null) {
                columns.append(columns.length() > 0 ? "," : "").append("\"").append(sqlModel.getMapper().getFieldMapper().get(key)).append("\"");
                values.append(values.length() > 0 ? "," : "").append(FuncUtils.fieldToSQL(params, key, sqlModel.getMapper().getTypeMapper().get(key)));
            }
        }
        if (sqlInsertString(sql, columns, values)) return sql.toString();
        return null;
//        return sqlInsert(sqlModel, params, isHistory);
    }

    private String sqlInsert(SqlModel sqlModel, Map<String, Object> params, Boolean isHistory) {
        StringBuilder sql = new StringBuilder();
        sql.append(" INSERT INTO ").append((sqlModel.getSchemaName() != null ? (sqlModel.getSchemaName() + ".") : "") + sqlModel.getTableName());
        sql.append("  (");
        StringBuilder columns = new StringBuilder();
        StringBuilder values = new StringBuilder();
        if (sqlModel.getMapper().getFieldMapper().containsKey("createdDate") && !isHistory) {
            params.put("createdDate", FuncUtils.nowDate());
        }
        if (sqlModel.getMapper().getFieldMapper().containsKey("version") && !isHistory) {
            params.put("version", 1);
        }
        if (sqlModel.getMapper().getFieldMapper().containsKey("accessLevel") && !isHistory) {
            params.put("accessLevel", 0);
        }
        if (sqlModel.getMapper().getFieldMapper().containsKey("activeFlag") && !isHistory) {
            params.put("activeFlag", 1);
        }
        if (sqlModel.getMapper().getFieldMapper().containsKey("actionFlag") && !isHistory) {
            params.put("actionFlag", 1);
        }
        for (String key : sqlModel.getMapper().getFieldMapper().keySet()) {
            if (sqlModel.getMapper().getFieldMapper().get(key) != null && FuncUtils.getValueMap(params, key, Object.class) != null) {
                columns.append(columns.length() > 0 ? "," : "").append("\"").append(sqlModel.getMapper().getFieldMapper().get(key)).append("\"");
                values.append(values.length() > 0 ? "," : "").append(FuncUtils.valueToSQL(params, key, sqlModel.getMapper().getTypeMapper().get(key), false));
//                values.append(values.length() > 0 ? ",:" : ":");
//                values.append(FuncUtils.fieldToSQL(params, key, sqlModel.getMapper().getTypeMapper().get(key)));

            }
        }
        if (sqlInsertString(sql, columns, values)) return sql.toString();
        return null;
    }


    private boolean sqlInsertString(StringBuilder sql, StringBuilder columns, StringBuilder values) {
        if (values.length() > 0 && columns.length() > 0) {
            sql.append(columns.toString()).append(") ");
            sql.append(" VALUES(");
            sql.append(values.toString()).append(") ");
            return true;
        }
        return false;
    }

    private String whereSQLNative(String query, Map<String, Object> params) {
        StringBuilder sql = new StringBuilder();
        for (String field : params.keySet()) {
            if (!"_order".equals(field) && !"_sort".equals(field) && !"_page".equals(field) && !"_size".equals(field) && !"activeFlag".equals(field) && !query.contains(":" + field)) {
                System.out.println("sqlNative " + field + ", " + sqlOperation(field, FuncUtils.getValueMap(params, field, Object.class)) + " , " + FuncUtils.valueToSQL(params, field, field, true));
                sql.append(sql.length() > 0 ? " AND " : "");
                sql.append(FuncUtils.toColumn(field, null)
                        + sqlOperation(field, FuncUtils.getValueMap(params, field, Object.class))
                        + FuncUtils.valueToSQL(params, field, field, true));
            }
        }
        return (sql.length() > 0 ? " WHERE " : "") + sql.toString();
    }

    private String whereSQL(SqlMapper mapper, Map<String, Object> params) {
        StringBuilder sql = new StringBuilder();
        for (String field : params.keySet()) {
            if (mapper.getFieldMapper().containsKey(field)) {
                sql.append(sql.length() > 0 ? " AND " : "");
                sql.append(mapper.getFieldMapper().get(field)
                        + sqlOperation(mapper.getTypeMapper().get(field), FuncUtils.getValueMap(params, field, Object.class))
                        + FuncUtils.valueToSQL(params, field, mapper.getTypeMapper().get(field), true));
            }
        }
        return (sql.length() > 0 ? " WHERE " : "") + sql.toString();
    }

    @Override
    public Map<String, Object> objectSQL(SqlModel sqlModel, Map<String, Object> params) {
        return objectSQL(generateSQL(sqlModel, params), params, sqlModel);
    }

    @Override
    public Map<String, Object> objectSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException {
        return objectSQL(generateSQL(sqlModel, params), params, connection, sqlModel);
    }

    @Override
    public Map<String, Object> getPkSQL(SqlModel sqlModel, Map<String, Object> params) {
        if (sqlModel != null && sqlModel.getMapper() != null && sqlModel.getMapper().getPk() != null) {
            String sql = "SELECT * FROM (" + (sqlModel.getTableName() != null ? (sqlModel.getSchemaName() != null ? (sqlModel.getSchemaName() + ".") : sqlModel.getTableName()) : sqlModel.getSqlQuery()) + ") WHERE ";
            System.out.println("getSQL " + sqlModel.getCode());
            return objectSQL(sql, params, sqlModel);
        }
        return null;

    }

    @Override
    public Map<String, Object> saveSQL(SqlModel sqlModel, Map<String, Object> params) {
        Connection connection = null;
        Map<String, Object> object = null;
        try {
            connection = this.dataSource.getConnection();
            assert connection != null;
            connection.setAutoCommit(false);
            object = saveSQL(sqlModel, params, connection);
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("exception " + sqlModel.getCode());
            try {
                assert connection != null;
                connection.rollback();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                assert connection != null;
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return object;
    }

    @Override
    public List<Map<String, Object>> patchSQL(SqlModel sqlModel, List<Map<String, Object>> list, Connection connection) throws SQLException {
        if (sqlModel.getTableName() != null) {
            NamedParameterStatement update = null;
            NamedParameterStatement history = null;
            NamedParameterStatement insert = null;
            NamedParameterStatement delete = null;
            List<Map<String, Object>> deleteIndex = new ArrayList<>();
            for (Map<String, Object> params : list) {
                System.out.println("saveSQL " + connection);
                String notNull = isNullable(sqlModel, params);
                if (notNull.length() > 0) {
                    Map<String, String> error = new HashMap<>();
                    error.put("error", notNull);
                    throw new SQLException("isNull");
                }
                String sql;
                Long id;
                if (params.containsKey(sqlModel.getMapper().getPk()) && FuncUtils.getValueMap(params, sqlModel.getMapper().getPk(), Long.class) != null) {
                    Map<String, Object> idParam = new HashMap<>();
                    idParam.put(sqlModel.getMapper().getPk(), params.get(sqlModel.getMapper().getPk()));
                    Map<String, Object> oldRow = objectSQL(sqlModel, idParam, connection);
                    if (isHistory(sqlModel)) {
                        // HISTORY_ROW
                        if (!FuncUtils.rowEquas(oldRow, params, sqlModel, true)) {
                            Long newId = nextId(sqlModel, connection);
                            oldRow.put(sqlModel.getMapper().getPk(), newId);
                            oldRow.put("activeFlag", 0);
                            oldRow.put("primaryId", FuncUtils.getValueMap(oldRow, sqlModel.getMapper().getPk(), Long.class));
                            System.out.println("old.version " + FuncUtils.getValueMap(oldRow, "version", Integer.class));

                            if (history == null) {
                                String oldSql = sqlInsertBatch(sqlModel, oldRow, true);
                                history = new NamedParameterStatement(connection, oldSql);
                                System.out.println("oldSql " + oldSql);
                            }

                            setParameterStatement(oldRow, history);
                            System.out.println("old.version " + FuncUtils.getValueMap(oldRow, "version", Integer.class));
                            history.addBatch();
//                            int result = statement.executeUpdate();
//                            System.out.println("insert.oldRow " + result);
                            params.put("updatedDate", FuncUtils.nowDate());
                            System.out.println("old.version " + FuncUtils.getValueMap(oldRow, "version", Integer.class));
                            if (!params.containsKey("updatedBy"))
                                params.put("updatedBy", 2L);
                            params.put("version", FuncUtils.getValueMap(oldRow, "version", Integer.class) + 1);
                            params.put("actionFlag", 2L);

//                            statement.close();
                        }
                    }
                    FuncUtils.getValueMap(params, sqlModel.getMapper().getPk(), Long.class);
                    if (!FuncUtils.rowEquas(oldRow, params, sqlModel, false)) {
                        if (update == null) {
                            sql = sqlUpdateBatch(sqlModel, params);
                            System.out.println("patchSQL.update " + sql);
                            System.out.println("patchSQL.param " + params + " :oldRow: " + oldRow);
                            update = new NamedParameterStatement(connection, sql);
                        }
//                        sql = setParameter(sql, params);
                        params.put("updatedDate", FuncUtils.nowDate());
                        if (!params.containsKey("updatedBy"))
                            params.put("updatedBy", 2L);
                        params.put("actionFlag", 2L);
                        setParameterStatement(params, update);
                        update.addBatch();

                    } else {
                        System.out.println("not update patch");
                        if (params.containsKey("_rowState")) {
                            if (delete == null) {
                                sql = sqlDelete(sqlModel, params);
                                System.out.println("patchSQL.delete " + sql);
                                System.out.println("patchSQL.param " + params);
                                delete = new NamedParameterStatement(connection, sql);
                            }
                            setParameterStatement(params, delete);
                            delete.addBatch();
                            deleteIndex.add(params);
                        }
                    }


                } else {
                    id = nextId(sqlModel, connection);
                    params.put(sqlModel.getMapper().getPk(), id);
                    if (sqlModel.getMapper().getFieldMapper().containsKey("primaryId")) {
                        params.put("primaryId", id);
                    }


//                    sql = setParameter(sql, params);

                    if (insert == null) {
                        sql = sqlInsertBatch(sqlModel, params, false);
                        System.out.println("patchSQL.insert " + sql);
                        insert = new NamedParameterStatement(connection, sql);
                    }
//                    NamedParameterStatement statement = ;
                    setParameterStatement(params, insert);
                    insert.addBatch();
//                    int result = statement.executeUpdate();
//                    System.out.println("update " + result);
//                    statement.close();

                }
                Map<String, String> relationFields = sqlModel.getMapper().getTypeMapper().entrySet().stream()
                        .filter(map -> map.getValue().equals("LIST") || map.getValue().equals("MAP"))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                System.out.println("isRelation.saveSQL " + relationFields);
//                if (history != null) {
//                    System.out.println("history.patch " + history.executeBatch().length);
////                    history.close();
//                }
//                if (update != null) {
//                    System.out.println("update.patch " + update.executeBatch().length);
////                    update.close();
//                }
//                if (insert != null) {
//                    System.out.println("insert.patch " + insert.executeBatch().length);
////                    insert.close();
//                }
//                if (delete != null) {
//                    System.out.println("delete.patch " + delete.executeBatch().length);
//                }
                relationValueSave(sqlModel, params, connection, relationFields);

//                params.clear();
//                params.put(sqlModel.getMapper().getPk(), id);
//                return objectSQL(sqlModel, params, connection);s
            }
            if (history != null) {
                System.out.println("history.patch " + history.executeBatch().length);
                history.close();
            }
            if (update != null) {
                System.out.println("update.patch " + update.executeBatch().length);
                update.close();
            }
            if (insert != null) {
                System.out.println("insert.patch " + insert.executeBatch().length);
                insert.close();
            }
            if (delete != null) {
                System.out.println("delete.patch " + delete.executeBatch().length);
                delete.close();
            }
            System.out.println("deleteIndex. " + deleteIndex);
            if (!deleteIndex.isEmpty()) {
                for (Map<String, Object> i : deleteIndex) {
                    list.remove(i);
                    System.out.println("remote.i " + i + ", " + list);
                }
            }
            return list;
        } else {
            System.out.println("tableName is null " + sqlModel.getTableName());
            return null;
        }
    }

    private void relationValueSet(Map<String, Object> params, List<Map<String, Object>> relationList, Map<String, Object> map) {
        for (Map<String, Object> relation : relationList) {
            String fromFieldName = FuncUtils.getValueMap(relation, "fromFieldName", String.class);
            String staticValue = FuncUtils.getValueMap(relation, "staticValue", String.class);
            String toFieldName = FuncUtils.getValueMap(relation, "toFieldName", String.class);
            if (!(fromFieldName == null && staticValue != null && staticValue.toUpperCase().startsWith("(SELECT"))) {
                map.put(toFieldName, fromFieldName != null ? FuncUtils.getValueMap(params, fromFieldName, Object.class) : staticValue);
            }
        }
    }

    @Override
    public List<Map<String, Object>> patchSQL(SqlModel sqlModel, List<Map<String, Object>> list) {
        Connection connection = null;
        List<Map<String, Object>> result = new ArrayList<>();
        try {
            connection = this.dataSource.getConnection();
            assert connection != null;
            connection.setAutoCommit(false);
            result = patchSQL(sqlModel, list, connection);
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("exception " + sqlModel.toString());
            try {
                assert connection != null;
                connection.rollback();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                assert connection != null;
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public Map<String, Object> deleteSQL(SqlModel sqlModel, Map<String, Object> params) {
        Connection connection = null;
        Map<String, Object> object = null;
        try {
            connection = this.dataSource.getConnection();
            assert connection != null;
            connection.setAutoCommit(false);
            object = deleteSQL(sqlModel, params, connection);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("error " + sqlModel.toString());
            try {
                assert connection != null;
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                assert connection != null;
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return object;
    }

    @Override
    public Map<String, Object> removeSQL(SqlModel sqlModel, Map<String, Object> params) {
        return null;
    }

    @Override
    public Map<String, Object> restoreSQL(SqlModel sqlModel, Map<String, Object> params) {
        return null;
    }

    @Override
    public Map<String, Object> getPkSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException {
        if (sqlModel != null && sqlModel.getMapper() != null && sqlModel.getMapper().getPk() != null) {
            String sql = "";
            if (params.containsKey(sqlModel.getMapper().getPk())) {
                sql = FuncUtils.toColumn(sqlModel.getMapper().getPk(), sqlModel) + "=" + params.get(sqlModel.getMapper().getPk());
            }
            sql = "SELECT * FROM (" + (sqlModel.getTableName() != null ? (sqlModel.getSchemaName() != null ? (sqlModel.getSchemaName() + ".") : sqlModel.getTableName()) : sqlModel.getSqlQuery()) + ") WHERE " + sql;
            return objectSQL(sql, params, connection, sqlModel);
        }
        return null;
    }

    private Long nextId(SqlModel sqlModel, Connection connection) throws SQLException {
        Map<String, Object> nextId = objectSQL("SELECT (" + sqlModel.getSchemaName() + "." + sqlModel.getSeqName() + ") ID FROM DUAL", new HashMap<>(), connection, null);
        if (nextId == null || nextId.get("id") == null) {
            System.out.println("SeqProblem " + sqlModel.getSeqName());
            return null;
        }
        return FuncUtils.getValueMap(nextId, "id", Long.class);
    }

    private Boolean isHistory(SqlModel sqlModel) {
        return sqlModel.getMapper().getFieldMapper().containsKey("version")
                && sqlModel.getMapper().getFieldMapper().containsKey("activeFlag")
                && sqlModel.getMapper().getFieldMapper().containsKey("primaryId");
    }

    @Override
    public Map<String, Object> saveSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException {
        if (sqlModel.getTableName() != null) {
            System.out.println("saveSQL " + connection);
            String notNull = isNullable(sqlModel, params);
            if (notNull.length() > 0) {
                Map<String, Object> error = new HashMap<>();
                error.put("error", notNull);
                return error;
            }
            String sql;
            Object id;
            if (params.containsKey(sqlModel.getMapper().getPk()) && FuncUtils.getValueMap(params, sqlModel.getMapper().getPk(), Object.class) != null) {

                Map<String, Object> idParam = new HashMap<>();
                idParam.put(sqlModel.getMapper().getPk(), params.get(sqlModel.getMapper().getPk()));
                Map<String, Object> oldRow = objectSQL(sqlModel, idParam, connection);
                if (isHistory(sqlModel)) {
                    // HISTORY_ROW
                    if (!FuncUtils.rowEquas(oldRow, params, sqlModel, true)) {
                        Long newId = nextId(sqlModel, connection);
                        oldRow.put(sqlModel.getMapper().getPk(), newId);
                        oldRow.put("activeFlag", 0);
                        oldRow.put("primaryId", FuncUtils.getValueMap(oldRow, sqlModel.getMapper().getPk(), Object.class));
//                        System.out.println("old.version " + FuncUtils.getValueMap(oldRow, "version", Integer.class));
                        String oldSql = sqlInsert(sqlModel, oldRow, true);
                        NamedParameterStatement statement = new NamedParameterStatement(connection, oldSql);
//                        System.out.println("oldSql " + oldSql);
                        setParameterStatement(oldRow, statement);
//                        System.out.println("old.version " + FuncUtils.getValueMap(oldRow, "version", Integer.class));
                        int result = statement.executeUpdate();
//                        System.out.println("insert.oldRow " + result);
                        params.put("updatedDate", FuncUtils.nowDate());
//                        System.out.println("old.version " + FuncUtils.getValueMap(oldRow, "version", Integer.class));
                        if (!params.containsKey("updatedBy"))
                            params.put("updatedBy", 2L);
                        params.put("version", FuncUtils.getValueMap(oldRow, "version", Integer.class) + 1);
                        params.put("actionFlag", 2L);
                        statement.close();
                    }
                }
                id = FuncUtils.getValueMap(params, sqlModel.getMapper().getPk(), Object.class);
                if (!FuncUtils.rowEquas(oldRow, params, sqlModel, false)) {
                    sql = sqlUpdate(sqlModel, params);
                    System.out.println("saveSQL " + sql);
                    sql = setParameter(sql, params);
                    NamedParameterStatement statement = new NamedParameterStatement(connection, sql);
                    setParameterStatement(params, statement);
                    int result = statement.executeUpdate();
//                    System.out.println("update " + result);
                    statement.close();
                } else {
                    System.out.println("not update");
                }


            } else {
                id = nextId(sqlModel, connection);
                params.put(sqlModel.getMapper().getPk(), id);
                if (sqlModel.getMapper().getFieldMapper().containsKey("primaryId")) {
                    params.put("primaryId", id);
                }
                sql = sqlInsert(sqlModel, params, false);
//                System.out.println("saveSQL " + sql);
                sql = setParameter(sql, params);
                NamedParameterStatement statement = new NamedParameterStatement(connection, sql);
                setParameterStatement(params, statement);
                int result = statement.executeUpdate();
//                System.out.println("update " + result);
                statement.close();

            }
            Map<String, String> relationFields = sqlModel.getMapper().getTypeMapper().entrySet().stream()
                    .filter(map -> map.getValue().equals("LIST") || map.getValue().equals("MAP"))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            System.out.println("isRelation.saveSQL " + relationFields);
            for (String field : relationFields.keySet()) {
                if (relationFields.get(field).equals("LIST")) {
                    // LIST
                    Long modelId = FuncUtils.getValueMap(sqlModel.getMapper().getSettingsMapper(), field + ".modelId", Long.class);
                    SqlModel model = getModel(modelId);
                    List<Map<String, Object>> childrenList = FuncUtils.getValueMap(params, field, List.class);
//                    System.out.println("childrenList " + childrenList + ", " + field + ", param " + params);
                    if (modelId != null && model != null && model.getMapper() != null && model.getTableName() != null
                            && childrenList != null && childrenList.size() > 0) {
                        for (Map<String, Object> map : childrenList) {
                            List<Map<String, Object>> relationList = FuncUtils.getValueMap(sqlModel.getMapper().getRelationMapper(), field, List.class);
                            if (relationList.size() > 0) {
                                relationValueSet(params, relationList, map);
                            }
                            System.out.println("relation params " + map + ", model " + model);
                            Map<String, Object> relationSave = saveSQL(model, map, connection);
                            System.out.println("relationSave " + relationSave);
                        }
                    }
                    List<Map<String, Object>> deleteList = FuncUtils.getValueMap(params, "_delete_" + field, List.class);
//                    System.out.println("deleteList " + deleteList + ", " + field + ", param " + params);
                    if (deleteList != null && deleteList.size() > 0) {
                        for (Map<String, Object> map : deleteList) {
                            List<Map<String, Object>> relationList = FuncUtils.getValueMap(sqlModel.getMapper().getRelationMapper(), field, List.class);
                            if (relationList.size() > 0) {
                                relationValueSet(params, relationList, map);
                            }
                            System.out.println("delete.relation params " + map + ", model " + model);
                            deleteSQL(model, map, connection);
                        }
                    }

                } else {
                    // MAP
                    Map<String, Object> childrenMap = FuncUtils.getValueMap(params, field, Map.class);
                }

            }

            params.clear();
            params.put(sqlModel.getMapper().getPk(), id);
            return objectSQL(sqlModel, params, connection);
        } else {
            System.out.println("tableName is null " + sqlModel.getTableName());
            return null;
        }
    }

    private void relationValueSave(SqlModel sqlModel, Map<String, Object> params, Connection connection, Map<String, String> relationFields) throws SQLException {
        for (String field : relationFields.keySet()) {
            if (relationFields.get(field).equals("LIST")) {
                // LIST
                List<Map<String, Object>> childrenList = FuncUtils.getValueMap(params, field, List.class);
//                System.out.println("childrenList " + childrenList + ", " + field + ", param " + params);
                Long modelId = FuncUtils.getValueMap(sqlModel.getMapper().getSettingsMapper(), field + ".modelId", Long.class);
                SqlModel model = getModel(modelId);
                if (modelId != null && model != null && model.getMapper() != null && model.getTableName() != null) {

                    List<Map<String, Object>> relationList = FuncUtils.getValueMap(sqlModel.getMapper().getRelationMapper(), field, List.class);
                    if (relationList.size() > 0) {
                        if (childrenList != null && childrenList.size() > 0) {
                            for (Map<String, Object> map : childrenList) {
                                relationValueSet(params, relationList, map);
//                                System.out.println("relation params " + map + ", model " + model);
                            }
                            patchSQL(model, childrenList, connection);
                        }

                    }
//                                saveSQL(model, map, connection);
                }
            } else {
                // MAP
                Map<String, Object> childrenMap = FuncUtils.getValueMap(params, field, Map.class);
            }

        }
    }

    @Override
    public Map<String, Object> deleteSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException {
        if (sqlModel.getTableName() != null) {
            System.out.println("deleteSQL " + connection);
            String sql;
            sql = sqlDelete(sqlModel, params);
            System.out.println("deleteSQL " + sql);
            sql = setParameter(sql, params);
            NamedParameterStatement statement = new NamedParameterStatement(connection, sql);
            setParameterStatement(params, statement);
            int result = statement.executeUpdate();
            System.out.println("delete " + result);
            statement.close();
            return params;
        } else {
            System.out.println("tableName is null " + sqlModel.getTableName());
            return null;
        }
    }

    @Override
    public Map<String, Object> removeSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException {
        return null;
    }

    @Override
    public Map<String, Object> restoreSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException {
        return null;
    }

    @Override
    public Map<String, Object> objectSQL(String sql, Map<String, Object> params, Connection connection, SqlModel sqlModel) throws SQLException {
        sql = setParameter(sql, params);
        return sqlRun(connection, sql, params, sqlModel);
    }

    private Map<String, Object> sqlRun(Connection connection, String sql, Map<String, Object> params, SqlModel sqlModel) throws SQLException {
        Map<String, Object> row = null;
        NamedParameterStatement statement = new NamedParameterStatement(connection, sql);
        setParameterStatement(params, statement);
        ResultSet rs = statement.executeQuery();
        Map<String, String> fieldMapper = new HashMap<>();
        Map<String, String> typeMapper = new HashMap<>();
        if (sqlModel == null || sqlModel.getMapper() == null) {
            metaDataToRow(fieldMapper, typeMapper, rs.getMetaData());
        } else {
            fieldMapper = sqlModel.getMapper().getFieldMapper();
            typeMapper = sqlModel.getMapper().getTypeMapper();
        }
        if (rs.next()) {
            row = new LinkedHashMap<>();
            resultToMap(rs, row, fieldMapper, typeMapper, connection, sqlModel, false);
        }
        rs.close();
        statement.close();
        return row;
    }

    @Override
    public SqlModel generateSqlTable(String tableName) {
        return generateSqlTable(currentSchema, tableName);
    }

    @Override
    public SqlModel generateSqlTable(String schema, String tableName) {
        Connection connection = null;
        SqlModel sqlModel = null;
        schema = schema.toUpperCase();
        tableName = tableName.toUpperCase();
        try {
            connection = getConnection();
            assert connection != null;
            connection.setAutoCommit(false);
//            NamedParameterStatement statement = new NamedParameterStatement(connection, "select * from " + table);
//            ResultSet rs = statement.executeQuery();
            Map<String, Object> row = null;
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet rs = metaData.getTables(schema, null, tableName, null);
            Map<String, String> columnMap = new HashMap<>();
            Map<String, String> typeMap = new HashMap<>();
            List<String> uniqueList = new ArrayList<>();
            ResultSetMetaData resultSetMetaData = rs.getMetaData();
            metaDataToRow(columnMap, typeMap, resultSetMetaData);
            if (rs.next()) {
                row = new LinkedHashMap<>();
                resultToMap(rs, row, columnMap, typeMap, connection, null, false);
                System.out.println("columns " + columnMap);
                System.out.println("typeMap " + typeMap);
                System.out.println("row " + row);
                // PK
                ResultSet pk = metaData.getPrimaryKeys(schema, schema, tableName);
                String pkColumn = null;
                while (pk.next()) {
                    String columnName = pk.getString("COLUMN_NAME");
                    pkColumn = FuncUtils.toField(columnName);
                }
                pk.close();
                HashMap<String, Object> params = new HashMap<>();
                params.put("schema", schema);
                params.put("tableName", tableName);
                Map<String, Object> tableComment = objectSQL("SELECT * FROM ALL_TAB_COMMENTS WHERE OWNER=:schema AND TABLE_NAME=:tableName", params, connection, null);
                System.out.println("tableComment " + tableComment);
                // UNIQUE
                ResultSet unique = metaData.getIndexInfo(schema, schema, tableName, true, true);
                while (unique.next()) {
                    String indexName = unique.getString("INDEX_NAME");
                    if (indexName != null) {
                        String columnName = FuncUtils.toField(unique.getString("COLUMN_NAME"));
                        if (!pk.equals(columnName)) {
                            uniqueList.add(columnName);
                        }
                    }
                }
                unique.close();
                // SEQ_NAME
                String seqName = "SEQ_" + tableName + ".NEXTVAL";
                if (tableName.equals(modelTable) || tableName.equals(mapperTable)
                        || tableName.equals(mapperFieldTable) || tableName.equals(mapperFieldRelationTable)) {
                    seqName = "SELECT NVL(MAX(ID) + 1,1) ID FROM " + tableName;
                }
//                resultToMap(tableName.getMetaData(), columnMap, typeMap);
                ResultSet colResultSet = metaData.getColumns(schema, null, tableName, null);
                SqlMapper mapper = new SqlMapper();
                mapper.setCode(tableName + "_MAPPER");
                mapper.setName(tableName + "_MAPPER");
                ResultSetMetaData colmd = colResultSet.getMetaData();
                row = new LinkedHashMap<>();
                ResultSetMetaData colResultSetMetaData = colResultSet.getMetaData();
                metaDataToRow(columnMap, typeMap, colResultSetMetaData);
                row.put("columnMap", columnMap);
                row.put("typeMap", typeMap);
                while (colResultSet.next()) {
//                    for(int i = 0; i < colmd.getColumnCount(); i ++) {
//                        System.out.println("col " + i + ", " + colmd.getColumnName(i + 1) + ", " +  colResultSet.getObject(i + 1));
//                    }
                    String columnName = colResultSet.getString("COLUMN_NAME");
                    params.put("columnName", columnName);
                    Map<String, Object> columnComment = objectSQL("SELECT * FROM ALL_COL_COMMENTS WHERE OWNER=:schema AND TABLE_NAME=:tableName AND COLUMN_NAME=:columnName", params, connection, null);
                    String fieldName = FuncUtils.toField(columnName);
                    int datatype = colResultSet.getInt("DATA_TYPE");
                    int columnsize = colResultSet.getInt("COLUMN_SIZE");
                    int decimaldigits = colResultSet.getInt("DECIMAL_DIGITS");
                    Boolean isNullable = true; //colResultSet.getBoolean("IS_NULLABLE");

                    Boolean isPrimary = fieldName.equals(pkColumn);
                    Map<String, Object> settings = new HashMap<>();
                    settings.put("isNullable", isNullable);
                    settings.put("isPrimary", isPrimary);
                    settings.put("size", columnsize);
                    settings.put("digits", decimaldigits);
                    settings.put("isUnique", uniqueList.contains(fieldName));
                    settings.put("label", columnComment != null && FuncUtils.getValueMap(columnComment, "comments", String.class) != null ? FuncUtils.getValueMap(columnComment, "comments", String.class) : columnName);
                    mapper.getSettingsMapper().put(fieldName, settings);
                    mapper.getFieldMapper().put(fieldName, columnName);
                    mapper.getTypeMapper().put(fieldName, FuncUtils.toType(datatype, decimaldigits, columnsize));
                    mapper.setPk(pkColumn);

                }
                colResultSet.close();
                sqlModel = new SqlModel();
                String code = schema != null ? FuncUtils.toField(schema) + "." : "";
                sqlModel.setCode(code + FuncUtils.toField(tableName));
                sqlModel.setMapper(mapper);
                sqlModel.setTableName(tableName);
                sqlModel.setName(tableComment != null && FuncUtils.getValueMap(tableComment, "comments", String.class) != null ? FuncUtils.getValueMap(tableComment, "comments", String.class) : tableName);
                sqlModel.setSeqName(seqName);
                sqlModel.setSchemaName(schema);
                insertModel(connection, sqlModel);
            } else {
                return null;
            }


//            object.put("code", "");
//            object.put("columns", columnMap);
//            object.put("types", typeMap);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("error ");
            try {
                assert connection != null;
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                assert connection != null;
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return sqlModel;
    }

    private void metaDataToRow(Map<String, String> columnMap, Map<String, String> typeMap, ResultSetMetaData colResultSetMetaData) throws SQLException {
        for (int i = 0; i < colResultSetMetaData.getColumnCount(); i++) {
            String columnName = colResultSetMetaData.getColumnName(i + 1);
            String fieldName = FuncUtils.toField(colResultSetMetaData.getColumnName(i + 1));
            columnMap.put(fieldName, columnName);
            typeMap.put(fieldName, colResultSetMetaData.getColumnTypeName(i + 1));
        }
    }


    private SqlModel insertModel(Connection connection, SqlModel sqlModel) throws SQLException {
        Map nextVal = this.objectSQL("SELECT NVL(MAX(ID) + 1,1) ID FROM " + mapperTable, new HashMap<>(), connection, null);
        SqlMapper mapper = sqlModel.getMapper();
        System.out.println("nextVal " + nextVal);
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("code", mapper.getCode());
        Map mapperCheck = objectSQL("SELECT * FROM " + mapperTable + " WHERE CODE=:code", params, connection, null);
        if (mapperCheck != null) {
            mapper.setId(FuncUtils.getValueMap(mapperCheck, "id", Long.class));
        } else {
            mapper.setId(FuncUtils.getValueMap(nextVal, "id", Long.class));
            params.put("id", mapper.getId());
            params.put("code", mapper.getCode());
            params.put("name", mapper.getName());
            NamedParameterStatement statement = new NamedParameterStatement(connection, "INSERT INTO " + mapperTable + "(ID, CODE, NAME) VALUES(:id, :code, :name)");
            setParameterStatement(params, statement);
            System.out.println("update " + statement.executeQuery());
            statement.close();
        }

        nextVal = this.objectSQL("SELECT NVL(MAX(ID) + 1,1) ID FROM " + mapperFieldTable, new HashMap<>(), connection, null);
        Long fieldNextId = FuncUtils.getValueMap(nextVal, "id", Long.class);
        int deleteFields = this.executeSQL("DELETE FROM " + mapperFieldTable + " WHERE COLUMN_NAME IS NOT NULL AND MAPPER_ID=" + mapper.getId(), new HashMap<>(), connection);
        System.out.println("delete fields " + deleteFields);
        for (String field : mapper.getFieldMapper().keySet()) {
            String insertSQL = "INSERT INTO " + mapperFieldTable + "(ID,MAPPER_ID,COLUMN_NAME,FIELD_NAME,\"TYPE\",IS_PRIMARY,IS_UNIQUE,\"SIZE\",DIGITS,LABEL)" +
                    "  VALUES(:id, :mapperId, :columnName, :fieldName,:type, :isPrimary, :isUnique, :size, :digits, :label)";
            params.clear();
            params.put("id", fieldNextId);
            params.put("mapperId", mapper.getId());
            params.put("columnName", mapper.getFieldMapper().get(field));
            params.put("fieldName", field);
            params.put("type", mapper.getTypeMapper().get(field));
            params.put("isPrimary", FuncUtils.valueToSQL(mapper.getSettingsMapper(), field + ".isPrimary", "BOOL", false));
            params.put("isUnique", FuncUtils.valueToSQL(mapper.getSettingsMapper(), field + ".isUnique", "BOOL", false));
            params.put("size", FuncUtils.valueToSQL(mapper.getSettingsMapper(), field + ".size", "INT", false));
            params.put("digits", FuncUtils.valueToSQL(mapper.getSettingsMapper(), field + ".digits", "INT", false));
            params.put("label", FuncUtils.valueToSQL(mapper.getSettingsMapper(), field + ".label", "STR", false));
            NamedParameterStatement fieldStatement = new NamedParameterStatement(connection, insertSQL);
            setParameterStatement(params, fieldStatement);
            fieldStatement.executeUpdate();
            fieldNextId += 1;
            fieldStatement.close();
        }
        params.clear();
        params.put("code", sqlModel.getCode());
        Map modelCheck = this.objectSQL("SELECT * FROM " + modelTable + " WHERE CODE=:code", params, connection, null);
        if (modelCheck != null) {
            String updateSQL = "UPDATE " + modelTable + "  SET MAPPER_ID=:mapperId,SEQ_NAME=:seqName,TABLE_NAME=:tableName,NAME=:name,SCHEMA_NAME=:schemaName WHERE ID=:id";
            params.clear();
            params.put("id", FuncUtils.getValueMap(modelCheck, "id", Long.class));
            params.put("mapperId", mapper.getId());
            params.put("name", sqlModel.getName());
            params.put("tableName", sqlModel.getTableName());
            if (sqlModel.getTableName() != null && sqlModel.getMapper() != null) {
                params.put("seqName", sqlModel.getSeqName());
            }
            if (sqlModel.getSchemaName() != null && sqlModel.getMapper() != null) {
                params.put("schemaName", sqlModel.getSchemaName());
            }
            NamedParameterStatement fieldStatement = new NamedParameterStatement(connection, updateSQL);
            setParameterStatement(params, fieldStatement);
            fieldStatement.executeUpdate();
            sqlModel.setId(FuncUtils.getValueMap(modelCheck, "id", Long.class));
            sqlModel.setSqlQuery(FuncUtils.getValueMap(modelCheck, "sqlQuery", String.class));
            System.out.println("sqlModel " + sqlModel);
            sqlModel.setTableName(FuncUtils.getValueMap(modelCheck, "tableName", String.class));
            fieldStatement.close();
        } else {
            nextVal = this.objectSQL("SELECT NVL(MAX(ID) + 1,1) ID FROM " + modelTable, new HashMap<>(), connection, null);
            String insertSQL = "INSERT INTO " + modelTable + " (ID,CODE,NAME,TABLE_NAME,SQL_QUERY,MAPPER_ID,SEQ_NAME,SCHEMA_NAME) VALUES(:id,:code,:name,:table,:sqlQuery,:mapperId, :seqName, :schemaName)";
            params.clear();
            params.put("id", FuncUtils.getValueMap(nextVal, "id", Long.class));
            params.put("code", sqlModel.getCode());
            params.put("name", sqlModel.getCode());
            params.put("table", sqlModel.getTableName());
            params.put("sqlQuery", sqlModel.getSqlQuery());
            params.put("mapperId", mapper.getId());
            if (sqlModel.getTableName() != null && sqlModel.getMapper() != null) {
                params.put("seqName", sqlModel.getSeqName());
            }
            if (sqlModel.getSchemaName() != null && sqlModel.getMapper() != null) {
                params.put("schemaName", sqlModel.getSchemaName());
            }
            NamedParameterStatement fieldStatement = new NamedParameterStatement(connection, insertSQL);
            setParameterStatement(params, fieldStatement);
            fieldStatement.executeUpdate();
            fieldStatement.close();
        }
        if (modelMapper.containsKey(sqlModel.getCode())) {
            modelMapper.remove(sqlModel.getCode());
            modelIdMapper.remove(sqlModel.getId());
            mapperMapper.remove(sqlModel.getMapper().getCode());
            mapperIdMapper.remove(sqlModel.getMapper().getId());
        }

        mapperIdMapper.put(sqlModel.getMapper().getId(), sqlModel.getMapper().getCode());
        mapperMapper.put(sqlModel.getMapper().getCode(), sqlModel.getMapper());
        modelIdMapper.put(sqlModel.getId(), sqlModel.getCode());
        modelMapper.put(sqlModel.getCode(), sqlModel);
        return sqlModel;
    }

    private void resultToMap(ResultSet rs, Map<String, Object> row, Map<String, String> columnMap, Map<String, String> typeMap, Connection connection, SqlModel sqlModel, Boolean list) throws SQLException {

        for (String field : columnMap.keySet()) {
            String col = columnMap.get(field);
//            System.out.println("col " + col + ", " + typeMap.get(field));
            try {
                switch (typeMap.get(field)) {
                    case "BLOB":
                        Blob blob = rs.getBlob(col);
                        if (blob != null)
                            row.put(field, new String(blob.getBytes(1L, (int) blob.length())));
                        break;
                    case "CLOB":
                        Clob clob = rs.getClob(col);
                        if (clob != null)
                            row.put(field, clob.getSubString(1L, (int) clob.length()));
//                        System.out.println("ISCLOB " + clob);
                        break;
                    case "DATE":
                        Date date = rs.getDate(col);
                        if (date != null)
                            row.put(field, FuncUtils.sd.format(date));
                        break;
                    case "TIMESTAMP":
                    case "DATE_TIME":
                        date = rs.getDate(col);
                        if (date != null)
                            row.put(field, FuncUtils.sdf.format(date));
                        break;
                    case "MAP":
//                        System.out.println("map.model");
                        Long modelId = FuncUtils.getValueMap(sqlModel.getMapper().getSettingsMapper(), field + ".modelId", Long.class);
                        Boolean isList = FuncUtils.getValueMap(sqlModel.getMapper().getSettingsMapper(), field + ".isList", Boolean.class);
                        if (!list || (isList != null && isList)) {
//                            System.out.println("map.model " + modelId + ", " + isList);
                            SqlModel model = getModel(modelId);
                            List<Map<String, Object>> relationList = FuncUtils.getValueMap(sqlModel.getMapper().getRelationMapper(), field, List.class);
                            if (model != null && relationList.size() > 0) {
//                                System.out.println("modelRelation Data " + model);

                                Map<String, Object> params = new HashMap<>();
                                for (Map<String, Object> relation : relationList) {
                                    String toFieldName = FuncUtils.getValueMap(relation, "toFieldName", String.class);
                                    String fromFieldName = FuncUtils.getValueMap(relation, "fromFieldName", String.class);
                                    params.put(toFieldName, FuncUtils.getValueMap(row, fromFieldName, Object.class));
                                }
//                                System.out.println("relation params " + params);
                                Map<String, Object> relationData = objectSQL(model, params, connection);
                                row.put(field, relationData);
                            }
                            break;
                        }

                    case "LIST":
//                        System.out.println("list.model");
                        modelId = FuncUtils.getValueMap(sqlModel.getMapper().getSettingsMapper(), field + ".modelId", Long.class);
                        isList = FuncUtils.getValueMap(sqlModel.getMapper().getSettingsMapper(), field + ".isList", Boolean.class);
//                        System.out.println("list.model " + modelId + ", " + isList + ", list " + list);
//                        System.out.println("modelrelation " + modelId);
                        if (!list || (isList != null && isList)) {
                            SqlModel model = getModel(modelId);
                            List<Map<String, Object>> relationList = FuncUtils.getValueMap(sqlModel.getMapper().getRelationMapper(), field, List.class);
                            if (model != null && relationList != null && relationList.size() > 0) {
//                                System.out.println("modelRelation Data " + model.getId());
                                Map<String, Object> params = new HashMap<>();
                                for (Map<String, Object> relation : relationList) {

                                    String fromFieldName = FuncUtils.getValueMap(relation, "fromFieldName", String.class);
                                    String operator = FuncUtils.getValueMap(relation, "operator", String.class);
                                    String staticValue = FuncUtils.getValueMap(relation, "staticValue", String.class);
                                    String toFieldName = FuncUtils.getValueMap(relation, "toFieldName", String.class);
                                    if (operator != null && staticValue != null) {
                                        List<String> listParam = new ArrayList<>();
                                        listParam.add(operator);
                                        listParam.add(staticValue);
                                        params.put(toFieldName, listParam);

                                    } else {
                                        if (staticValue != null) {
                                            params.put(toFieldName, staticValue);
                                        }
                                        params.put(toFieldName, fromFieldName != null ? FuncUtils.getValueMap(row, fromFieldName, Object.class) : staticValue);
                                    }


//                                    System.out.println("relation row " + row);
                                }
//                                System.out.println("relation params " + params);
                                List<Map<String, Object>> relationData = listSQL(model, params, connection, false);
                                row.put(field, relationData);
                            }
                        }

                        break;
                    default:
                        Object object = rs.getObject(col);
                        if (object != null)
                            row.put(field, rs.getObject(col));
                        break;
                }
            } catch (SQLException notfound) {
//                System.out.println("is ");
            }


        }
    }

    @Override
    public int executeSQL(String sql, Map<String, Object> params) {
        Connection connection = null;
        int rows = -1;
        try {
            connection = this.dataSource.getConnection();
            assert connection != null;
            connection.setAutoCommit(false);
            rows = executeSQL(sql, params, connection);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                assert connection != null;
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                assert connection != null;
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return rows;
    }

    @Override
    public int executeSQL(String sql, Map<String, Object> params, Connection connection) throws SQLException {
        sql = setParameter(sql, params);
        NamedParameterStatement statement = new NamedParameterStatement(connection, sql);
        setParameterStatement(params, statement);
        System.out.println("executeSQL " + sql);
        System.out.println("executeSQL.parameter " + statement.getParameter());
        int result = statement.executeUpdate();
        System.out.println("executeSQL " + result);
        statement.close();
        return result;
    }

//    private void resultToMap(ResultSetMetaData rsmd, Map<String, String> fieldMapper, Map<String, String> typeMapper) throws SQLException {
//
//    }

    private String setParameter(String sql, Map<String, Object> params) {
//        if (sql.contains(":workerCode")) {
//            System.out.println("workerCode " + workerService.getWorkerCode());
//            System.out.println("params " + params);
//            params.put("workerCode", workerService.getWorkerCode());
//        }

        if (params != null && !params.isEmpty()) {
            for (String key : params.keySet()) {
                if (params.get(key) == null) {
                    sql = sql.replaceAll(":" + key, "null");
//                    System.out.println("sql " + sql);
                }
            }
        }
        return sql;
    }

    @Override
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
