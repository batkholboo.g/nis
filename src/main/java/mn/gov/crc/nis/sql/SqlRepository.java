package mn.gov.crc.nis.sql;

import mn.gov.crc.nis.model.SqlMapper;
import mn.gov.crc.nis.model.SqlModel;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


public interface SqlRepository {
    SqlModel getModel(String code);

    SqlModel getModel(Long id);

    SqlMapper getMapper(String code);

    SqlMapper getMapper(Long id);

    Map<String, SqlModel> reloadSQL();

//    Object monitorSQL();

    List<Map<String, Object>> listSQL(SqlModel sqlModel, Map<String, Object> params);

    List<Map<String, Object>> listSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection, Boolean list) throws SQLException;

    List<Map<String, Object>> listSQL(String sql, Map<String, Object> params, SqlModel sqlModel);

    List<Map<String, Object>> listSQL(String sql, Map<String, Object> params, Connection connection, SqlModel sqlModel, Boolean list) throws SQLException;


    Long totalSQL(SqlModel sqlModel, Map<String, Object> params);

    Long totalSQL(String sql, Map<String, Object> params);

    Long totalSQL(String sql, Map<String, Object> params, Connection connection) throws SQLException;


    Map<String, Object> objectSQL(SqlModel sqlModel, Map<String, Object> params);

    Map<String, Object> objectSQL(String sql, Map<String, Object> params, SqlModel sqlModel);

    Map<String, Object> objectSQL(String sql, Map<String, Object> params, Connection connection, SqlModel sqlModel) throws SQLException;

    Map<String, Object> objectSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException;


    List<Map<String, Object>> patchSQL(SqlModel sqlModel, List<Map<String, Object>> list);

    List<Map<String, Object>> patchSQL(SqlModel sqlModel, List<Map<String, Object>> list, Connection connection) throws SQLException;

    Map<String, Object> deleteSQL(SqlModel sqlModel, Map<String, Object> params);

    Map<String, Object> removeSQL(SqlModel sqlModel, Map<String, Object> params);

    Map<String, Object> restoreSQL(SqlModel sqlModel, Map<String, Object> params);

    Map<String, Object> getPkSQL(SqlModel sqlModel, Map<String, Object> params);

    Map<String, Object> getPkSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException;

    Map<String, Object> saveSQL(SqlModel sqlModel, Map<String, Object> params);

    Map<String, Object> saveSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException;

    Map<String, Object> deleteSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException;

    Map<String, Object> removeSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException;

    Map<String, Object> restoreSQL(SqlModel sqlModel, Map<String, Object> params, Connection connection) throws SQLException;


    SqlModel generateSqlTable(String tableName);
    SqlModel generateSqlTable(String schema, String tableName);

    void modulePortReload(String moduleName);
    String getModulePort(String moduleName);
    Connection getConnection();

    int executeSQL(String sql, Map<String, Object> params);

    int executeSQL(String sql, Map<String, Object> params, Connection connection) throws SQLException;
}
