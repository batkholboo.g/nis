package mn.gov.crc.nis.report;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.util.MultiValueMap;


/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/29 7:13 PM
 */
@AllArgsConstructor
public class ReportData {
    @Getter
    private Long reportId;
    @Getter
    private ReportType type;
    @Getter
    private MultiValueMap<String, String> params;
}
