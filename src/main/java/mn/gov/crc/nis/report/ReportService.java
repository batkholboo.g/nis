package mn.gov.crc.nis.report;

import mn.gov.crc.nis.entity.SysReport;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.*;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/29 7:13 PM
 */
@Service
public class ReportService {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String[] DATE_FORMATS = new String[]{"yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM-dd HH", "yyyy-MM-dd", "yyyy-MM", "yyyy", "HH:mm:ss", "HH:mm"};

    public byte[] buildReport(ReportData reportData, SysReport sysReport) throws ReportNotFoundException, JRException, SQLException {

        if (sysReport != null && sysReport.getReport() != null) {
            InputStream reportStream = new ByteArrayInputStream(sysReport.getReport().getBytes());
            JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
            Map<String, Object> parameter = getParameter(jasperReport, reportData.getParams());

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Exporter exporter = getExporter(reportData.getType(), parameter, outputStream);

            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameter, jdbcTemplate.getDataSource().getConnection());

            exporter.setExporterInput(new SimpleExporterInput(print));
            exporter.exportReport();

            return outputStream.toByteArray();
        }
        throw new ReportNotFoundException(reportData.getReportId() + "");
    }

    private Exporter getExporter(ReportType type, Map<String, Object> parameter, OutputStream outputStream) {
        switch (type) {
            case HTML:
                SimpleHtmlExporterConfiguration htmlConfiguration = new SimpleHtmlExporterConfiguration();
                htmlConfiguration.setBetweenPagesHtml("");
                htmlConfiguration.setHtmlHeader(getHtmlHeader());
                htmlConfiguration.setHtmlFooter(getHtmlFooter());

                parameter.put(JRParameter.IS_IGNORE_PAGINATION, true);
                parameter.put("HTML_REPORT", true);
                HtmlExporter htmlExporter = new HtmlExporter();
                htmlExporter.setExporterOutput(new SimpleHtmlExporterOutput(outputStream));
                htmlExporter.setConfiguration(htmlConfiguration);
                return htmlExporter;
            case PDF:
                JRPdfExporter pdfExporter = new JRPdfExporter();
                pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
                return pdfExporter;
            default:
                SimpleXlsxExporterConfiguration xlsxConfiguration = new SimpleXlsxExporterConfiguration();
                xlsxConfiguration.setKeepWorkbookTemplateSheets(false);

                parameter.put(JRParameter.IS_IGNORE_PAGINATION, true);
                JRXlsxExporter xlsxExporter = new JRXlsxExporter();
                xlsxExporter.setConfiguration(xlsxConfiguration);
                xlsxExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
                return xlsxExporter;
        }
    }

    public Map<String, Object> getParameter(JasperReport jasperReport, MultiValueMap<String, String> queryParams) {
        Map<String, Object> jrParameters = new HashMap<>();
        for (JRParameter jrParameter : jasperReport.getParameters()) {
            if (queryParams.keySet().contains(jrParameter.getName())) {
                Object value = getParamValue(jrParameter, queryParams.get(jrParameter.getName()).get(0));
                jrParameters.put(jrParameter.getName(), value);
            }
        }
        return jrParameters;
    }

    private Object getParamValue(JRParameter jrParameter, String value) {
        Class<?> type = jrParameter.getValueClass();

        if (value.toLowerCase().equals("null")) return null;
        if (String.class.isAssignableFrom(type)) return value;
        if ("".equals(value)) return null;

        if (Date.class.isAssignableFrom(type)) return getDateValue(value, type);
        if (Boolean.class == type || Boolean.TYPE == type) return Boolean.parseBoolean(value);
        if (Byte.class == type || Byte.TYPE == type) return Byte.parseByte(value);
        if (Short.class == type || Short.TYPE == type) return Short.parseShort(value);
        if (Integer.class == type || Integer.TYPE == type) return Integer.parseInt(value);
        if (Long.class == type || Long.TYPE == type) return Long.parseLong(value);
        if (Float.class == type || Float.TYPE == type) return Float.parseFloat(value);
        if (Double.class == type || Double.TYPE == type) return Double.parseDouble(value);
        if (BigDecimal.class == type) return new BigDecimal(value);
        if (BigInteger.class == type) return new BigInteger(value, 10);

        throw new IllegalArgumentException("\"" + value + "\" not parsable to " + type.getName());
    }

    private Object getDateValue(String value, Class type) {
        try {
            Date date = DateUtils.parseDate(value, DATE_FORMATS);
            return type.getConstructor(long.class).newInstance(date.getTime());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }


    private String getHtmlHeader() {
        StringBuffer sb = new StringBuffer();
        sb.append("<html>");
        sb.append("<head>");
        sb.append("  <title></title>");
        sb.append("  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>");
        sb.append("  <style type=\"text/css\">");
        sb.append("    a {text-decoration: none}");
        sb.append("  </style>");
        sb.append("</head>");
        sb.append("<body text=\"#000000\" link=\"#000000\" alink=\"#000000\" vlink=\"#000000\">");
        sb.append("<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
        sb.append("<tr><td align=\"left\">");

        return sb.toString();
    }

    private String getHtmlFooter() {
        StringBuffer sb = new StringBuffer();
        sb.append("</td>");
        sb.append("</tr>");
        sb.append("</table>");
        sb.append("</body>");
        sb.append("</html>");

        return sb.toString();
    }
}
