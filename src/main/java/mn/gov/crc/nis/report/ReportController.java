package mn.gov.crc.nis.report;

import mn.gov.crc.nis.dao.SysReportDAO;
import mn.gov.crc.nis.entity.SysReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/29 6:43 PM
 */
@Controller
@RequestMapping("/reports")
public class ReportController {

    @Autowired
    ReportService reportService;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    @Autowired
    SysReportDAO sysReportDAO;

    @GetMapping("{reportId}/{type}")
    ResponseEntity pdfReport(@PathVariable("reportId") Long reportId, @PathVariable("type") String strType, @RequestParam MultiValueMap<String, String> params) {
        try {
            SysReport sysReport = sysReportDAO.findOne(reportId);
            ReportType type = ReportType.valueOf(strType.toUpperCase());
            byte[] bis = reportService.buildReport(new ReportData(reportId, type, params), sysReport);
            return buildResponse(type, sysReport.getName(), bis);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (ReportNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (Throwable e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    private ResponseEntity buildResponse(ReportType type, String reportName, byte[] body) {
        ByteArrayResource resource = new ByteArrayResource(body);
        ResponseEntity.BodyBuilder bodyBuilder = ResponseEntity
                .ok()
                .contentType(MediaType.parseMediaType(type.getContentType()))
                .contentLength(body.length);

        String fileName = reportName + "_" + dateFormat.format(new Date(System.currentTimeMillis()));

        if (type == ReportType.PDF) {
            bodyBuilder.header("Content-Disposition", "attachment; filename=\"" + fileName + ".pdf\"");
        } else if (type == ReportType.XLSX) {
            bodyBuilder.header("Content-Disposition", "attachment; filename=\"" + fileName + ".xlsx\"");
        }
        return bodyBuilder
                .body(resource);
    }

    @PostMapping(produces = "application/json")
    @Transactional
    public ResponseEntity upload(
            @RequestParam(value = "name", required = true) String name,
            @RequestParam(value = "parentId", required = false) Long parentId,
            @RequestParam(value = "report", required = true) MultipartFile report) throws IOException {

        if (!report.getOriginalFilename().endsWith(".jrxml")) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Report file нь зөвхөн \".jrxml\" төрөлтэй байхыг зөвшөөрөнө.");
        }

        SysReport sysReport = new SysReport();
        sysReport.setName(name);
        sysReport.setReport(new String(report.getBytes(), StandardCharsets.UTF_8));
        sysReport.setParentId(parentId);

        sysReportDAO.save(sysReport);

        return ResponseEntity.ok(sysReport);
    }


    @PostMapping(value = "{reportId}", produces = "application/json")
    @Transactional
    public ResponseEntity upload(
            @PathVariable(value = "reportId") Long reportId,
            @RequestParam(value = "name", required = true) String name,
            @RequestParam(value = "parentId", required = false) Long parentId,
            @RequestParam(value = "report", required = true) MultipartFile report) throws IOException {

        if (!report.getOriginalFilename().endsWith(".jrxml")) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Report file нь зөвхөн \".jrxml\" төрөлтэй байхыг зөвшөөрөнө.");
        }

        SysReport sysReport = sysReportDAO.exists(reportId) ? sysReportDAO.findOne(reportId) : new SysReport();
        if (sysReport.getId() == null) sysReport.setId(reportId);
        sysReport.setName(name);
        sysReport.setReport(new String(report.getBytes(), StandardCharsets.UTF_8));
        sysReport.setParentId(parentId);

        sysReportDAO.save(sysReport);

        return ResponseEntity.ok(sysReport);
    }

}
