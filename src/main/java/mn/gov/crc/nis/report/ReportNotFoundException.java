package mn.gov.crc.nis.report;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/29 8:55 PM
 */
public class ReportNotFoundException extends Exception {
    public ReportNotFoundException(String reportName) {
        super(reportName + " report not found!!!");
    }
}
