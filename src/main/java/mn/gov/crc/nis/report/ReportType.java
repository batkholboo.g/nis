package mn.gov.crc.nis.report;

/**
 * Created by Nasanjargal Bayanmunkh /Б.Насанжаргал/
 * Date: 2017/11/29 7:14 PM
 */
public enum ReportType {
    PDF("application/pdf"),
    XLSX("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
    HTML("text/html");

    private String contentType;

    ReportType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }


}
