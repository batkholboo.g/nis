package mn.gov.crc.nis.model;

import lombok.Data;

@Data
public class SqlModel {
    Long id;
    String code;
    String name;
    String tableName;
    String sqlQuery;
    Long mapperId;
    SqlMapper mapper;
    String seqName;
    String schemaName;
}
