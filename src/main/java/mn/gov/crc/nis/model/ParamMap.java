package mn.gov.crc.nis.model;

import mn.gov.crc.nis.util.FuncUtils;

import java.util.Map;

public class ParamMap {
    private Map<String, Object> variables;

    public ParamMap(Map<String, Object> vars) {
        this.variables = vars;
    }

    public Long getLong(String field) {
        return FuncUtils.getValueMap(variables, field, Long.class);
    }

    public Integer getInt(String field) {
        Integer result = FuncUtils.getValueMap(variables, field, Integer.class);
        return result == null ? 0 : result;
    }

    public Double getDecimal(String field) {
        Double result = FuncUtils.getValueMap(variables, field, Double.class);
        return result == null ? 0 : result;

    }

    public Boolean getBool(String field) {
        Boolean result = FuncUtils.getValueMap(variables, field, Boolean.class);
        return result == null ? false : result;
    }
}
