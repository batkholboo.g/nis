package mn.gov.crc.nis.model;


import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
public class SqlMapper {
    Long id;
    String code;
    String name;
    Map<String, String> typeMapper = new LinkedHashMap<>();
    Map<String, String> fieldMapper = new LinkedHashMap<>();
    Map<String, Object> modelMapper = new LinkedHashMap<>();
    Map<String, Object> settingsMapper = new LinkedHashMap<>();
    Map<String, Object> relationMapper = new LinkedHashMap<>();
    String pk;

    public void fromExtend(SqlMapper extend) {
        for (String key : extend.typeMapper.keySet()) {
            typeMapper.put(key, extend.typeMapper.get(key));
        }
        for (String key : extend.fieldMapper.keySet()) {
            fieldMapper.put(key, extend.fieldMapper.get(key));
        }
        for (String key : extend.modelMapper.keySet()) {
            modelMapper.put(key, extend.modelMapper.get(key));
        }
        for (String key : extend.settingsMapper.keySet()) {
            settingsMapper.put(key, extend.settingsMapper.get(key));
        }
        for (String key : extend.relationMapper.keySet()) {
            relationMapper.put(key, extend.relationMapper.get(key));
        }
        this.pk = extend.getPk();
    }
}

