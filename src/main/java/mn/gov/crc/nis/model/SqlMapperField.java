package mn.gov.crc.nis.model;


import lombok.Data;

@Data
public class SqlMapperField {
    Long id;
    String fieldName;
    String columnName;
    String type;
}
